<?php

namespace app\controllers;

use app\models\Users;
use app\models\UsersPasswordRecoveryKeys;

use lithium\security\Auth;
use lithium\util\Validator;
use lithium\util\Set;
use li3_extras\storage\Session;

class AuthController extends \lithium\action\Controller
{
	public function _init()
	{
		parent::_init();
		
		$this->user = Auth::check('default');
		$this->_render['layout'] = 'empty';
		
		if (!$this->user && !method_exists(__CLASS__, $this->request->params['action'])) {
			return $this->redirect('Auth::login', array('exit' => TRUE));
		}
		
		$this->set(array(
			'user' => $this->user
		));
	}
	
	
	public function login()
	{
		if ($this->request->is('post')) {
			$this->user = Auth::check('default', $this->request);
			$this->set(array('errors' => TRUE));
		}
		
		$redirect = $this->request->redirect ?: NULL;
		$this->request = $redirect ?: $this->request;
		
		if ($this->user) {
			$location = $redirect ? $redirect->url : 'Admin::index';
			return $this->redirect($location);
		}
	}
	
	
	public function logout()
	{
		Auth::clear('default');
		return $this->redirect('Auth::login');
	}
	
	
	public function password_recovery()
	{
		$formData = $this->request->data + array('email' => NULL);
		$this->set(array(
			'formData' => $formData,
			'errors'   => FALSE,
			'success'  => FALSE,
		));
		
		if (!$this->request->is('post')) {
			return;
		}
		
		if (!Validator::rule('email', $formData['email'])) {
			return array(
				'errors' => 'Please provide a valid email address!'
			);
		}
		
		$user = Users::first(array(
			'conditions' => array(
				'email'  => $formData['email'],
				'status' => Users::STATUS_ACTIVE
			)
		));
		
		if (!$user) {
			return array(
				'errors' => 'There is no active user using this email address.'
			);
		}
		
		if (UsersPasswordRecoveryKeys::isThrottled($user->id)) {
			return array(
				'errors' => 'The maximum number of password reset attempts has been reached. Please try again in one hour.'
			);
		}
		
		$passwordRecovery = UsersPasswordRecoveryKeys::create(array(
			'user_id' => $user->id
		));
		$passwordRecovery->save();
		
		UsersPasswordRecoveryKeys::emailUrl(array(
			'user'    => $user,
			'key'     => $passwordRecovery,
			'context' => $this->request
		));
		
		return array(
			'success' => TRUE
		);
	}
	
	
	public function password_reset($key = NULL)
	{
		$this->set(array(
			'errors'  => FALSE,
			'success' => FALSE,
			'invalid' => FALSE
		));
		
		$userKey = UsersPasswordRecoveryKeys::getValid($key);
		
		if (!$userKey) {
			return array('invalid' => TRUE);
		}
		
		if (!$this->request->is('post')) {
			return;
		}
		
		$model = new Users();
		$rules = array_intersect_key($model->validates, array('password' => NULL, 'confirm' => NULL));
		
		$errors = Validator::check($this->request->data, $rules) + array('password' => array());
		$errors = implode('<br />', Set::flatten($errors));
		
		if ($errors) {
			return compact('errors');
		}
		
		$user = Users::first($userKey->user_id);
		
		if ($user) {
			$user->password = $user->confirm = $this->request->data['password'];
			$user->save();
		}
		
		$userKey->save(array(
			'used' => gmdate('Y-m-d H:i:s')
		));
		
		UsersPasswordRecoveryKeys::update(array(
			'status' => UsersPasswordRecoveryKeys::STATUS_DISABLED
		), array(
			'user_id' => $userKey->user_id
		));
		
		return array('success' => TRUE);
	}
}

?>