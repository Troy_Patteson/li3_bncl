(function($){
	$(function() {
		var Dialog = {
			confirm : function(msg, callback, classes) {
				return bootbox.dialog('<div class="modal-content">' + msg + '</div>', [{
					label    : '<i class="icon-remove"></i>',
					'class'  : 'btn btn-no',
					callback : function() {
						return callback(false);
					}
				}, {
					label    : '<i class="icon-ok"></i>',
					'class'  : 'btn-yes',
					callback : function() {
						return callback(true);
					}
				}], {
					animate  : false,
					backdrop : true,
					keyboard : true,
					onEscape : callback || true
				});
			}
		}

		function toggleAllCheckboxes() {
			var $this = $(this),
			    form  = $this.closest('form'),
			    set   = form.find('input[type="checkbox"][name="' + $this.attr('data-target') + '"]');

			// in theory, set.attr('checked', this.checked) should have done the job... but it doesn't!
			set.each(function() {
				this.checked = $this[0].checked;
			});

			form.find('[type="submit"]').attr('disabled', !this.checked);
		}

		function deselectAllCheckboxes() {
			var $this    = $(this),
			    form     = $this.closest('form'),
			    name     = $this.attr('name'),
			    checkAll = form.find('input[data-target="' + name + '"]'),
			    checked  = !form.find('input[name="' + name + '"]:not(:checked)').size();

			checkAll.each(function() {
				this.checked = checked;
			});

			form.find('[type="submit"]').attr('disabled', !form.find('input[name="' + name + '"]:checked').size());
		}

		$('input[name="check-all"]').click(toggleAllCheckboxes);
		$('.col-check input[type="checkbox"]').not('[name="check-all"]').click(deselectAllCheckboxes);


		$('.delete-form').submit(function(e) {
			var $this = $(this),
			    message = 'Are you sure you want to delete the selected items?';

			if (!$this.hasClass('confirmed')) {
				e.preventDefault();

				Dialog.confirm(message, function(result) {
					result && $this.addClass('confirmed').submit();
				});
			}
		});


		$('.wizzard li *').tooltip();

		function filterCampaigns() {
			$(this).closest('form').submit();
		}

		$('#campaignFilters select').change(filterCampaigns);
		$('#campaignFilters input').click(filterCampaigns);


		$('#addProduct').click(function() {
			var product = $('#tpl-product>div').clone();
			$('#campaignProducts').append(product);
      initializeFileUpload();
		});


		$('#campaignProducts').on('click', '.remove-item', function() {
			var message = 'Are you sure you want to remove this product from the campaign?',
			    parent  = $(this).closest('.product-listing');

			if (parent.find('input[name^="new"]').size()) {
				parent.remove();
				return;
			}

			Dialog.confirm(message, function(result) {
				result && parent.remove();
			});
		});


		$('#addEmail').click(function() {
			var product = $('#tpl-email>div').clone();
			$('#manageEmails').append(product);
		});


		$('#manageEmails').on('click', '.remove-item', function() {
			var message = 'Are you sure you want to remove this email address?',
			    parent  = $(this).closest('.item-listing');

			if (parent.find('input[name^="new"]').size()) {
				parent.remove();
				return;
			}

			Dialog.confirm(message, function(result) {
				result && parent.remove();
			});
		});


		var titleField = $('#inTitle'),
		    slugField  = $('#inSlug');

		if (titleField.size() && slugField.size()) {
			titleField.on('keypress, keydown, keyup', function() {
				if (titleField.val().match(/^\s*$/)) {
					slugField.removeAttr('data-custom').val('');
				}

				if (slugField.attr('data-custom') != 1) {
					var slug = String(titleField.val()).replace(/[^a-z0-9_]/gi, '-').replace(/-+/g, '-').replace(/^-*(.*?)-*$/g, '$1');

					slugField.val(slug);
				}
			});

			slugField.on('keyup', function() {
				if (slugField.val() != '') {
					slugField.attr('data-custom', 1);
				} else {
					slugField.attr('data-custom', 0);
				}
			});
		}

		$('.datepicker').datepicker();

		$('button.attach-vouchers').click(function() {
			if (this.disabled) {
				return false;
			}

			this.disabled = true;

			var $this = $(this),
			    url   = $this.attr('data-url'),
			    input = $this.parent().find('input[name*="vouchers"]'),
			    count = parseInt(input.val(), 10);

			if (!count) {
				onError();
				return;
			}

			function onError() {
				input.addClass('error');
				$this[0].disabled = false;
			}

			function onSuccess(data) {
				if (!data.product) {
					onError()
					return;
				}

				input.removeClass('error').val(data.product.vouchers).attr('disabled', true);
				$this.hide().after('<button class="btn" disabled="disabled"><i class="icon-ok-sign"></i></button>');

				if ($this.closest('.extended-voucher-listing').size() > 0) {
					$this.closest('.vouchers-group').find('.extended-vouchers-toggle input').attr('disabled', true);
				}
			}

			$.ajax({
				type    : 'POST',
				url     : url,
				data    : {count : count},
				error   : onError,
				success : onSuccess
			});
		});

		$('.extended-vouchers-toggle input').click(function(e) {
			!this.disabled && $(this).closest('.vouchers-group').find('.extended-voucher-listing').toggleClass('hide', !this.checked);
		});

    var initializeFileUpload = function()
    {
        $('.fileupload-complete button').click(function() {
          $(this).parent().hide().find('input').val('').end().siblings('.fileupload').show();
        });

        $('.fileupload input').each(function() {
          var $this    = $(this),
              parent   = $this.closest('.fileupload'),
              errors   = parent.siblings('.fileupload-errors'),
              complete = parent.siblings('.fileupload-complete'),
              label    = complete.find('.fileupload-label'),
              progress = parent.siblings('.fileupload-progress'),
              status   = progress.find('.bar');

          $this.fileupload({
            dataType : 'json',
            submit : function(e, data) {
              label.text(data.files[0].name);
              status.width(0);

              errors.hide();
              parent.hide();
              progress.show();
            },

            progress : function(e, data) {
              status.css({width : parseInt(data.loaded / data.total * 100, 10) + '%'});
            },

            error : function() {
              progress.hide();
              parent.show();
              errors.html('We couldn\'t upload the selected file. Please try again.').show();
            },

            done: function(e, data) {
              progress.hide();

              if (data.result.file) {
                complete.find('input').val(data.result.file);
                complete.show();
              } else {
                var errorMsg = data.result.errors.join('<br />');

                parent.show();
                errors.html(errorMsg).show();
              }
            }
          });
        });
    }

		initializeFileUpload();

		/* Email preview dialog */
		$('.email-preview').click(function(e) {
			var $this = $(this),
			    message = $('.tpl-email-preview').html();

			var dialog = Dialog.confirm(message, function(result) {
				if (result) {
					dialog.addClass('loading');

					$.post($this.attr('data-url'), {
						email : dialog.find('input[name="email"]').val()
					}).done(function() {
						dialog.modal('hide');
					}).fail(function() {
						dialog.removeClass('loading').find('.modal-content').text('The email could not be sent. Please try again!');
					});

					return false;
				}
			});
		});

    /**
     * Search
     */

    if($('.btn-search').length)
    {
        var search = $('#tpl-search').html();
        var search_is_visible = false;

        $('.btn-search').popover({
            html: true
          , content: search
          , placement: 'left'
          , container: 'body'
        });
        $('.btn-search').popover('hide');

        $('.btn-search').click(function(e)
        {
            e.preventDefault();

            $this = $(this);

            if(!search_is_visible)
            {
                search_is_visible = true;
                $this.popover('show');
            }
            else
            {
                search_is_visible = false;
                $this.popover('hide');
            }
        });
    }

    /**
     * Email preview
     */

    $('.btn-preview-email').click(function(e)
    {
        e.preventDefault();

        var preview_window = window.open('', '', 'width=800,height=600');
        preview_window.document.write($('#inEmailTemplate').val());
    });

    /**
     * Rakuten
     */

    var rakutenCheck = function()
    {
        if($('#inRakuten').prop('checked'))
        {
            $('.other-tracking-control-group').hide();
        }
        else
        {
            $('.other-tracking-control-group').show();
        }
    }

    rakutenCheck();
    $('#inRakuten').change(rakutenCheck);

	});

})(jQuery);