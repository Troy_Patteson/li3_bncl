<?php

$titles = array(
  1 => 'Step 1: Website address',
  2 => 'Step 2: Paypal & currency',
  3 => 'Step 3: Unique code prefix',
  4 => 'Step 4: Website template',
  5 => 'Step 5: Email template',
);

$currentStep = intVal($step, 10);
$prevStep    = $currentStep - 1;
$nextStep    = $currentStep + 1;

?>

<form class="" action="<?=$this->url($this->_request->params)?>" method="post">

  <div class="content-top">
    <div class="pull-right">
      <a href="<?=$this->url('Websites::index')?>" class="btn btn-close"><i class="icon-remove"></i></a>
    </div>
    <h1>Edit website</small></h1>
  </div>
  
  <div class="content-middle">
    <fieldset>
      <h2><?=$titles[$currentStep]?></h2>
      <?php if ($errors) : ?>
      <div class="alert alert-error">
        <ul>
          <?php foreach ($errors as $error) : ?>
          <li><?php echo implode('<br />', $error)?></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <?php endif; ?>
      
      <?php echo $this->_render('template', '_step-' . $step); ?>
      
    </fieldset>
  </div>
      
  <div class="content-bottom">

      <div class="form-actions">
          <div class="actions-left">
            <?php if ($currentStep != 1) : ?>
              <a href="<?=$this->url(array('args' => 'step-' . $prevStep) + $this->_request->params)?>" class="btn btn-prev">Prev<i class="icon-chevron-left"></i></a>
            <?php endif; ?>
          </div>
          <div class="actions-middle">
            <?php
              echo $this->_render('element', 'form-wizard', array(
                'stepsTitles' => $titles,
                'currentStep' => $currentStep,
                'unlocked'    => $unlocked,
                'baseHref'    => array('Websites::edit', 'id' => $website->id)
              ));
            ?>
          </div>
          <div class="actions-right">
            <?php if ($currentStep != 5) : ?>
            <button type="submit" class="btn btn-next"><i class="icon-chevron-right"></i>Next</button>
            <?php else : ?>
            <button type="submit" class="btn btn-save"><i class="icon-ok"></i>Save</button>
            <?php endif; ?>
          </div>
      </div>

  </div>

</form>
