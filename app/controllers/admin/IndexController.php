<?php

namespace app\controllers\admin;

use \app\models\Users;

use lithium\action\DispatchException;
use lithium\util\Inflector;
use li3_extras\storage\Session;

class IndexController extends \app\controllers\AuthController
{
	public function _init()
	{
		parent::_init();
		
		$this->_render['layout'] = 'admin';
	}
	
	
	public function render(array $options = array())
	{
		$controller = Inflector::underscore($this->request->params['controller']);
		
		$options += array(
			'controller' => 'admin/' . $controller
		);
		
		return parent::render($options);
	}
	
	
	public function index()
	{
		return $this->redirect('Campaigns::index');
	}
	
	
	public function account()
	{
		$this->set(array(
			'errors'  => array(),
			'success' => Session::flash('flash.passwordChanged')
		));
		
		$user = Users::first($this->user['id']);
		
		if (!$this->request->is('post')) {
			return;
		}
		
		$data = array_intersect_key($this->request->data, array('password' => '', 'confirm' => ''));
		$user->set($data);
		
		if ($user->save()) {
			Session::write('flash.passwordChanged', TRUE);
			return $this->redirect('Index::account');
		}
		
		$this->set(array(
			'errors' => $user->errors()
		));
	}
	
	
	protected function _notFound()
	{
		throw new DispatchException('Could not route request.');
	}
}

?>