<ul class="side-menu">
	<li<?php echo ($this->_request->params['controller'] == 'campaigns') ? ' class="active"' : ''?>><a href="<?=$this->url('Campaigns::index')?>">CAMPAIGNS</a></li>
	<li<?php echo ($this->_request->params['controller'] == 'websites') ? ' class="active"' : ''?>><a href="<?=$this->url('Websites::index')?>">WEBSITES</a></li>
</ul>

<?php

if ($this->_options['controller'] == 'admin/campaigns' && $this->_options['action'] == 'index') {
	echo $this->_render('template', '_filters');
}

?>