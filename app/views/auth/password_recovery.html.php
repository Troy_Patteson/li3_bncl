<form class="authentication span5" method="post" action="<?=$this->url('Auth::password_recovery');?>">

  <div class="form-fields">
    <fieldset>
      <legend>Password recovery</legend>
      
      <?php if ($success) : ?>
      <div class="alert alert-success"><i class="icon-envelope-alt"></i> The email has been sent.</div>
      <p>
        Please check your email and follow the instructions on how to reset your password.
      </p>
      <?php endif; ?>
      
      <?php if ($errors) : ?>
      <div class="alert alert-error"><i class="icon-minus-sign"></i> <?=$errors?></div>
      <?php endif; ?>
      
      <?php if (!$success) : ?>
      <p>
        Tell us your email and we'll send you instructions on how to reset your password.
      </p>
      
      <input type="email" name="email" id="email" placeholder="Email" class="input-block-level last" value="<?=$formData['email']?>" />
      <?php endif; ?>
    </fieldset>
  </div>

	<div class="form-actions">
		<?php if (!$success) : ?>
		<button type="submit" class="btn btn-authentication pull-right">Send</button>
		<?php endif; ?>
		<a href="<?=$this->url(array('Auth::login', 'admin' => NULL))?>">Return to the login form</a>
	</div>

</form>