
<div class="tpl" id="tpl-search">
	<div class="section-search">
		<form class="form-inline" action="<?=$this->url(array('Campaigns::search'))?>" method="get">
			<?php if (!empty($campaign)) : ?>
			<input type="hidden" name="campaign_id" value="<?=$campaign->id?>" />
			<?php endif; ?>
      <input class="input-large" type="text" name="code" placeholder="Code" />
      or
			<input class="input-small" type="text" name="transaction_id" placeholder="PayPal TXN" />
			<input class="input-small" type="text" name="email" placeholder="Email" />
			<input class="input-small" type="text" name="first_name" placeholder="First name" />
			<input class="input-small" type="text" name="last_name" placeholder="Last name" />
			<button type="submit" class="btn">Search</button>
		</form>
	</div>
</div>
