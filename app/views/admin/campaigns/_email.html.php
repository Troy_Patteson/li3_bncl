<?php

$fieldName  = $email && $email->exists() ? 'email[' . $email->id . ']' : 'new-email[]';
$fieldValue = $email ? $email->address : '';

$errors = $email ? $email->errors() : array();

?>
<div class="control-group email-listing item-listing">
	<label class="control-label">Email address</label>
	<div class="controls">
		<input class="input-large <?=isset($errors['address']) ? 'error' : ''?>" type="text" name="<?=$fieldName?>" value="<?=$fieldValue?>" />
		<button class="btn btn-delete remove-item" type="button"><i class="icon-minus-sign"></i></button>
	</div>
</div>
