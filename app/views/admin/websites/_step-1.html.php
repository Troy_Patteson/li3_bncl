<?php
use \DateTime;
use \DateTimeZone;

$timezones = array(0 => '--- Select ---');

foreach (DateTimeZone::listIdentifiers() as $timezoneName) {
	$timezone = new DateTime('now', new DateTimeZone($timezoneName));
	$offset   = $timezone->getOffset();
	$sign     = $offset >= 0 ? '+' : '-';
	$hours    = gmdate('H:i', abs($offset));
	$details  = explode('/', $timezoneName);
	
	if (count($details) < 2) {
		continue;
	}
	
	list($group, $zone) = $details;
	
	if (!isset($timezones[$group])) {
		$timezones[$group] = array();
	}
	
	$timezones[$group][$timezoneName] = str_replace('_', ' ', $zone) . ' (GMT ' . $sign . ' ' . $hours . ')';
}
?>
	<div class="control-group">
		<label class="control-label" for="wsHost">Full URL</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsHost" name="host" value="<?=$website->host?>" autocomplete="off" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="wsHost">Timezone</label>
		<div class="controls">
				<?php
				echo $this->form->select('timezone', $timezones, array(
					'value' => $website->timezone, 
					'class' => 'input-block-level'
				));
				?>
		</div>
	</div>
