
<?php
	foreach ($products as $product) :
		$errors = $product->errors();
?>
<div class="vouchers-group">
	<div class="control-group voucher-listing clearfix">
		<div class="voucher-amount">
			<label class="control-label"><?=$product->name?></label>
		</div>
		<div class="voucher-limit">
			<label class="control-label">Buy limit</label>
		</div>
		
		<div class="voucher-amount">
			<div class="controls">
				<?php if ($product->vouchers) : ?>
				<input class="input-large <?=isset($errors['vouchers']) ? 'error' : ''?>" type="text" name="vouchers[<?=$product->id?>]" placeholder="<?=$product->available_vouchers?> available out of <?=$product->vouchers?>" value="" />
				<?php else : ?>
				<input class="input-large <?=isset($errors['vouchers']) ? 'error' : ''?>" type="text" name="vouchers[<?=$product->id?>]" placeholder="0" value="" />
				<?php endif; ?>

				<a href="<?=$this->url(array('Products::download_vouchers', 'id' => $product->id, 'type' => 'csv'))?>" class="btn btn-download" target="_blank"><i class="icon-download"></i></a>

				<button data-url="<?=$this->url(array('Products::attach_vouchers', 'id' => $product->id, 'type' => 'json'))?>" class="btn btn-attach attach-vouchers" type="button">
					<i class="icon-link"></i>
				</button>
			</div>
		</div>
		
		<div class="voucher-limit">
			<input class="input-mini" type="text" name="buy-limit[<?=$product->id?>]" placeholder="unlimited" value="<?=$product->buy_limit ?: ''?>" />
		</div>
	</div>
	
	<div class="control-group voucher-listing extended-voucher-listing clearfix <?php echo !$product->vouchersExtended ? 'hide' : '' ?>">
		<div class="voucher-amount">
			<div class="controls">
				<?php if ($product->vouchersExtended) : ?>
				<input class="input-large <?=isset($errors['extended-vouchers']) ? 'error' : ''?>" type="text" name="extended-vouchers[<?=$product->id?>]" placeholder="<?=$product->remainingExtended?> available out of <?=$product->vouchersExtended?>" value="" />
				<?php else : ?>
				<input class="input-large <?=isset($errors['extended-vouchers']) ? 'error' : ''?>" type="text" name="extended-vouchers[<?=$product->id?>]" placeholder="0" value="" />
				<?php endif; ?>

				<a href="<?=$this->url(array('Products::download_vouchers', 'id' => $product->id, 'type' => 'csv', 'args' => array('extended')))?>" class="btn btn-download" target="_blank"><i class="icon-download"></i></a>

				<button data-url="<?=$this->url(array('Products::attach_vouchers', 'id' => $product->id, 'type' => 'json', 'args' => array('extended')))?>" class="btn btn-attach attach-vouchers" type="button">
					<i class="icon-link"></i>
				</button>
			</div>
		</div>
	</div>

	<div class="control-group extended-vouchers-toggle">
		<label class="control-label checkbox">
			<input type="checkbox" name="" value="" <?php echo $product->vouchersExtended ? 'disabled checked' : '' ?> />
			Extendable
		</label>
	</div>
</div>
<?php
	endforeach;
?>