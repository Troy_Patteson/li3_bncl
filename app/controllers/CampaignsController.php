<?php

namespace app\controllers;

use app\models\Websites;
use app\models\Campaigns;
use app\models\Products;
use app\models\Vouchers;
use app\models\Orders;
use app\models\OrdersItems;
use app\models\EmailNotifications;

use li3_paypal\ExpressCheckout;

use lithium\data\Connections;
use lithium\net\http\Router;
use lithium\action\DispatchException;

class CampaignsController extends \lithium\action\Controller
{
	public function _init()
	{
		parent::_init();

		$this->sandboxed = FALSE;
		$this->testing   = FALSE;
	}


	public function render(array $options = array())
	{
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
			$campaign = @$this->_render['data']['campaign'];
			if ($campaign) {
				$campaign->layout->responsive = TRUE;
			}
			$this->_render['layout'] = 'responsive';
		}


		return parent::render($options);
	}


	public function index($action = NULL)
	{
		$params = $this->request->params + array(
			'id'   => NULL,
			'slug' => NULL
		);

		$errors  = array();
		$website = preg_replace('/^' . Websites::SUBDOMAIN . '\./', '', $this->request->env('HTTP_HOST'));

		if ((!empty($action) && !method_exists(__CLASS__, $action)) || empty($params['id']) || empty($params['slug'])) {
			throw new DispatchException('Could not route request.');
		}

		$campaign = Campaigns::completeDetails($params);

		if (!$campaign || (($website != $campaign->website->host || !$campaign->isVisible()) && !$this->sandboxed)) {
			throw new DispatchException('Could not route request.');
		}

		if(!empty($campaign->use_rakuten))
		{
				$rakuten_mediaforge = json_decode(RAKUTEN_MEDIAFORGE, true);
				$rakuten_linkshare  = json_decode(RAKUTEN_LINKSHARE, true);
				if(!empty($rakuten_mediaforge[$campaign->website->host]))
				{
						$this->set(array(
								'rakuten_mediaforge_mid' => $rakuten_mediaforge[$campaign->website->host]
						));
				}
				if(!empty($rakuten_linkshare[$campaign->website->host]))
				{
						$this->set(array(
								'rakuten_linkshare_mid' => $rakuten_linkshare[$campaign->website->host]
						));
				}
		}

		$formData = $this->request->data + array(
			'first_name'         => '',
			'last_name'          => '',
			'email'              => '',
			'email_confirmation' => '',
			'phone'              => '',
			'qty'                => array()
		);

		$availability = Products::availability($campaign->products->values('id'));

		$errors['invalidQuantity'] = array();

		foreach ($campaign->products as $product) {
			$id  = $product->id;
			$qty = !empty($formData['qty'][$id]) ? $formData['qty'][$product->id] : 0;

			$formData['qty'][$id] = $qty;

			$product->availability = $availability->offsetGet($product->id);

			if ($product->buy_limit > 0 && $product->buy_limit < $qty) {
				$errors['invalidQuantity'][] = 'There is a buy limit of <strong>' . $product->buy_limit . '</strong> vouchers for the product <strong>' . $product->name . '</strong>.';
				continue;
			}

			if (!empty($formData['extend'][$product->id]) && $formData['extend'][$product->id] === '1') {
				if ($product->availability->remainingExtended < $qty) {
					$err = 'You have requested more vouchers than available for product <strong>' . $product->name . '</strong>.';
					if ($product->availability->remaining >= $qty) {
						$err .= ' There are, however, enough regular vouchers for that same product.';
					}

					$errors['invalidQuantity'][] = $err;
				}
			} else {
				if ($product->availability->remaining < $qty) {
					$err = 'You have requested more vouchers than available for product <strong>' . $product->name . '</strong>.';
					if ($product->availability->remainingExtended >= $qty) {
						$err .= ' There are, however, enough extended vouchers for that same product.';
					}

					$errors['invalidQuantity'][] = $err;
				}
			}
		}

		if (!count($errors['invalidQuantity'])) {
			unset($errors['invalidQuantity']);
		}

		$this->set(array(
			'formData'    => $formData,
			'errors'      => $errors,
			'order'       => Orders::create($formData),
			'campaign'    => $campaign,
			'layout'      => $campaign->layout,
			'placeholder' => Websites::TPL_PLACEHOLDER
		));

		if ($this->sandboxed) {
			$credentials = array(
				'username'  => PAYPAL_SANDBOX_USERNAME,
				'password'  => PAYPAL_SANDBOX_PASSWORD,
				'signature' => PAYPAL_SANDBOX_SIGNATURE,
			);
		} else {
			$credentials = array(
				'username'  => $campaign->website->api_username,
				'password'  => $campaign->website->api_password,
				'signature' => $campaign->website->api_signature,
			);
		}

		$this->paypal = new ExpressCheckout();
		$this->paypal->sandboxed($this->sandboxed);
		$this->paypal->credentials($credentials);
		$this->paypal->currency($campaign->website->currency_abbreviation);
		$this->paypal->urls(array(
			'cancel' => Router::match(array('args' => 'cancel') + $params, $this->request, array('absolute' => TRUE)),
			'return' => Router::match(array('args' => 'success') + $params, $this->request, array('absolute' => TRUE))
		));

    $this->_render['template'] = $campaign->layout->name . '/index';

		if (method_exists(__CLASS__, $action)) {
			return $this->$action($campaign);
		}
	}


	protected function buy($campaign)
	{
		$this->_render['template'] = $campaign->layout->name . '/buy';

		if (!$this->request->is('post')) {
			return;
		}

		extract($this->_render['data']);

		$products = Products::buildSet();
		$order->value = 0;

		foreach ($formData['qty'] as $id => $qty) {
			if ($formData['qty'][$id]) {
				$product = $campaign->products->offsetGet($id);

				$product->qty = $formData['qty'][$id];
				$product->extended = empty($formData['extend'][$id]) || !$product->availability->remainingExtended ? 0 : 1;
				$products->offsetSet($id, $product);

				$order->value += $product->price * $product->qty;

				if ($product->extended) {
					$order->value += Products::EXTENSION_PRICE * $product->qty;
				}
			}
		}

		$order->set(array(
			'products'    => $products,
			'campaign_id' => $campaign->id
		));

		if ($order->reachedBuyLimit($campaign->products)) {
			$errors['buyLimit'] = array('You have exceeded the maximum allowed quantity per customer for some products');
		}

		if ($errors || !$order->validates() || !$products->count()) {
			$errors += $order->errors();
			if (!$products->count()) {
				$errors['notEmpty'] = array('You haven\'t added any products');
			}

			return compact('errors');
		}

		foreach ($products as $product) {
			$this->paypal->addItem(array(
				'id'    => $product->id,
				'name'  => $product->name,
				'price' => $product->price,
				'qty'   => $product->qty
			));

			if ($product->extended) {
				$this->paypal->addItem(array(
					'id'    => $product->id . '-EXT',
					'name'  => 'Extension for ' . $product->name,
					'price' => Products::EXTENSION_PRICE,
					'qty'   => $product->qty
				));
			}
		}

		$params = array_diff_key($this->request->params, array(
			'action' => NULL,
			'type'   => NULL
		));

		if ($this->paypal->total() == 0) {
			$order->save();
			$hash = md5($order->id . $order->email . $order->created);

			return $this->redirect(array(
				'args' => array('success', $hash)
			) + $params);
		}

		$success = $this->paypal->create();

		if ($success) {
			if ($this->testing || !$this->sandboxed) {
				$order->token = $this->paypal->token();
				$order->save();
			}

			$this->paypal->checkout();
		}
	}


	protected function success($campaign)
	{
		$this->_render['template'] = $campaign->layout->name . '/success';

		$data = $this->request->query + array(
			'token'   => NULL,
			'PayerID' => NULL
		);

		$errors = array();
		$order  = Orders::findFirstByToken($data['token']);
		$isFree = FALSE;

		if (!empty($this->request->args[1])) {
			$isFree = TRUE;
			$hash   = 'MD5(CONCAT(id, email, created))';
			$order  = Orders::first(array(
				'conditions' => array($hash => $this->request->args[1])
			));
		} elseif (!empty($data['token'])) {
			$order = Orders::findFirstByToken($data['token']);
		}

		if ($order) {
			$message = str_replace(
				Orders::$messagesVars,
				array($order->fullName(), $order->email),
				$campaign->thank_you_msg
			);

			$this->set(array(
				'message' => $message,
				'amount'  => $order->value,
				'order'   => $order,
			));

			$order->products = OrdersItems::forOrder($order->id);

			if ($order->status == Orders::STATUS_COMPLETED) {
				if ($this->testing || $this->sandboxed) {
					$vouchers = array();
					$website  = Websites::first($campaign->website_id);

					foreach ($order->products as $item) {
						for ($q = 0; $q < $item->quantity; $q++) {
							$vouchers[] = Vouchers::create(array(
								'voucher'      => Products::generateVoucher($website->code_prefix, $campaign->id),
								'product_name' => $item->product->name,
								'extended'     => $item->extended,
							));
						}
					}

					$vouchers = Vouchers::buildSet($vouchers);
				} else {
					$vouchers = Vouchers::byOrder($order->id);
				}

				return array('vouchers' => $vouchers);
			}
		}

		if (!$order || $order->status != Orders::STATUS_PENDING) {
			return array(
				'message'  => 'This order has been canceled',
				'amount'   => 0,
				'vouchers' => Vouchers::buildSet()
			);
		}

		$qty = array();

		$available = Products::availability($campaign->products->values('id'));

		foreach ($campaign->products as $product) {
			$item = $order->products->first(array('product_id' => $product->id));
			$prod = $available->offsetGet($product->id);

			$product->qty = $qty[$product->id] = $item ? $item->quantity : 0;

			$remaining = $item && $item->extended ? $prod->remainingExtended : $prod->remaining;

			if (($item && !$prod) || ($item && $item->quantity > $remaining)) {
				$errors['invalidQuantities'] = array('You have requested more items than available.');
			}
		}

		if ($order->reachedBuyLimit($campaign->products)) {
			$errors['buyLimit'] = array('You have exceeded the maximum allowed quantity per customer for some products');
		}

		if ($errors) {
			$formData = $order->data() + compact('qty');

			$this->set(array(
				'errors'   => $errors,
				'formData' => $formData
			));
			$this->_render['template'] = $campaign->layout->name . '/buy';

			$order->status = Orders::STATUS_FAILED;
			$order->save();

			return;
		}

		if ($this->sandboxed) {
			$order->status = Orders::STATUS_COMPLETED;
			$order->save();

			$vouchers = array();
			$website  = Websites::first($campaign->website_id);

			foreach ($order->products as $item) {
				for ($q = 0; $q < $item->quantity; $q++) {
					$vouchers[] = Vouchers::create(array(
						'voucher'      => Products::generateVoucher($website->code_prefix, $campaign->id),
						'product_name' => $item->product->name,
						'extended'     => $item->extended,
					));
				}
			}

			$vouchers = Vouchers::buildSet($vouchers);

			$this->set(array(
				'vouchers' => $vouchers
			));

			return;
		}

		if ($isFree) {
			$success = TRUE;
		} else {
			$this->paypal->getDetails($data['token']);
			$success = $this->paypal->capture();
		}


		if ($success) {
			$order->status = Orders::STATUS_COMPLETED;

			foreach ($order->products as $item) {
				Vouchers::update(array(
					'order_item_id' => $item->id
				), array(
					'product_id'    => $item->product_id,
					'order_item_id' => NULL,
					'extended'      => $item->extended
				), array(
					'limit' => $item->quantity
				));
			}
		}

		if (!$isFree) {
			$order->payer_id = $this->paypal->payerId();
			$order->transaction_id = $this->paypal->transactionId();
		}

		$order->save();

		if ($success) {
			$order->sendEmail();
			$this->set(array(
				'vouchers'   => Vouchers::byOrder($order->id),
				'isCheckout' => TRUE,
			));

			EmailNotifications::sendFor($campaign->id);
		}
	}


	protected function cancel($campaign)
	{
		$this->_render['template'] = $campaign->layout->name . '/cancel';

		$data = $this->request->query + array(
			'token'   => NULL,
			'PayerID' => NULL
		);

		$order = Orders::findFirstByToken($data['token']);
		$message = '';

		if ($order && $order->status == Orders::STATUS_PENDING) {
			$order->status = Orders::STATUS_CANCELED;
			$order->save();
		}

		if ($order) {
			$message = str_replace(
				Orders::$messagesVars,
				array($order->fullName(), $order->email),
				$campaign->error_msg
			);
		}

		$this->set(compact('message'));
	}
}

?>