<?php

namespace app\models;

class Model extends \lithium\data\Model
{
  const DEFAULT_COUNT  = 100;

	/**
	 * Determines if any fields were actually modified, to prevent making a pointless query
	 */
	public function hasChanged($entity, array $options = array())
	{
		$values = $entity->export();
		
		foreach ($values['update'] as $key => $val) {
			if (!isset($values['data'][$key]) || $values['data'][$key] != $val) {
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	
	/**
	 * Execute a raw SQL query, using PDO's prepared statements
	 */
	public static function executeQuery($query, $params = array())
	{
		reset($params);
		$first = current($params);
		
		if (is_array($first)) {
			$fields = array();
			$values = array();
			
			foreach ($params as $setKey => $set) {
				$fieldSet = array();
				foreach ($set as $key => $p) {
					$fieldSet[] = ':v_' . $setKey . '_' . $key;
					$values[':v_' . $setKey . '_' . $key] = $p;
				}
				$fields[] = '(' . implode(', ', $fieldSet) . ')';
			}
			
			$query  = str_replace('{:values}', implode(', ', $fields), $query);
			$params = $values;
		}
		
		$lastKey = array_keys($params);
		$lastKey = array_pop($lastKey);
		
		if (is_int($lastKey)) {
			$params = array_values($params);
		}
		
		$conn = static::connection()->connection;
		$stmt = $conn->prepare($query);
		
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($params);
		
		return $stmt;
	}
	
	
	public static function lastInsertId()
	{
		$conn = static::connection()->connection;
		return $conn->lastInsertId();
	}
	
	
	public static function buildSet($dataSet = array(), $model = NULL)
	{
		$model = $model ?: get_called_class();
		$idKey = $model::meta('key');
		$conn  = $model::connection();
		
		$set = $conn->invokeMethod('_instance', array('set', compact('model')));
		
		foreach ($dataSet as $key => $data) {
			if (is_array($data)) {
				$offset = isset($data[$idKey]) ? $data[$idKey] : $key;
				$item   = $conn->invokeMethod('_instance', array('entity', compact('data', 'model')));
			} else {
				$offset = $data->{$idKey};
				$item   = $data;
			}
			
			$set->offsetSet($offset, $item);
		}
		
		return $set;
	}
	
	
	/**
	 * Return just the values of a specific column, without field names
	 */
	public function values($entity, $field = NULL)
	{
		$data  = (array) $entity->data() ?: array();
		$key   = isset($data[$field]) ? $field : key($data);
		$value = isset($data[$field]) ? $data[$field] : array_shift($data);
		
		return $value;
	}
	
	
	/**
	 * Sets item status to 0 (deleted) and changes the following items' order
	 */
	public static function softDelete($conditions, $params = array())
	{
		$params += array(
			'flag'  => 'status',
			'key'   => 'id',
			'value' => 0
		);
		
		if (!is_array($conditions)) {
			$conditions = array($params['key'] => $conditions);
		}
		
		$result = static::update(array(
			$params['flag'] => $params['value']
		), $conditions);
		
		return $result;
	}
	
	
	// set `created` and `modified` values for all entities that support it
	public function save($entity, $data = NULL, array $options = array())
	{
		$NOW = gmdate('Y-m-d H:i:s');
		
		if (!$entity->exists() && $entity->schema('created')) {
			$entity->created = $NOW;
		}
		
		if ($entity->schema('modified')) {
			$entity->modified = $NOW;
		}
		
		return parent::save($entity, $data, $options);
	}
}


?>