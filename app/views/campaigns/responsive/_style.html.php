<style type="text/css">
  body { font: normal 12px/1.3em Arial, Helvetica, sans-serif; }
  
  body > div { position: relative; width: 90%; margin: 0 5%; min-width: 274px; }
  
  .logo { text-align: center; padding: 0 0 10px; margin-bottom: 10px; border-bottom: 1px solid #d9d6cf; }
  .logo img { max-width: 100%; }
  
  .campaign-wrapper { position: relative; width: 100%; max-width: 800px; margin: 0 auto; padding: 40px 0; }
  .campaign-wrapper h1{ margin: 0 0 20px 0; font-size: 30px; line-height: 1.3em; }

  .campaign-splash  { text-align: center; }
  .campaign-splash img    { max-width: 800px; width: 100%; border: 0;}

  .campaign-customer-and-submit{  }

  .campaign-sidebar { position: relative; width: 100%; text-align: center; margin: 0 0 20px; }
  .campaign-sidebar img { width: 100%; max-width: 320px; }
  
  .campaign-countdown     { width: 170px; margin: 0 auto 20px; padding: 20px 0 20px 100px; font-size: 16px; text-align: center; /*border: 1px solid #333;*/ background: url(<?=$this->url('/img/clock.png')?>) left center no-repeat; }
  .campaign-countdown-row     { text-align: center; }
  .campaign-countdown-column  { display: inline-block; vertical-align: top; }
  .campaign-countdown-block   { margin: 0 2px; font-size: 14px; }
  .campaign-countdown-digits  { color: #ff0000; font-size: 22px; }
  .campaign-countdown h2{ font-size: 20px; color: #01a5e2; text-transform: uppercase; margin: 0 0 10px 0; }

  .campaign-details-image { width: 320px; }

  .campaign-header { margin: 0 0 20px 0; }

  .campaign-form { /*width: 400px; padding: 20px; background: #fff; */}
  .campaign-form fieldset { border: 0; padding: 20px 0; margin: 20px 0; }
  .campaign-form legend { display: block; width: 100%; border-bottom: 2px solid #47c550; }

  .campaign-buyer-details label { display: inline-block; width: 110px; text-align: right; margin-right: 10px; }
  .campaign-customer-and-submit input {
    width: 250px; background: #fafafa; color: #333; border: 1px solid #999; padding: 5px;
    -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;
  }
  .campaign-customer-and-submit input:focus { border-color: #47c550; background: #fff; }

  .campaign-products-list{ width: 100%; position: relative; }
  .campaign-remaining-qty { display: block; font-size: 12px; color: #aaa; }
  .campaign-product-sold-out { display: block; font-size: 12px; color: #c00; font-weight: bold; margin: 0 0 10px; }
  /*
  .campaign-product-name  { display: inline-block; width: 320px; float: left; padding: 0 5px; }
  .campaign-remaining-qty { display: block; font-size: 12px; color: #aaa; }
  .campaign-product-sold-out { display: block; font-size: 12px; color: #c00; font-weight: bold; margin: 0 0 10px; }
  .campaign-product-qty   { float: right; text-align: center; width: 60px; padding: 0 5px; }
  .campaign-product-price { display: block; font-size: 12px; margin: 5px 0 0; }
  */

  .campaign-products-list table{ border-collapse: collapse; border-spacing: 0; width: 100%; }

  .campaign-product-header{ background: url(<?=$this->url('img/background-quantity-header.jpg')?>) top left repeat-x; }
  .campaign-product-header th { color: #ffffff; padding: 6px; font-weight: normal; }

  .campaign-product-header .campaign-product-total{ border-right: 1px solid #968f85; }

  .campaign-product-row{ border-bottom: 1px solid #d9d6cf; }
  .campaign-product-row td{ padding: 12px 6px; }

  .campaign-product-name  { width: 47%; text-align: left; }
  .campaign-product-qty   { width: 15%; border-left: 1px solid #968f85; text-align: center; }
  .campaign-product-price { width: 15%; border-left: 1px solid #968f85; text-align: center; }
  .campaign-product-total { width: 15%; border-left: 1px solid #968f85; text-align: center; }

  .campaign-product-row .campaign-product-name,
  .campaign-product-row .campaign-product-price{
    background: #f6f6f6;
  }

  .campaign-product-row .campaign-product-name,
  .campaign-product-row .campaign-product-qty,
  .campaign-product-row .campaign-product-price,
  .campaign-product-row .campaign-product-total{
    border-left: 1px solid #d9d6cf;
  }

  .campaign-product-row .campaign-product-total{
    border-right: 1px solid #d9d6cf;
  }

  .campaign-product-row .campaign-product-qty .btn{
    background: url(<?=$this->url('/img/background-quantity-header.jpg')?>) top left repeat-x;
    text-decoration: none;
    padding: 6px;
    font-size: 12px;
    line-height: 12px;
    color: #ffffff;
    width: 10px;
    display: block;
    margin: 0 auto;
    position: relative;
  }
  
  .btn-increment {
    top: -60px;
  }
  
  .btn-decrement {
    top: 60px;
  }

  .campaign-product-row .campaign-product-qty input{
    text-align: center;
    border-radius: 0;
    padding: 4px 0 5px 0;
    width: 30px;
    height: 30px;
    background: transparent;
    border: none;
    font-weight: bold;
    display: block;
    margin: 0 auto;
  }

  .campaign-order-errors  {
    padding: 10px; margin: 0 0 20px; border: 1px solid #900; background: #fee; color: #955; font-size: 14px; background-clip: padding-box;
    -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;
  }

  .campaign-form-actions { text-align: center; }
  .campaign-form-actions button {
    width: 200px; height: 50px; text-indent: -9999px; text-align: left;
    padding: 0; border: 0;
    cursor: pointer;
    background: url(<?=$this->url('img/buy-now.png')?>) top left no-repeat;
  }

  .campaign-form-actions button:hover { background-position: bottom left; }

  .campaign-form-row { padding: 5px 0; max-width: 480px; margin: 0 auto; }
  .campaign-form-row:hover { background: #f3f3f3; }

  .campaign-thank-you-msg { margin: 0 0 20px; }
  .campaign-cancel-msg { margin: 0 0 20px; }

  .clearfix:before, .clearfix:after { content: ""; display: table; }
  .clearfix:after { clear: both; }
  .clearfix { zoom: 1; }

  .email-confirmation-error{
    color: #ff0000;
    width: 260px;
    margin: 0 0 0 124px;
    display: none;
  }

  .vouchers-listing { margin: 40px 0 0; }

  .extend-voucher { border-top: 1px solid #D9D6CF; margin: 10px 0 0; padding: 5px 5px 0; }
  .extend-voucher .checkbox { margin: 5px 0; font-size: 11px; }
  
  .checkbox { cursor: pointer; display: inline-block; }
  .checkbox input { vertical-align: middle; margin-top: -2px; }
  .checkbox .note { font-style: italic; }
  .checkbox .important { color: #c00; font-style: italic; }
  
  @media only screen and (max-width: 479px) {
    .campaign-buyer-details label {
      text-align: left;
    }
    
    .campaign-customer-and-submit input {
      width: 100%;
      
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
  }
</style>