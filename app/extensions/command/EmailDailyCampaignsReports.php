<?php

namespace app\extensions\command;

use lithium\analysis\Logger;

use \app\models\Campaigns;

class EmailDailyCampaignsReports extends \lithium\console\Command
{
  public function run() {
    Campaigns::processDaily();
  }
}
