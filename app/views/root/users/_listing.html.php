<table class="table table-striped">
	<thead>
		<tr>
			<th class="col-check"><input type="checkbox" name="check-all" data-target="users[]" value="0" /></th>
			<th>User</th>
			<th class="col-updated">Updated</th>
		</tr>
	</thead>
	
	<tbody>
		<?php 
		foreach ($admins as $user) :
			$updated = date('M jS Y', strtotime($user->modified));
		?>
		<tr>
			<td class="col-check"><input type="checkbox" name="users[]" value="<?=$user->id?>" /></td>
			<td><a href="<?=$this->url(array('Users::edit', 'id' => $user->id))?>"><?=$user->email?></a></td>
			<td class="col-updated"><?=$updated?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>