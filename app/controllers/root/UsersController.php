<?php

namespace app\controllers\root;

use app\models\Users;
use li3_extras\storage\Session;
use lithium\action\DispatchException;

class UsersController extends IndexController
{
	public function index()
	{
		$page   = @$this->request->params['page'];
		$limit  = Users::DEFAULT_COUNT;
		$admins = Users::admins(compact('page', 'limit'));
		
		$deletedUsers = Session::flash('flash.usersDeleted');
		
		if ($admins->pages < $page) {
			return $this->redirect(array('Users::index', 'page' => $admins->pages));
		}
		
		return compact('admins', 'deletedUsers');
	}
	
	
	public function add()
	{
		$this->set(array(
			'errors'  => array(),
			'success' => Session::flash('flash.userAdded')
		));
		
		if (!$this->request->is('post')) {
			return;
		}
		
		$user = Users::create($this->request->data);
		$user->role = Users::ROLE_ADMIN;
		
		if ($user->save()) {
			Session::write('flash.userAdded', TRUE);
			return $this->redirect('Users::add');
		}
		
		$this->set(array(
			'errors' => $user->errors()
		));
	}
	
	
	public function edit()
	{
		$this->set(array(
			'errors'  => array(),
			'success' => Session::flash('flash.userUpdated')
		));
		
		$userId = @$this->request->params['id'];
		$user   = $userId ? Users::admin($userId) : NULL;
		
		if (!$user) {
			throw new DispatchException('Could not route request.');
		}
		
		$this->set(array('editUser' => $user));
		
		if (!$this->request->is('post')) {
			return;
		}
		
		$data = array_intersect_key($this->request->data, array('password' => '', 'confirm' => ''));
		$user->set($data);
		
		if ($user->save()) {
			Session::write('flash.userUpdated', TRUE);
			return $this->redirect(array('Users::edit', 'id' => $user->id));
		}
		
		$this->set(array(
			'errors' => $user->errors()
		));
	}
	
	
	public function delete()
	{
		$users = array();
		
		if (isset($this->request->data['users'])) {
			$users = (array) $this->request->data['users'];
		}
		
		if ($users) {
			Users::softDelete(array('id' => $users));
			Session::write('flash.usersDeleted', TRUE);
		}
		
		return $this->redirect('Users::index');
	}
}

?>