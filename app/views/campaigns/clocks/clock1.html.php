<?php

$timezone    = new DateTimeZone('GMT');
$currentDate = new DateTime('NOW', $timezone);
$endDate     = new DateTime($campaign->end_date, $timezone);

$countdown   = $endDate->diff($currentDate);
$countdown   = $countdown->invert ? $countdown : $currentDate->diff($currentDate);

$difference  = max(0, $endDate->getTimestamp() - $currentDate->getTimestamp());

?>

<div class="campaign-countdown">
  <h2>Time remaining</h2>
  <div class="campaign-countdown-label">
    <div class="campaign-countdown-row clearfix">
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits hours">--</div>
        <div class="campaign-countdown-block">Hrs</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits">:</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits minutes">--</div>
        <div class="campaign-countdown-block">Mins</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits">:</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits seconds">--</div>
        <div class="campaign-countdown-block">Secs</div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  jQuery( document ).ready(function($)
  {
      var remaining   = <?=$difference?>;
      var one_day     = 24 * 3600;

      var division_modulo   = remaining % one_day;
      var division_integer  = Math.floor(remaining / one_day);

      var tick = function()
      {
          remaining -= 1;

          if(!remaining){
            clearInterval(tick_interval);
          }

          division_modulo   = remaining % one_day;
          division_integer  = Math.floor(remaining / one_day);

          if(division_integer >= 1)
          {
              division_modulo += one_day;
          }

          var seconds   = Math.max(division_modulo, 0);
          var minutes   = Math.max(Math.floor(seconds/60), 0);
          var hours     = Math.max(Math.floor(minutes/60), 0);

          seconds %= 60;
          minutes %= 60;

          $('.campaign-countdown-label .hours').html(addZero(hours));
          $('.campaign-countdown-label .minutes').html(addZero(minutes));
          $('.campaign-countdown-label .seconds').html(addZero(seconds));
      }

      var addZero = function(number)
      {
          if(number < 10)
          {
              return '0' + number;
          }

          return number;
      }

      tick();
      var tick_interval = setInterval(tick, 1000);

  });

</script>