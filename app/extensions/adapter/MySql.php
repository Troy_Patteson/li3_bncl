<?php

namespace app\extensions\adapter;

class MySql extends \lithium\data\source\database\adapter\MySql {
	
	public function __construct(array $config = array()) {
		parent::__construct($config);
		$this->_strings['update'] = "UPDATE {:source} SET {:fields} {:conditions} {:limit};{:comment}";
	}
}

?>