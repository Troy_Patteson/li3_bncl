<form id="campaignFilters" action="<?=$this->url('Campaigns::filter')?>" method="post">
	<h5>FILTERS</h5>

	<select name="website_id">
		<option value="0">All websites</option>

		<?php foreach ($websites as $website) : ?>
		<option <?php echo $website->id == $filters['website_id'] ? 'selected="selected"' : ''?> value="<?=$website->id?>"><?=$website->host?></option>
		<?php endforeach; ?>
	</select>

	<br />

	<select name="owner_id">
		<option value="0">All users</option>

		<?php foreach ($users as $user) : ?>
		<option <?php echo $user->id == $filters['owner_id'] ? 'selected="selected"' : ''?> value="<?=$user->id?>"><?=$user->email?></option>
		<?php endforeach; ?>
	</select>

	<br />

	<label class="checkbox">
		Show only active deals
		<input <?php echo !empty($filters['active']) ? 'checked="checked"' : ''?> type="checkbox" name="active" value="1" />
	</label>
</form>
