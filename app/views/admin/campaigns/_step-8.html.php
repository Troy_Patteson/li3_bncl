<?php

use \app\models\Orders;

$emailTemplate = $campaign->email_template ?: $website->email_template;

?>
	<div class="control-group">
		<label class="control-label" for="inEmailTemplate">Email template</label>
		<div class="controls">
			<textarea class="input-block-level" rows="10" id="inEmailTemplate" name="email_template"><?=$emailTemplate?></textarea>
			<small class="muted">Available variables are: <code><?php echo implode('</code>, <code>', Orders::$emailVars)?></code></small>
			
			<div class="pull-right">
				<button class="btn btn-preview-email" type="button"><i class="icon-eye-open"></i></button>
				<button class="btn btn-attach email-preview" type="button" data-url="<?=$this->url(array('Campaigns::preview_email', 'id' => $campaign->id))?>"><i class="icon-envelope"></i></button>
			</div>
		</div>
	</div>
	
	
	<div class="tpl tpl-email-preview">
		<label class="control-label">Send email preview to:</label>
		<input class="input-large" type="text" name="email" value="<?=$user['email']?>" />
	</div>