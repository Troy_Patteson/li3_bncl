if FileTest.directory?('/vagrant/app/resources/tmp')
  Factor.add('storage_directory_exists') do
    setcode { true }
  end
end
