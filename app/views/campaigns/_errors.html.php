<?php if ($errors) : ?>
	
	<div class="campaign-order-errors" id="errorsMsg">
		<?php
		foreach ($errors as $error) :
			echo implode('<br />', $error) . '<br />';
		endforeach;
		?>
	</div>
	
<?php endif; ?>