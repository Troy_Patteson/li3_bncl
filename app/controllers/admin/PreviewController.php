<?php

namespace app\controllers\admin;

use lithium\core\Libraries;
use lithium\data\Connections;

class PreviewController extends IndexController
{
	public function _init()
	{
		parent::_init();
		
		$this->_render['layout'] = 'default';
		$this->_render['controller'] = 'campaigns';
	}
	
	
	public function render(array $options = array()) {
		return parent::render(array(
			'controller' => 'campaigns'
		));
	}
	
	
	public function index($action = NULL) {
		$controller = Libraries::instance('controllers', 'Campaigns', array(
			'request' => $this->request,
		));
		
		$controller->sandboxed = TRUE;
		$controller->testing   = TRUE;
		
		echo $controller($this->request, $this->request->params);
		exit();
	}
}

?>