<?php

namespace app\models;

use lithium\analysis\Logger;

use \lithium\net\http\Router;
use \lithium\action\Request;
use \lithium\net\http\Media;

use \li3_swiftmailer\mailer\Transports;
use \li3_swiftmailer\mailer\Message;
use \Swift_Attachment;

use \DateTime;
use \DateTimeZone;

use \Exception;

class Campaigns extends Model
{
	const STATUS_DELETED    = 0;
	const STATUS_ACTIVE     = 1;
	const STATUS_INCOMPLETE = 2;
	const STATUS_ENDED      = 3;

  const CLOCK_CLASIC      = 0;
  const CLOCK_48_TO_24    = 1;

	protected $_meta = array(
		'key'    => 'id',
		'source' => 'campaigns',
		'locked' => TRUE,
	);

	protected $_schema = array(
		'id'             => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'owner_id'       => array('type' => 'integer',  'null' => FALSE),
		'website_id'     => array('type' => 'integer',  'null' => FALSE),
		'title'          => array('type' => 'string',   'null' => FALSE),
		'slug'           => array('type' => 'string',   'null' => FALSE),
		'system_title'   => array('type' => 'string',   'null' => FALSE),
		'start_date'     => array('type' => 'datetime', 'null' => FALSE),
		'end_date'       => array('type' => 'datetime', 'null' => FALSE),
		'layout_id'      => array('type' => 'integer',  'null' => FALSE),
    'clock'          => array('type' => 'integer',  'null' => FALSE, 'default' => 0),
		'landing_image'  => array('type' => 'integer',  'null' => FALSE),
		'details_image'  => array('type' => 'integer',  'null' => FALSE),
		'header'         => array('type' => 'text' ,    'null' => TRUE),
		'footer'         => array('type' => 'text' ,    'null' => TRUE),
		'email_template' => array('type' => 'text',     'null' => FALSE),
		'thank_you_msg'  => array('type' => 'string',   'null' => FALSE),
		'error_msg'      => array('type' => 'string',   'null' => FALSE),
		'use_rakuten'    => array('type' => 'integer', 'null' => FALSE, 'default' => 0),
		'tracking'       => array('type' => 'text',     'null' => TRUE),
    'created'        => array('type' => 'datetime', 'null' => FALSE),
		'modified'       => array('type' => 'datetime', 'null' => FALSE),
		'status'         => array('type' => 'integer',  'null' => FALSE, 'default' => 2),
    'daily_report'   => array('type' => 'integer', 'null' => TRUE, 'default' => NULL)
	);

	public $belongsTo = array(
			'Website' => array(
				'to'  => 'app\models\Websites',
				'key' => 'website_id'
			)
		, 'Layout' => array(
					'to'  => 'app\models\Layouts'
				, 'key' => 'layout_id'
			)
	);

	public $hasMany = array(
		'Products' => array(
			//'to'  => 'app\models\Websites',
			'key' => array('id' => 'campaign_id'),
			'constraints' => array('Products.status' => Products::STATUS_ACTIVE)
		)
	);

	public $hasOne = array(
		'LandingImage' => array(
			'to'  => 'app\models\Images',
			'key' => array('landing_image' => 'id')
		),

		'DetailsImage' => array(
			'to'  => 'app\models\Images',
			'key' => array('details_image' => 'id')
		),
	);

	public $validates = array(
		'website_id' => array(
			array('validWebsite', 'message' => 'You specified an invalid website', 'required' => TRUE),
		),

		'layout_id' => array(
			array('validLayout', 'message' => 'You have specified an invalid website', 'required' => TRUE),
		),

    'clock' => array(
      array('validClock', 'message' => 'You have specified an invalid clock', 'required' => TRUE),
    ),

		'title' => array(
			array('notEmpty', 'message' => 'You specified an invalid campaign title', 'required' => TRUE),
		),

		'system_title' => array(
			array('notEmpty', 'message' => 'You specified an invalid campaign system title', 'required' => TRUE),
		),

		'start_date' => array(
			array('validStartDate', 'message' => 'Invalid start date', 'required' => TRUE),
		),

		'end_date' => array(
			array('validEndDate', 'message' => 'Invalid end date', 'required' => TRUE),
		),

		'thank_you_msg' => array(
			array('notEmpty', 'message' => 'The "Thank you" message can\'t be empty', 'required' => TRUE),
		),

		'error_msg' => array(
			array('notEmpty', 'message' => 'The "Errors" message can\'t be empty', 'required' => TRUE),
		),
	);

	public static $thankYouMessage = 'Thank you {--customerName--}.<br />An email has been sent to {--customerEmail--}. Your unique voucher code/s are:';
	public static $errorMessage = 'Sorry {--customerName--}, there has been a problem. Could you try again please.';
  public static $footer = '<span style="font-size:11px;">Offer excludes shipping. Standard shipping starts from $4.95 for Photo Block & Photo Book payable at redemption of voucher and $9.95 for Canvas.</span>';


	public static function create(array $data = array(), array $options = array())
	{
		$entity = parent::create($data, $options);
		$entity->thank_you_msg = $entity->thank_you_msg ?: static::$thankYouMessage;
		$entity->error_msg     = $entity->error_msg ?: static::$errorMessage;
    $entity->footer        = $entity->footer ?: static::$footer;

		return $entity;
	}


	public static function listing($params)
	{
		$params += array(
			'conditions' => array(),
			'page'       => 1,
			'limit'      => static::DEFAULT_COUNT,
			'order'      => array('created' => 'DESC'),
			'with'       => array('Website')
		);

		$params['page']  = max(1, (int) $params['page']);
		$params['limit'] = max(1, (int) $params['limit']);
		$total = static::count($params['conditions']);
		$pages = ceil($total / $params['limit']);

		if (!$total) {
			$items = static::buildSet();
			$items->pages = 0;

			return $items;
		}

		$items = static::all($params);
		$items->pages = $pages;

		return $items;
	}


	public static function completeDetails($params)
	{
		$params += array(
			'id'   => NULL,
			'slug' => FALSE
		);

		$conditions = array('id' => $params['id']);

		if ($params['slug'] !== FALSE) {
			$conditions['slug'] = $params['slug'];
		}

		$campaign = Campaigns::first(array(
			'conditions' => $conditions,

			'with' => array(
				'Website',
				'Layout',
				'Products',
				'LandingImage',
				'DetailsImage',
			)
		));

		if (!$campaign) {
			return NULL;
		}

		if (is_callable(array($campaign->products, 'values'))) {
			$availability = Products::availability($campaign->products->values('id'));

			foreach ($availability as $item) {
				$product = $campaign->products->offsetGet($item->id);

				if ($product) {
					$product->remaining = $item->remaining;
				}
			}
		}

		return $campaign;
	}


  public static function processDaily()
  {
      $campaigns = static::all(array(
          'conditions' => array(
              'daily_report'  => 1
            , 'status'        => static::STATUS_ACTIVE
          )
        , 'with' => array('Website')
      ));

      foreach ($campaigns as $campaign)
      {
          $campaign->emailDailyReport();
      }
  }


  public function emailDailyReport($entity)
  {
      $emails = EmailReports::findAllByCampaignId($entity->id);
      $report = Campaigns::report(array(
          'campaign'  => $entity
        , 'website'   => $entity->website
        , 'all'       => TRUE
      ));

      $tmp = fopen('php://temp', 'r+');

      foreach ($report['orderItems'] as $item) {
          $date = new DateTime($item->order_date, new DateTimeZone('GMT'));
          $date->setTimezone(new DateTimeZone('Australia/Sydney'));

          fputcsv($tmp, array(
              $item->formatCode()
            , $item->transaction_id
            , $item->product_name
            , $item->first_name
            , $item->last_name
            , $item->email
            , $item->phone
            , $date->format('M dS Y h:i A')
          ));
      }

      $data = '';

      rewind($tmp);
      while (!feof($tmp)) {
          $data .= fread($tmp, 8192);
      }

      fclose($tmp);

      $filename = preg_replace('/[^-_\.\(\)a-zA-Z0-9]/', '-', $entity->title);
      $filename = preg_replace('/-+/', '-', $filename);
      $filename.= '.csv';

      $params = array('campaign' => $entity);

      $renderer = Media::view('default', $params);
      $msgBody = $renderer->render('all', $params, array(
          'controller'  => 'campaigns'
        , 'template'    => '_daily_email_reports'
        , 'handler'     => 'html'
      ));

      $attachment = Swift_Attachment::newInstance()
          ->setFilename($filename)
          ->setContentType('text/css')
          ->setBody($data);

      $mailer   = Transports::adapter('default');
      $message  = Message::newInstance()
          ->setSubject('Campaign Daily Report: ' . $entity->title)
          ->setFrom(array('no-reply@' . $entity->website->host => $entity->website->host))
          ->setBody(wordwrap(strip_tags(preg_replace('/<br(.*?)>/', "\n", $msgBody))))
          ->addPart($msgBody, 'text/html')
          ->attach($attachment);

      foreach ($emails as $email) {
          $message->setTo(array($email->address));
          $mailer->send($message);
      }
  }


	public static function processEnding()
	{
		$NOW = gmdate('Y-m-d H:i:s');

		$campaigns = static::all(array(
			'conditions' => array(
				'end_date' => array('<=' => $NOW),
				'status'   => static::STATUS_ACTIVE
			),

			'with' => array('Website')
		));

		foreach ($campaigns as $campaign) {
			$campaign->markAsEnded();
		}
	}


	public function markAsEnded($entity)
	{
		$emails = EmailReports::findAllByCampaignId($entity->id);
		$report = Campaigns::report(array(
			'campaign' => $entity,
			'website'  => $entity->website,
			'all'      => TRUE
		));

		$tmp = fopen('php://temp', 'r+');

		foreach ($report['orderItems'] as $item) {
          		$date = new DateTime($item->order_date, new DateTimeZone('GMT'));
          		$date->setTimezone(new DateTimeZone('Australia/Sydney'));

			fputcsv($tmp, array(
				$item->formatCode(),
				$item->transaction_id,
				$item->product_name,
				$item->first_name,
				$item->last_name,
				$item->email,
				$item->phone,
				$date->format('M dS Y h:i A'),
			));
		}

		$data = '';

		rewind($tmp);
		while (!feof($tmp)) {
			$data .= fread($tmp, 8192);
		}

		fclose($tmp);

		$filename = preg_replace('/[^-_\.\(\)a-zA-Z0-9]/', '-', $entity->title);
		$filename = preg_replace('/-+/', '-', $filename);
		$filename.= '.csv';

		$params = array('campaign' => $entity);

		$renderer = Media::view('default', $params);
		$msgBody  = $renderer->render('all', $params, array(
			'controller' => 'campaigns',
			'template'   => '_email_reports',
			'handler'    => 'html'
		));

		$attachment = Swift_Attachment::newInstance()
			->setFilename($filename)
			->setContentType('text/csv')
			->setBody($data);

		$mailer  = Transports::adapter('default');
		$message = Message::newInstance()
			->setSubject('Campaign Ending Report: ' . $entity->title)
			->setFrom(array('no-reply@' . $entity->website->host => $entity->website->host))
			->setBody(wordwrap(strip_tags(preg_replace('/<br(.*?)>/', "\n", $msgBody))))
			->addPart($msgBody, 'text/html')
			->attach($attachment);

		foreach ($emails as $email) {
			$message->setTo(array($email->address));
			$mailer->send($message);
		}

		$entity->status = static::STATUS_ENDED;
		$entity->save(array(), array('validate' => FALSE));
	}


	public function isVisible($entity)
	{
		$timezone = new DateTimeZone('GMT');
		$currentDate = new DateTime('now', $timezone);

		if($entity->status != static::STATUS_ACTIVE && $entity->status != static::STATUS_ENDED)
		{
			return FALSE;
		}

		try {
			$campaignStart = new DateTime($entity->start_date, $timezone);
		} catch(Exception $e) {
			return FALSE;
		}

		return $campaignStart <= $currentDate;
	}


	public function hasEnded($entity)
	{
		$timezone = new DateTimeZone('GMT');
		$currentDate = new DateTime('now', $timezone);
		try {
			$campaignEnd = new DateTime($entity->end_date, $timezone);
		} catch(Exception $e) {
			return TRUE;
		}

		return $campaignEnd < $currentDate;
	}


	public function soldOut($entity)
	{
		$products = $entity->products ?: array();

		foreach ($products as $product) {
			if ($product->remaining > 0) {
				return FALSE;
			}
		}

		return TRUE;
	}


	public function setDates($entity, $website = NULL)
	{
		if (!$website && !$entity->id && !$entity->website) {
			return FALSE;
		}

		try {
			$website  = $website ?: $entity->website ?: Websites::first($entity->website_id);
			$timezone = $website ? new DateTimeZone($website->timezone) : NULL;
			$gmtZone  = new DateTimeZone('GMT');
		} catch (Exception $e) {
			return FALSE;
		}

		$entity->website = $website;

		foreach (array('start_date', 'end_date') as $dateField) {
			try {
				$localField = 'local_' . $dateField;
				$localDate  = $entity->$localField ?: 'trigger exception';
				$gmtDate    = new DateTime($localDate, $timezone);

				$gmtDate->setTimezone($gmtZone);

				$entity->{$dateField}  = $gmtDate->format('Y-m-d H:i:s');
				$entity->{$localField} = NULL;
			} catch (Exception $e) {}
		}
	}


	public function localDate($entity, $type)
	{
		if (!$entity->id && !$entity->website) {
			return FALSE;
		}

		$entity->website = $entity->website ?: Websites::first($entity->website_id);

		try {
			$locale = new DateTimeZone($entity->website->timezone);
		} catch (Exception $e) {
			$locale = new DateTimeZone('GMT');
		}

		$gmt    = new DateTimeZone('GMT');
		$date   = $entity->{$type};

		try {
			$date = new DateTime($date, $gmt);
			$date->setTimezone($locale);
			$date = $date->format('m/d/Y');
		} catch (Exception $e) {}

		return $date;
	}


	public function previewUrl($entity, $context = NULL)
	{
		if (!$entity->id && !$entity->website) {
			return FALSE;
		}

		$entity->website = $entity->website ?: Websites::first($entity->website_id);

		return Router::match(array(
			'Preview::index',
			'slug' => $entity->slug,
			'id'   => $entity->id
		), $context);
	}


	public function publicUrl($entity, $context = NULL)
	{
		if (!$entity->id && !$entity->website) {
			return FALSE;
		}

		$entity->website = $entity->website ?: Websites::first($entity->website_id);

		return Router::match(array(
			'Campaigns::index',
			'slug' => $entity->slug,
			'id'   => $entity->id
		), $context, array(
			'absolute' => TRUE,
			'scheme'   => 'http://',
			'host'     => Websites::SUBDOMAIN . '.' . $entity->website->host
		));
	}


	public function editUrl($entity, $step = NULL)
	{
		if (!$entity->id && !$entity->website) {
			return FALSE;
		}

		$entity->website = $entity->website ?: Websites::first($entity->website_id);
		$context = new Request();

		return Router::match(array(
			'Campaigns::edit',
			'admin' => 'admin',
			'id'    => $entity->id,
			'args'  => $step ? 'step-' . $step : NULL
		), $context, array(
			'absolute' => TRUE
		));
	}


	public static function report($params)
	{
		$params += array(
			'campaign' => NULL,
			'website'  => NULL,
			'page'     => 1,
			'limit'    => static::DEFAULT_COUNT,
			'all'      => FALSE
		);

		extract($params);

		$query = array(
			'conditions' => array(
				'Orders.status' => Orders::STATUS_COMPLETED,
			)
		);

		if (!empty($campaign)) {
			$query['conditions']['Orders.campaign_id'] = $campaign->id;
		}

		if (!empty($conditions)) {
			$query['conditions'] += $conditions;
		}

		$query += array(
			'fields' => array(
				'Vouchers' => array('voucher'),
				array('Orders.transaction_id AS transaction_id'),
				array('Orders.first_name AS first_name'),
				array('Orders.last_name AS last_name'),
				array('Orders.email AS email'),
				array('Orders.phone AS phone'),
				array('Orders.created AS order_date'),

				array('OrdersItems.quantity AS quantity'),
				array('OrdersItems.price AS price'),
				array('OrdersItems.extended AS extended'),
				array('Products.name AS product_name'),
				array('Campaigns.title AS campaign_title'),
				array('Websites.host AS website_host'),
				array('Websites.timezone AS website_timezone'),
				array('Websites.currency_abbreviation AS currency_abbreviation'),
				array('Websites.currency_symbol AS currency_symbol'),
			),

			'joins' => array(
				array(
					'type'        => 'INNER',
					'source'      => 'orders_items',
					'alias'       => 'OrdersItems',
					'constraints' => 'OrdersItems.id = Vouchers.order_item_id'
				),

				array(
					'type'        => 'INNER',
					'source'      => 'orders',
					'alias'       => 'Orders',
					'constraints' => 'OrdersItems.order_id = Orders.id'
				),

				array(
					'type'        => 'INNER',
					'source'      => 'products',
					'alias'       => 'Products',
					'constraints' => 'OrdersItems.product_id = Products.id'
				),

				array(
					'type'        => 'INNER',
					'source'      => 'campaigns',
					'alias'       => 'Campaigns',
					'constraints' => 'Campaigns.id = Orders.campaign_id'
				),

				array(
					'type'        => 'INNER',
					'source'      => 'websites',
					'alias'       => 'Websites',
					'constraints' => 'Campaigns.website_id = Websites.id'
				),
			),

			'order' => array('Orders.created' => 'DESC', 'Orders.transaction_id' => 'ASC', 'Products.name' => 'ASC')
		);

		$limited = $query + (!$all ? array(
			'page'  => $page,
			'limit' => $limit
		) : array());

		$totalPages = !$all ? ceil(Vouchers::count($query) / $limit) : 1;
		$orderItems = Vouchers::all($limited);

		foreach ($orderItems as $item) {
			try {
				$timezone = new DateTimeZone($item->website_timezone);
			} catch (Exception $e) {
				$timezone = new DateTimeZone('GMT');
			}

			$created = new DateTime($item->order_date, $timezone);
			$item->order_date = $created->format('M jS Y g:i A');
			$item->friendly_price = $item->currency_symbol . number_format($item->price, 2);
		}

		return compact('orderItems', 'totalPages');
	}


	public static function thatHaveExtendedVouchers($ids)
	{
		if(empty($ids))
		{
		    return array();
		}

		$products = Products::all(array(
			'fields' => array(
				'Products' => array('id', 'campaign_id')
			),

			'conditions' => array(
				'Products.campaign_id' => $ids,
				'Products.status'      => Products::STATUS_ACTIVE,
				'Vouchers.extended'    => 1
			),

			'joins' => array(
				array(
					'type'        => 'INNER',
					'source'      => 'vouchers',
					'alias'       => 'Vouchers',
					'constraints' => 'Products.id = Vouchers.product_id'
				),
			),

			'group' => array('Products.campaign_id')
		));

		return $products;
	}
}



?>
