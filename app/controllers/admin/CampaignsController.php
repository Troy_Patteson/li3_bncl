<?php

namespace app\controllers\admin;

use \app\models\Websites;
use \app\models\Users;
use \app\models\Layouts;
use \app\models\Campaigns;
use \app\models\Products;
use \app\models\EmailNotifications;
use \app\models\EmailReports;
use \app\models\Images;
use \app\models\Pdfs;
use \app\models\Vouchers;
use \app\models\Orders;

use li3_extras\storage\Session;
use lithium\core\Libraries;
use lithium\util\Inflector;
use lithium\util\Validator;
use lithium\analysis\Logger;

use \DateTime;
use \DateTimeZone;

class CampaignsController extends IndexController
{
	public function _init()
	{
		parent::_init();

		$this->websites = Websites::forUser($this->user['id']);
		$this->users    = Users::all();
		$this->layouts  = Layouts::all();
		$this->filters  = (array) Session::read('campaignFilters');
		$this->filters += array('website_id' => 0, 'owner_id' => 0, 'active' => 0);
    $this->rakuten  = array_merge_recursive(
        json_decode(RAKUTEN_MEDIAFORGE, true)
      , json_decode(RAKUTEN_LINKSHARE, true)
    );

		$this->set(array(
			'websites' => $this->websites,
			'users'    => $this->users,
			'layouts'  => $this->layouts,
			'filters'  => $this->filters
		));
	}


	public function index($unpublished = FALSE)
	{
		$page  = isset($this->request->params['page']) ? $this->request->params['page'] : 1;
		$limit = Campaigns::DEFAULT_COUNT;
		// $order = array('title' => 'ASC');

		$websiteIds = array_keys($this->websites->data());
		$userIds    = array_keys($this->users->data());
		$conditions = array(
			'website_id'         => $websiteIds ?: 0,
			'Campaigns.owner_id' => $userIds ?: 0,
			'Campaigns.status'   => !$unpublished ? array(Campaigns::STATUS_ACTIVE, Campaigns::STATUS_ENDED) : Campaigns::STATUS_INCOMPLETE
		);

		if (!empty($this->filters['website_id']) && in_array($this->filters['website_id'], $conditions['website_id'])) {
			$conditions['website_id'] = $this->filters['website_id'];
		}

		if (!empty($this->filters['owner_id']) && in_array($this->filters['owner_id'], $conditions['Campaigns.owner_id'])) {
			$conditions['Campaigns.owner_id'] = $this->filters['owner_id'];
		}

		if (!empty($this->filters['active'])) {
			$conditions['start_date'] = array('<' => gmdate('Y-m-d H:i:s'));
			$conditions['end_date'] = array('>' => gmdate('Y-m-d H:i:s'));
		}

		$campaigns   = Campaigns::listing(compact('conditions', 'page', 'limit', 'order'));
		$unpublished = !$unpublished && $websiteIds ? Campaigns::count(array(
			'conditions' => array(
				'website_id' => $websiteIds,
				'owner_id'   => $userIds,
				'status'     => Campaigns::STATUS_INCOMPLETE
			)
		)) : 0;

		$extended = Campaigns::thatHaveExtendedVouchers($campaigns->values('id'));
		foreach ($extended as $item) {
			$campaigns->offsetGet($item->campaign_id)->hasExtended = TRUE;
		}

		$this->set(array(
			'campaigns'   => $campaigns,
			'unpublished' => $unpublished,
			'deleted'     => Session::flash('campaignDeleted'),
			'success'     => array()
		));
	}


	public function unpublished()
	{
		return $this->index(TRUE);
	}


	public function filter()
	{
		$data = $this->request->data + array(
			'website_id' => NULL,
			'owner_id' => NULL,
			'active' => FALSE
		);

		Session::write('campaignFilters', $data);

		return $this->redirect('Campaigns::index');
	}


	public function delete()
	{
		$items = array();

		if (isset($this->request->data['campaigns'])) {
			$items = (array) $this->request->data['campaigns'];
		}

		if ($items) {
			Campaigns::softDelete(array('id' => $items));
			Session::write('campaignDeleted', TRUE);
		}

		$this->request->params += array(
			'id' => 1
		);

		return $this->redirect(array('Campaigns::index', 'page' => $this->request->params['id']));
	}


	public function add($step = NULL)
	{
		if (!$step) {
			$step = 'step-1';
			Session::delete('Campaign');
		}

		return $this->_add_edit('add', $step);
	}


	public function edit($step = NULL)
	{
		if (!$step) {
			$step = 'step-1';
			Session::delete('Campaign');
		}

		if (!isset($this->request->params['id'])) {
			return $this->_notFound();
		}

		$campaign = Campaigns::first($this->request->params['id']);
		if (!$campaign || ($campaign->status != Campaigns::STATUS_ACTIVE && $campaign->status != Campaigns::STATUS_ENDED)) {
			return $this->_notFound();
		}

		Session::write('Campaign.unlocked', 10);

		return $this->_add_edit('edit', $step);
	}


	protected function _add_edit($action, $step = NULL)
	{
		$campaign = NULL;
		$website  = NULL;

		$method = '_' . Inflector::underscore($step);

		if (!method_exists(__CLASS__, $method)) {
			return $this->_notFound();
		}

		if (isset($this->request->params['id'])) {
			$campaign = Campaigns::first($this->request->params['id']);
			$website  = $campaign ? Websites::first($campaign->website_id) : NULL;
			$campaign->set($this->request->data);

			//if (!$website || $website->owner_id != $this->user['id']) {
			if (!$website) {
				return $this->_notFound();
			}
		}

		$campaign = $campaign ?: Campaigns::create($this->request->data);
		$website  = $website ?: $campaign->website_id ? Websites::first($campaign->website_id) : NULL;
		$website  = $website ?: Websites::create();
		$products = $campaign->exists() ? Products::forCampaign($campaign->id) : Products:: buildSet();
		$unlocked = Session::read('Campaign.unlocked') ?: 1;

		$campaign->products = $products;
		$campaign->owner_id = $this->user['id'];

		$this->set(array(
			'website'  => $website,
			'campaign' => $campaign,
			'products' => $products,
			'unlocked' => $unlocked,
			'errors'   => array(),
		));

		if ($this->request->is('post')) {
			$campaign->setDates($website);
			$campaign->validates();
		}

		$current = $this->{$method}($campaign);

		if ($unlocked < $current['step']) {
			return $this->redirect(array('Campaigns::' . $action, 'args' => 'step-' . $unlocked));
		}

		if ($this->request->is('post') && empty($current['errors'])) {
			$campaign->save(array(), array(
				'validate' => FALSE
			));

			if (!empty($current['last'])) {
				Session::delete('Campaign');
				Session::write('Campaign.saved', $campaign->data());

				$redirect = 'Campaigns::index';
			} else {
				$nextStep = $current['step'] + 1;
				$redirect = array(
					'Campaigns::' . $action,
					'id' => $campaign->id,
					'args' => 'step-' . $nextStep
				);

				Session::write('Campaign.unlocked', max($nextStep, $unlocked));
			}

			return $this->redirect($redirect);
		}

		return $current;
	}


	protected function _process_emails($model, $campaign)
	{
		$fields = array('address' => NULL);
		$errors = array();
		$emails = $model::findAllByCampaignId($campaign->id);

		$this->set(compact('emails'));

		if (!$this->request->is('post')) {
			return $errors;
		}

		$data = $this->request->data + array(
			'new-email' => array(),
			'email'     => array(),
		);

		$newAddresses = $model::buildSet();

		$process = function($id, $address) use ($model, $fields, $emails, $campaign, $newAddresses) {
			static $addresses = array();

			$address = trim($address);

			if (in_array($address, $addresses)) {
				return;
			}

			$entity = $id ? $emails->offsetGet($id) : $model::create();
			$entity->set(array(
				'campaign_id' => $campaign->id,
				'address'     => $address
			));
			$newAddresses->offsetSet($id, $entity);

			$entity->validates();
			$errors = array_intersect_key($entity->errors(), $fields);
			$entity->errors($errors);

			$addresses[] = $address;

			return $entity;
		};

		foreach ($data['email'] as $id => $address) {
			$email = $process($id, $address);

			if ($email && $email->errors()) {
				$errors['email'] = array('You have some invalid email addresses');
			}
		}

		foreach ($data['new-email'] as $id => $address) {
			$email = $process(NULL, $address);

			if ($email && $email->errors()) {
				$errors['email'] = array('You have some invalid email addresses');
			}
		}

		if (empty($errors)) {
			$model::remove(array(
				'id' => array('!=' => $newAddresses->values('id') ?: 0),
				'campaign_id' => $campaign->id
			));

			foreach ($newAddresses as $email) {
				$email->save();
			}
		}

		$this->set(array(
			'emails' => $newAddresses
		));

		return $errors;
	}


	protected function _step_1($campaign)
	{
		$step   = 1;
		$fields = array('website_id' => NULL, 'title' => NULL, 'slug' => NULL, 'system_title' => NULL);
		$errors = array_intersect_key($campaign->errors(), $fields);

		return compact('errors', 'step');
	}


	protected function _step_2($campaign)
	{
		$step   = 2;
		$fields = array('start_date' => NULL, 'end_date' => NULL);

		$errors = array_intersect_key($campaign->errors(), $fields);

		return compact('errors', 'step');
	}


	protected function _step_3($campaign)
	{
		$step   = 3;
		$fields = array('host' => NULL);
		$errors = array();

		$this->set(array(
			'landingImage' => $campaign->landing_image ? Images::first($campaign->landing_image) : NULL,
			'detailsImage' => $campaign->details_image ? Images::first($campaign->details_image) : NULL
		));

		if (!$this->request->is('post')) {
			return compact('errors', 'step');
		}

		$data = $this->request->data + array(
			'layout_id'     => NULL,
			'clock'         => NULL,
			'landing_image' => NULL,
			'details_image' => NULL,
			'header'        => NULL,
			'footer'        => NULL
		);

		$get = function($id, $owner) {
			if (empty($id)) {
				return NULL;
			}

			return Images::first(array(
				'conditions' => array(
					'id' => $id,
					//'owner_id' => $owner
				)
			));
		};

		$landingImage = NULL;
		$detailsImage = NULL;

		if (!$landingImage = $get($data['landing_image'], $this->user['id'])) {
			$errors['landing_image'] = array('Invalid landing page image');
		}

		if (!$detailsImage = $get($data['details_image'], $this->user['id'])) {
			$errors['details_image'] = array('Invalid customer details page image');
		}

		$campaign->set(array(
			'landing_image' => $landingImage ? $landingImage->id : $campaign->landing_image,
			'details_image' => $detailsImage ? $detailsImage->id : $campaign->details_image,
		));

		$campaign_errors = $campaign->errors();
		if(!empty($campaign_errors['layout_id']))
		{
			$errors['layout_id'] = $campaign_errors['layout_id'];
		}

		$this->set(compact('landingImage', 'detailsImage'));
		$this->set(array(
        'header'  => $data['header']
      , 'footer'  => $data['footer']
      , 'clock'   => $data['clock']
    ));

		return compact('errors', 'step');
	}


	protected function _step_4($campaign)
	{
		$step   = 4;
		$fields = array('name' => NULL, 'price' => NULL);
		$errors = array();

		if (!$this->request->is('post')) {
			return compact('errors', 'step');
		}

		$data = $this->request->data + array(
			'new-products' => array(),
			'new-prices'   => array(),
			'new-pdfs'     => array(),
			'products'     => array(),
			'prices'       => array(),
			'pdfs'         => array()
		);

		$products = $campaign->products;
		$campaign->products = array();

		$process = function($id, $params) use ($fields, $products, $campaign) {
			static $names = array();

			$params += array(
				'name'       => NULL,
				'price'      => NULL,
				'extendable' => 0,
				'pdf'        => NULL
			);
			extract($params);

			$name   = trim($name);
			$entity = $id ? $products->offsetGet($id) : Products::create();
			$entity->set(array(
				'campaign_id' => $campaign->id,
				'name'        => $name,
				'price'       => $price,
				'extendable'  => $extendable
			));

      $entity->temporary_pdf = $pdf;

			$campaign->products[] = $entity;

			$entity->validates();
			$errors = array_intersect_key($entity->errors(), $fields);

			if (in_array($name, $names)) {
				$errors['name'] = array('Each product must have an unique name');
			} else {
				$names[] = $name;
			}

			$entity->errors($errors);

			return $entity;
		};

		foreach ($data['products'] as $id => $name) {
			$product = $process($id, array(
				'name'       => $name,
				'price'      => $data['prices'][$id],
				'extendable' => empty($data['extendable'][$id]) ? 0 : 1,
				'pdf'        => $data['pdfs'][$id]
			));

			if ($product->errors()) {
				$errors['products'] = array('You have errors in your product listing');
			}
		}

		foreach ($data['new-products'] as $key => $name) {
			$product = $process(NULL, array(
				'name'       => $name,
				'price'      => $data['new-prices'][$key],
				'extendable' => empty($data['new-extendable'][$key]) ? 0 : 1,
				'pdf'        => $data['new-pdfs'][$key]
			));

			if ($product->errors()) {
				$errors['products'] = array('You have errors in your product listing');
			}
		}

		if (empty($campaign->products)) {
			$errors['no_products'] = array('You must add at least one product to your campaign');
		}

		if (empty($errors)) {
			if ($products->count()) {
				Products::softDelete(array(
					'id' => $products->values('id')
				));
			}

			foreach ($campaign->products as $product) {
				$product->save(array(), array(
					'validate' => FALSE
				));

        if(!empty($product->pdf->id) && (($product->temporary_pdf && $product->pdf->id != $product->temporary_pdf) || empty($product->temporary_pdf)))
        {
            Pdfs::remove(array(
                'id' => $product->pdf->id
            ));
        }

        if (!empty($product->temporary_pdf))
        {
            $pdf_entity = Pdfs::first($product->temporary_pdf);
            $pdf_entity->set(array(
                'product_id' => $product->id
            ));
            $pdf_entity->save();
        }
			}
		}

		$this->set(array(
			'products' => Products::buildSet($campaign->products)
		));

		return compact('errors', 'step');
	}


	protected function _step_5($campaign)
	{
		$step     = 5;
		$errors   = array();
		$fields   = array('vouchers' => NULL);
		$products = $campaign->products;
		$buyLimit = array();

		if (!$this->request->is('post')) {
			return compact('errors', 'step');
		}

		if (!empty($this->request->data['buy-limit'])) {
			$buyLimit = $this->request->data['buy-limit'];
		}

		foreach ($products as $product) {
			$product->buy_limit = @$buyLimit[$product->id];
			$product->validates();
			$err = array_intersect_key($product->errors(), $fields);
			$product->errors($err);

			if ($err) {
				$errors['vouchers'] = array('You have some invalid voucher numbers');
			} elseif ($product->hasChanged()) {
				$product->save();
			}
		}

		return compact('errors', 'step');
	}


	protected function _step_6($campaign)
	{
		$step   = 6;
		$model  = Libraries::locate('models', 'EmailNotifications');
		$errors = $this->_process_emails($model, $campaign);

		return compact('errors', 'step');
	}


	protected function _step_7($campaign)
	{
    if ($this->request->is('post'))
    {
      $data = $this->request->data + array(
        'daily_report' => NULL
      );

      $campaign->set($data);
    }
		$step   = 7;
		$model  = Libraries::locate('models', 'EmailReports');
		$errors = $this->_process_emails($model, $campaign);

		return compact('errors', 'step');
	}


	protected function _step_8($campaign)
	{
		$step    = 8;
		$fields  = array('email_template' => NULL);
		$errors  = array_intersect_key($campaign->errors(), $fields);
		$website = $this->_render['data']['website'];

		if ($website->email_template == $campaign->email_template) {
			$campaign->email_template = NULL;
		}

		if ($campaign->email_template && strpos($campaign->email_template, '{--voucherCodes--}') === FALSE) {
			$errors['invalidTemplate'] = array('Your email template must contain the placeholder <b>{--voucherCodes--}</b>');
		}

		return compact('errors', 'step');
	}


	protected function _step_9($campaign)
	{
		$step   = 9;
		$fields = array('thank_you_msg' => NULL, 'error_msg' => NULL);
		$errors = array_intersect_key($campaign->errors(), $fields);

    if ($this->request->is('post')) {
        $data = $this->request->data + array(
          'use_rakuten'   => NULL,
          'tracking'      => NULL,
        );

        if(!empty($data['use_rakuten']))
        {
            $campaign->set(array(
                'use_rakuten' => 1
              , 'tracking'    => NULL
            ));
        }
        else if(!empty($data['tracking']))
        {
            $campaign->set(array(
                'use_rakuten' => 0
              , 'tracking'    => $data['tracking']
            ));
        }
    }

    $this->set(array('rakuten' => $this->rakuten));
		return compact('errors', 'step');
	}


	protected function _step_10($campaign)
	{
		$step   = 10;
		$last   = TRUE;
		$errors = array();

		if ($this->request->is('post')) {
			$campaign->status = Campaigns::STATUS_ACTIVE;
		}

		return compact('errors', 'step', 'last');
	}


	public function upload_image()
	{
		$this->_render['data'] = array();

		if (!$this->request->is('post')) {
			return $this->_notFound();
		}

		$data = $this->request->data + array(
			'image' => NULL
		);

		$image = Images::create($data['image']);

		if ($image->validates()) {
			$image->owner_id = $this->user['id'];
			$image->save();
		}

		// because IE < 10 can't handle proper JSON headers
		die(json_encode(array(
			'file'   => $image->id ?: NULL,
			'errors' => $image->errors() ?: NULL
		)));

		/*
		$this->render(array(
			'type' => 'json',
			'data' => array(
				'image'  => $image->id ?: NULL,
				'errors' => $image->errors() ?: NULL
			),
		));
		/**/
	}


	public function upload_pdf()
	{
		$this->_render['data'] = array();

		if (!$this->request->is('post')) {
				return $this->_notFound();
		}

		$data = $this->request->data + array(
				'pdf' => NULL
		);

		$pdf = Pdfs::create($data['pdf']);

		if ($pdf->validates())
		{
				$pdf->owner_id = $this->user['id'];
				$pdf->save();
		}

		die(json_encode(array(
				'file'    => $pdf->id ?: NULL
			,	'errors'  => $pdf->errors() ?: NULL
		)));
	}


	public function report($page = 1)
	{
		$limit = Campaigns::DEFAULT_COUNT;
		$all   = $this->_render['type'] == 'csv';

		if (!isset($this->request->params['id'])) {
			return $this->_notFound();
		}

		$campaign = Campaigns::first($this->request->params['id']);
		if (!$campaign || ($campaign->status != Campaigns::STATUS_ACTIVE && $campaign->status != Campaigns::STATUS_ENDED)) {
			return $this->_notFound();
		}

		$this->request->params['page'] = $page;

		$website = Websites::first($campaign->website_id);
		$result  = Campaigns::report(compact('campaign', 'website', 'page', 'limit', 'all'));
		extract($result);

		if ($this->_render['type'] == 'csv') {
			$this->_render['data'] = array();
			$this->_render['data'][] = array(
				'Code',
				'Transaction',
				'Product name',
				'Price',
				'Extended',
				'First name',
				'Last name',
				'Email',
				'Phone',
				'Order date'
			);
			foreach ($orderItems as $item) {
				$date = new DateTime($item->order_date, new DateTimeZone('GMT'));
          			$date->setTimezone(new DateTimeZone('Australia/Sydney'));

				$this->_render['data'][] = array(
					$item->formatCode(),
					$item->transaction_id,
					$item->product_name,
					$item->friendly_price,
					$item->extended ? 'Yes' : 'No',
					$item->first_name,
					$item->last_name,
					$item->email,
					$item->phone,
					$date->format('M dS Y h:i A'),
				);
			}

			return;
		}

		$this->set(array(
			'website' => $website,
			'campaign' => $campaign,
			'orderItems' => $orderItems,
			'totalPages' => $totalPages
		));
	}

	public function search()
	{
		$limit      = Campaigns::DEFAULT_COUNT;
		$all        = FALSE;
		$campaign   = NULL;
		$conditions = array();

		if (empty($this->request->params['page'])) {
			$this->request->params['page'] = 1;
		}

		$page = $this->request->params['page'];

		$query = array_filter(array_intersect_key(
			$this->request->query,
			array(
				'transaction_id'  => NULL,
				'email'           => NULL,
				'first_name'      => NULL,
				'last_name'       => NULL,
				'campaign_id'     => NULL,
			)
		));

		if (!empty($query['campaign_id'])) {
			$campaign = Campaigns::first($query['campaign_id']);

			if (!$campaign) {
				return $this->_notFound();
			}
		}

    $voucher = $this->request->query['code'];

    if(!empty($voucher))
    {
        $conditions['Vouchers.voucher'] = array('like' => '%' . $voucher);
    }

		foreach ($query as $k => $v) {
			$v = trim($v);
			if (empty($v)) {
				continue;
			}

			$conditions['Orders.' . $k] = array('like' => '%' . $v . '%');
		}

		$website = $campaign ? Websites::first($campaign->website_id) : NULL;
		$result  = Campaigns::report(compact('campaign', 'website', 'page', 'limit', 'all', 'conditions'));

		extract($result);

		$this->set(array(
			'website' => $website,
			'campaign' => $campaign,
			'orderItems' => $orderItems,
			'totalPages' => $totalPages
		));
	}


	public function duplicate()
	{
		$campaign = Campaigns::first($this->request->params['id']);

		if (!$campaign) {
			return $this->_notFound();
		}

		$this->set(array('campaign' => $campaign));

		if (!$this->request->is('post')) {
			return;
		}

		$NOW = gmdate('Y-m-d H:i:s');

		$data = array(
			'start_date' => NULL,
			'end_date'   => NULL,
			'created'    => $NOW,
			'modified'   => $NOW,
			'status'     => Campaigns::STATUS_INCOMPLETE
		) + $campaign->data();

		unset($data['id']);

		$newCampaign = Campaigns::create($data);
		$newCampaign->save(array(), array('validate' => FALSE));

		$productsTable      = Products::meta('source');
		$notificationsTable = EmailNotifications::meta('source');
		$reportsTable       = EmailReports::meta('source');

		Products::executeQuery(
			"INSERT INTO `{$productsTable}` (`campaign_id`, `name`, `price`, `vouchers`, `threshold`, `buy_limit`, `created`)
			 SELECT '{$newCampaign->id}', `name`, `price`, 0, 0, `buy_limit`, '{$NOW}'
			 FROM `{$productsTable}`
			 WHERE `campaign_id` = '{$campaign->id}'"
		);

		EmailNotifications::executeQuery(
			"INSERT INTO `{$notificationsTable}` (`campaign_id`, `address`)
			 SELECT '{$newCampaign->id}', `address`
			 FROM `{$notificationsTable}`
			 WHERE `campaign_id` = '{$campaign->id}'"
		);

		EmailReports::executeQuery(
			"INSERT INTO `{$reportsTable}` (`campaign_id`, `address`)
			 SELECT '{$newCampaign->id}', `address`
			 FROM `{$reportsTable}`
			 WHERE `campaign_id` = '{$campaign->id}'"
		);


		return $this->redirect(array('Campaigns::add', 'id' => $newCampaign->id));
	}


	public function preview_email()
	{
		$validEmail = Validator::rule('email', @$this->request->data['email']);

		if ($this->request->is('post') && !empty($this->request->params['id']) && $validEmail) {
			$campaign = Campaigns::first($this->request->params['id']);
			if (!$campaign) {
				return $this->_notFound();
			}

			$website  = Websites::first($campaign->website_id);
			$products = Products::findAllByCampaignId($campaign->id);
			$vouchers = Vouchers::buildSet();

			foreach ($products as $product) {
				$vouchers->offsetSet($product->id, Vouchers::create(array(
					'voucher' => Products::generateVoucher($website->code_prefix),
          'product_name' => $product->name
				)));
			}

			$sent = Orders::sendOrderConfirmation(array(
				'campaign' => $campaign,
				'website'  => $website,
				'vouchers' => $vouchers,
				'toName'   => 'Customer Name',
				'toEmail'  => $this->request->data['email'],
			));

			if ($sent) {
				echo json_encode(TRUE);
				die();
			}
		}

		return $this->_notFound();
	}


	public function summary($page = 1)
	{
		$limit = Campaigns::DEFAULT_COUNT;

		if (!isset($this->request->params['id'])) {
			return $this->_notFound();
		}

		$campaign = Campaigns::first($this->request->params['id']);
		if (!$campaign || ($campaign->status != Campaigns::STATUS_ACTIVE && $campaign->status != Campaigns::STATUS_ENDED)) {
			return $this->_notFound();
		}

		$this->request->params['page'] = $page;

		$website = Websites::first($campaign->website_id);
		$summary = Products::summary($campaign->id, array(
			'page'  => $page,
			'limit' => $limit
		));


		$this->set(array(
			'website' => $website,
			'campaign' => $campaign
		) + $summary);
	}


	public function download_vouchers($flag = NULL)
	{
		if (!isset($this->request->params['id'])) {
			return $this->_notFound();
		}

		$extended = $flag == 'extended' ? 1 : 0;

		$vouchers = Vouchers::all(array(
			'fields' => array(
				'Vouchers' => array('voucher')
			),
			'conditions' => array(
				'Products.campaign_id' => $this->request->params['id'],
				'Products.status'      => Products::STATUS_ACTIVE,
				'Vouchers.extended'    => $extended
			),

			'joins' => array(
				array(
					'type'        => 'INNER',
					'source'      => 'products',
					'alias'       => 'Products',
					'constraints' => 'Products.id = Vouchers.product_id'
				),
			)
		));

		$this->_render['data'] = $vouchers->data();
		array_unshift($this->_render['data'], array('Code'));
	}
}

?>
