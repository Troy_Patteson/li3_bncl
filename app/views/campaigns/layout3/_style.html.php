<?php

if ($layout->responsive) :
  echo $this->_render('template', 'responsive/_style');
else :
?>

<style type="text/css" scoped>
  .campaign-wrapper { width: 960px; margin: 0 auto; padding: 40px 0; }
  .campaign-wrapper h1{ margin: 0 0 20px 0; }

  .campaign-splash  { text-align: center; position: relative; }
  .campaign-splash img    { max-width: 960px; }

  .campaign-customer-and-submit{  }

  .campaign-sidebar { position: relative; float: right; width: 320px;/* padding: 20px 0 20px 40px;*/ }
  .campaign-countdown     { position: absolute; top: 630px; right: 120px; margin: 0 0 20px; padding: 13px 20px 12px 100px; font-size: 16px; text-align: center; /*border: 1px solid #333;*/ background: url('/img/clock.png') 20px 0 no-repeat; }
  .campaign-countdown-row     { text-align: center; }
  .campaign-countdown-column  { display: inline-block; vertical-align: top; }
  .campaign-countdown-block   { margin: 0 2px; font-size: 14px; }
  .campaign-countdown-digits  { color: #ff0000; font-size: 22px; }
  .campaign-countdown h2{ font-size: 20px; color: #01a5e2; text-transform: uppercase; margin: 0 0 10px 0; }

  .campaign-details-image { width: 320px; }

  .campaign-header { margin: 0 0 20px 0; }

  .campaign-form { /*width: 400px; padding: 20px; background: #fff; */}
  .campaign-form fieldset { border: 0; padding: 20px 0; margin: 20px 0; }
  .campaign-form legend { display: block; width: 100%; border-bottom: 2px solid #47c550; }

  .campaign-buyer-details label { float: left; width: 20%; text-align: right; margin-right: 2%; margin-top: 4px; }
  .campaign-form input {
    width: 70%; background: #fafafa; color: #333; border: 1px solid #999; padding: 5px;
    -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;
  }
  .campaign-form input:focus { border-color: #47c550; background: #fff; }

  .campaign-products-list{ width: 100%; position: relative; }
  .campaign-products-list input { width: 30px; }
  .campaign-remaining-qty { display: block; font-size: 12px; color: #aaa; }
  .campaign-product-sold-out { display: block; font-size: 12px; color: #c00; font-weight: bold; margin: 0 0 10px; }
  /*
  .campaign-product-name  { display: inline-block; width: 320px; float: left; padding: 0 5px; }
  .campaign-remaining-qty { display: block; font-size: 12px; color: #aaa; }
  .campaign-product-sold-out { display: block; font-size: 12px; color: #c00; font-weight: bold; margin: 0 0 10px; }
  .campaign-product-qty   { float: right; text-align: center; width: 60px; padding: 0 5px; }
  .campaign-product-price { display: block; font-size: 12px; margin: 5px 0 0; }
  */

  .campaign-products-list table{ border-collapse: collapse; border-spacing: 0; width: 100%; }

  .campaign-product-header{ background: url(<?=$this->url('img/background-quantity-header.jpg')?>) top left repeat-x; }
  .campaign-product-header th { color: #ffffff; padding: 6px; font-weight: normal; }

  .campaign-product-header .campaign-product-total{ border-right: 1px solid #968f85; }

  .campaign-product-row{ border-bottom: 1px solid #d9d6cf; }
  .campaign-product-row td{ padding: 6px; }

  .campaign-product-name  { width: 47%; text-align: left; }
  .campaign-product-qty   { width: 15%; border-left: 1px solid #968f85; text-align: center; }
  .campaign-product-price { width: 15%; border-left: 1px solid #968f85; text-align: center; }
  .campaign-product-total { width: 15%; border-left: 1px solid #968f85; text-align: center; }

  .campaign-product-row .campaign-product-name,
  .campaign-product-row .campaign-product-price{
    background: #f6f6f6;
  }

  .campaign-product-row .campaign-product-name,
  .campaign-product-row .campaign-product-qty,
  .campaign-product-row .campaign-product-price,
  .campaign-product-row .campaign-product-total{
    border-left: 1px solid #d9d6cf;
  }

  .campaign-product-row .campaign-product-total{
    border-right: 1px solid #d9d6cf;
  }

  .campaign-product-row .campaign-product-qty .btn{
    background: url("/img/background-quantity-header.jpg") top left repeat-x;
    text-decoration: none;
    padding: 6px;
    font-size: 12px;
    line-height: 12px;
    color: #ffffff;
    width: 10px;
    display: inline-block;
  }

  .campaign-product-row .campaign-product-qty input{
    text-align: center;
    border-radius: 0;
    padding: 4px 0 5px 0;
    width: 30px;
    background: transparent;
    border: none;
    font-weight: bold;
  }

  .campaign-order-errors  {
    padding: 10px; margin: 0 0 20px; border: 1px solid #900; background: #fee; color: #955; font-size: 14px; background-clip: padding-box;
    -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;
  }

  .campaign-form-actions { text-align: center; }
  .campaign-form-actions button {
    width: 200px; height: 50px; text-indent: -9999px; text-align: left;
    padding: 0; border: 0;
    cursor: pointer;
    background: url(<?=$this->url('img/buy-now.png')?>) top left no-repeat;
  }

  .campaign-form-actions button:hover { background-position: bottom left; }

  .campaign-form-row { padding: 5px 100px; }
  .campaign-form-row:hover { background: #f3f3f3; }

  .campaign-thank-you-msg { margin: 0 0 20px; }
  .campaign-cancel-msg { margin: 0 0 20px; }

  .clearfix:before, .clearfix:after { content: ""; display: table; }
  .clearfix:after { clear: both; }
  .clearfix { zoom: 1; }

  .email-confirmation-error{
    color: #ff0000;
    width: 260px;
    margin: 0 0 0 124px;
    display: none;
  }

  .vouchers-listing { width: 460px; margin: 40px 0 0; }

  .extend-voucher { border-top: 1px solid #D9D6CF; margin: 10px 0 0; padding: 5px 5px 0; }

  .checkbox { cursor: pointer; }
  .checkbox input { vertical-align: middle; margin-top: -2px; }
  .checkbox .note { font-style: italic; }
  .checkbox .important { color: #c00; font-style: italic; }
</style>

<?php endif; ?>
