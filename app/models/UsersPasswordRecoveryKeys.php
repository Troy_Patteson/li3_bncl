<?php

namespace app\models;

use \lithium\util\String;
use \lithium\net\http\Media;
use \lithium\net\http\Router;

use \li3_swiftmailer\mailer\Transports;
use \li3_swiftmailer\mailer\Message;

class UsersPasswordRecoveryKeys extends Model
{
	const STATUS_DISABLED = 0;
	const STATUS_ACTIVE   = 1;
	
	const MAX_ATTEMPTS  = 3;
	const KEY_VALIDITY  = 86400;
	const THROTTLE_TIME = 3600;
	
	const EMAIL_SUBJECT      = 'Password Recovery';
	const EMAIL_FROM_ADDRESS = 'no-reply@miniboxmemories.com';
	const EMAIL_FROM_NAME    = 'no-reply@miniboxmemories.com';
	
	protected $_meta = array(
		'key'    => 'id',
		'source' => 'users_password_recovery_keys'
	);
	
	protected $_schema = array(
		'id'      => array('type' => 'integer',  'null' => FALSE),
		'user_id' => array('type' => 'integer',  'null' => FALSE),
		'key'     => array('type' => 'string',   'null' => FALSE),
		'created' => array('type' => 'datetime', 'null' => FALSE),
		'expire'  => array('type' => 'datetime', 'null' => FALSE),
		'used'    => array('type' => 'datetime', 'null' => TRUE,  'default' => NULL),
		'status'  => array('type' => 'integer',  'null' => FALSE, 'default' => 1),
	);
	
	
	public static function create(array $data = array(), array $options = array())
	{
		$data += array(
			'created' => gmdate('Y-m-d H:i:s'),
			'expire'  => gmdate('Y-m-d H:i:s', time() + static::KEY_VALIDITY)
		);
		
		$key = parent::create($data, $options);
		$key->generateKey();
		
		return $key;
	}
	
	
	public function generateKey($entity)
	{
		$entity->key = md5($entity->user_id . '_' . time()) . md5(String::uuid());
	}
	
	
	public static function isThrottled($userId)
	{
		$count = static::count(array(
			'conditions' => array(
				'user_id' => $userId,
				'created' => array('>' => gmdate('Y-m-d H:i:s', time() - static::THROTTLE_TIME)),
				'status'  => UsersPasswordRecoveryKeys::STATUS_ACTIVE
			),
			
			'limit' => static::MAX_ATTEMPTS
		));
		
		return $count >= static::MAX_ATTEMPTS;
	}
	
	
	public static function getValid($key)
	{
		$userKey = static::first(array(
			'conditions' => array(
				'`key`'  => $key,
				'expire' => array('>' => gmdate('Y-m-d H:i:s')),
				'status' => static::STATUS_ACTIVE
			)
		));
		
		return $userKey;
	}
	
	
	public static function emailUrl($params)
	{
		$params += array(
			'user'    => NULL,
			'key'     => NULL,
			'context' => NULL,
		);
		
		$params['url'] = Router::match(array(
			'Auth::password_reset',
			'args' => $params['key']->key,
		), $params['context'], array('absolute' => TRUE));
		
		extract($params);
		
		$tpl = Media::view('default', $params);
		$msg = $tpl->render('all', $params, array(
			'controller' => 'auth',
			'template'   => 'email/reset_key',
			'handler'    => 'html'
		));
		
		$mailer  = Transports::adapter('default');
		$message = Message::newInstance()
			->setSubject(static::EMAIL_SUBJECT)
			->setFrom(array('no-reply@' . $context->env('HTTP_HOST') => $context->env('HTTP_HOST')))
			->setTo(array($user->email))
			->setBody(wordwrap(strip_tags(preg_replace('/<br(.*?)>/', "\n", $msg))))
			->addPart($msg, 'text/html');
		
		return (boolean) $mailer->send($message);
	}
}

?>