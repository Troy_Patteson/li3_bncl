<?php

$baseUrl  = (array) $baseUrl;
$queryStr = !empty($query) ? '?' . http_build_query($query) : NULL;

//if ($totalPages > 1) :

if ($totalPages < 12) {
	$startPage = 2;
	$endPage   = $totalPages - 1;
} else {
	$startPage = max(2, $currentPage - 4);
	$endPage   = min($totalPages - 1, $startPage + 8);
	$startPage = max(2, $endPage - 8);
}

?>
<div class="pagination pagination-right">
	<ul>
		<?php if ($currentPage <= 1) : ?>
		<li class="disabled"><span>&laquo;</span></li>
		<?php else : ?>
		<li><a href="<?=$this->url($baseUrl + array('page' => $currentPage - 1)) . $queryStr?>">&laquo;</a></li>
		<?php endif; ?>
		
		<li<?php echo (1 == $currentPage) ? ' class="active"' : ''?>><a href="<?=$this->url($baseUrl) . $queryStr?>">1</a></li>
		
		<?php if ($startPage > 2) : ?>
		<li class="disabled"><span>...</span></li>
		<?php endif;?>
		
		<?php for ($p = $startPage; $p <= $endPage; $p++) : ?>
		<li<?php echo ($p == $currentPage) ? ' class="active"' : ''?>><a href="<?=$this->url($baseUrl + array('page' => $p)) . $queryStr?>"><?=$p?></a></li>
		<?php endfor; ?>
		
		<?php if ($endPage < $totalPages - 1) : ?>
		<li class="disabled"><span>...</span></li>
		<?php endif;?>
		
		<?php if ($totalPages > 1) : ?>
		<li<?php echo ($totalPages == $currentPage) ? ' class="active"' : ''?>><a href="<?=$this->url($baseUrl + array('page' => $totalPages)) . $queryStr?>"><?=$totalPages?></a></li>
		<?php endif;?>
		
		<?php if ($currentPage >= $totalPages) : ?>
		<li class="disabled"><span>&raquo;</span></li>
		<?php else : ?>
		<li><a href="<?=$this->url($baseUrl + array('page' => $currentPage + 1)) . $queryStr?>">&raquo;</a></li>
		<?php endif; ?>
	</ul>
</div>
<?php //endif; ?>