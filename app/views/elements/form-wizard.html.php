  <div class="wizzard size-<? echo count($stepsTitles); ?>">
    <ul>
    <?php 
    foreach ($stepsTitles as $key => $title) :
      $url = $this->url(array_merge_recursive($baseHref, array('args' => array('step-' . $key))));
    ?>
      <?php if ($key == $currentStep) : ?>
      <li><a href="<?=$url?>" title="<?=$title?>" class="step current"><?=$key?></a></li>
      <?php elseif ($key <= $unlocked) : ?>
      <li><a href="<?=$url?>" title="<?=$title?>" class="step unlocked"><?=$key?></a></li>
      <?php else : ?>
      <li><span title="<?=$title?>" class="step"><?=$key?></span></li>
      <?php endif; ?>
    <?php endforeach; ?>
    </ul>
  </div>