<?php if(!empty($campaign->tracking) && !empty($amount)){
  echo str_replace('AMOUNT', $amount, $campaign->tracking);
} ?>

<div>
  <div class="campaign-wrapper clearfix">
    <h1><?=$campaign->title?></h1>

    <div class="campaign-form">
      <div class="campaign-thank-you-msg">
        <?php echo $message;?>
      </div>

      <a href="<?=$this->url(array('args' => 'buy') + $this->_request->params)?>">Return to campaign</a>

      <div class="vouchers-listing">
        <?php echo $this->_render('template', '_email_vouchers'); ?>
      </div>
    </div>
  </div>

  <?=$this->_render('template', $layout->name . '/_style')?>

  <?=$this->_render('template', '_analytics_ecommerce')?>

  <?php if(!empty($rakuten_mediaforge_mid) && $order && !empty($order)): ?>
    <script type="text/javascript" src="//jp-tags.mediaforge.com/js/<?=$rakuten_mediaforge_mid?>/?orderNumber=<?=$order->id?>&price=<?=$order->value?>"></script>
  <?php endif; ?>

  <?php
      $skulist  = array();
      $qlist    = array();
      $amtlist  = array();

      foreach($order->products as $item)
      {
          array_push($skulist , $item->product_id);
          array_push($qlist   , $item->quantity);
          array_push($amtlist , round($item->price * $item->quantity * 100 / (100 + 10) * 100));
      }

      $skulist  = implode('|', $skulist);
      $qlist    = implode('|', $qlist);
      $amtlist  = implode('|', $amtlist);
  ?>
  <?php if(!empty($skulist) && !empty($qlist) && !empty($amtlist) && !empty($rakuten_linkshare_mid)): ?>
  <img src="http://track.linksynergy.com/ep?mid=<?=$rakuten_linkshare_mid?>&ord=<?=$order->id?>&skulist=<?=$skulist?>&qlist=<?=$qlist?>&amtlist=<?=$amtlist?>&cur=<?=$campaign->website->currency_abbreviation?>">
  <?php endif; ?>

</div>