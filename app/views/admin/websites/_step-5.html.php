<?php

use \app\models\Orders;

?>
	<div class="control-group">
		<label class="control-label" for="wsEmailTemplate">Email template</label>
		<div class="controls">
			<textarea class="input-block-level" rows="10" id="wsEmailTemplate" name="email_template"><?=$website->email_template?></textarea>
			<small class="muted">Available variables are: <code><?php echo implode('</code>, <code>', Orders::$emailVars)?></code></small>
		</div>
	</div>
	