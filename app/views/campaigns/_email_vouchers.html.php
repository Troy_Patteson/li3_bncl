<?php if (!empty($vouchers) && (is_array($vouchers) || $vouchers->count())) : ?>
<table style="border: 1px solid #000; border-collapse: collapse; width: 100%;">
	<tr>
		<th style="border: 1px solid #000; padding: 10px; color: #ff0000; font-family:sans-serif;">Item</th>
		<th style="border: 1px solid #000; padding: 10px; color: #ff0000; font-family:sans-serif;">Voucher</th>
	</tr>
	
	<?php foreach ($vouchers as $voucher) : ?>
	<tr>
		<td style="border: 1px solid #000; padding: 10px; color: #ff0000; font-family:sans-serif; font-size:14px;">
			<?=$voucher->product_name?>
			<?php if ($voucher->extended) :?>
			<br><strong style="color: #000;">(Extended)</strong>
			<?php endif; ?>
		</td>
		<td style="border: 1px solid #000; padding: 10px; color: #ff0000; font-family:sans-serif; font-size:14px;"><?=$voucher->formatCode()?></td>
	</tr>
	<?php endforeach; ?>
</table>
<?php endif; ?>