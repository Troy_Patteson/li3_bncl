<?php

namespace app\models;

class OrdersItems extends Model
{
	protected $_meta = array(
		'key'    => 'id',
		'source' => 'orders_items',
		'locked' => TRUE,
	);
	
	protected $_schema = array(
		'id'         => array('type' => 'integer', 'null' => FALSE, 'key' => 'primary'),
		'order_id'   => array('type' => 'integer', 'null' => FALSE),
		'product_id' => array('type' => 'integer', 'null' => FALSE),
		'quantity'   => array('type' => 'integer', 'null' => FALSE),
		'price'      => array('type' => 'float',   'null' => FALSE),
		'extended'   => array('type' => 'integer', 'null' => FALSE, 'default' => 0),
	);
	
	public $validates = array(
		
	);
	
	public $belongsTo = array(
		'Products' => array('key' => 'product_id')
	);
	
	
	public static function forOrder($orderId)
	{
		$data = static::all(array(
			'fields' => array(
				'OrdersItems' => array_keys(static::schema()->fields()),
				'Products'    => array_keys(Products::schema()->fields()),
			),
			
			'with' => array('Products'),
			
			'conditions' => array(
				'OrdersItems.order_id' => $orderId
			),
			
			'order' => array('OrdersItems.id' => 'DESC')
		));
		
		return $data;
	}
}



?>