<?php

namespace app\controllers\admin;

use app\models\Websites;

use li3_extras\storage\Session;
use li3_paypal\ExpressCheckout;

use lithium\util\Inflector;
use lithium\action\DispatchException;
use lithium\data\Connections;

class WebsitesController extends IndexController
{
	public function index()
	{
		$page     = isset($this->request->params['page']) ? $this->request->params['page'] : 1;
		$limit    = Websites::DEFAULT_COUNT;
		$websites = Websites::forUser($this->user['id'], compact('page', 'limit'));
		
		$this->set(array(
			'deleted'  => Session::flash('websiteDeleted'),
			'success'  => Session::flash('websiteSaved'),
			'websites' => $websites
		));
	}
	
	
	public function add($step = NULL)
	{
		if (!$step) {
			$step = 'step-1';
			Session::delete('newWebsite');
			Session::delete('newWebsite-unlocked');
			Session::delete('editWebsite');
		}
		
		$method = '_' . Inflector::underscore($step);
		
		if (!method_exists(__CLASS__, $method)) {
			throw new DispatchException('Could not route request.');
		}
		
		$data     = $this->request->data + (array) Session::read('newWebsite');
		$website  = Websites::create($data);
		$unlocked = Session::read('newWebsite-unlocked') ?: 1;
		
		$website->owner_id = $this->user['id'];
		
		$this->set(array(
			'website'  => $website,
			'errors'   => array(),
			'unlocked' => $unlocked,
			'success'  => Session::flash('websiteSaved')
		));
		
		if ($this->request->is('post')) {
			$website->validates();
		}
		
		$current  = $this->{$method}($website);
		
		if ($unlocked < $current['step']) {
			return $this->redirect(array('Websites::add', 'args' => 'step-' . $unlocked));
		}
		
		if ($this->request->is('post') && empty($current['errors'])) {
			if ($current['step'] == 5) {
				$website->owner_id = $this->user['id'];
				$website->save();
			
				Session::delete('newWebsite');
				Session::delete('newWebsite-unlocked');
				Session::write('websiteSaved', $website->data());
				
				$redirect = 'Websites::index';
			} else {
				$nextStep = $current['step'] + 1;
				$redirect = array('Websites::add', 'args' => 'step-' . $nextStep);
				
				Session::write('newWebsite', $website->data());
				Session::write('newWebsite-unlocked', max($nextStep, $unlocked));
			}
			
			return $this->redirect($redirect);
		}
		
		return $current;
	}
	
	public function edit($step = NULL)
	{
		if (!$step) {
			$step = 'step-1';
			Session::delete('newWebsite');
			Session::delete('editWebsite');
		}
		
		$method = '_' . Inflector::underscore($step);
		
		$website = Websites::active(array(
			'siteId' => $this->request->params['id'],
			'userId' => $this->user['id']
		));
		
		if (!$website || !method_exists(__CLASS__, $method)) {
			throw new DispatchException('Could not route request.');
		}
		
		$data = $this->request->data + (array) Session::read('newWebsite');
		$website->set($data);
		
		$this->set(array(
			'website'  => $website,
			'errors'   => array(),
			'unlocked' => 5,
			'success'  => Session::flash('websiteSaved')
		));
		
		if ($this->request->is('post')) {
			$website->validates();
		}
		
		$current = $this->{$method}($website);
		
		if ($this->request->is('post') && empty($current['errors'])) {
			if ($current['step'] == 5) {
				$website->save();
				
				Session::delete('newWebsite');
				Session::write('websiteSaved', $website->data());
				
				$redirect = 'Websites::index';
			} else {
				$nextStep = $current['step'] + 1;
				$redirect = array('Websites::edit', 'id' => $website->id, 'args' => 'step-' . $nextStep);
				
				Session::write('newWebsite', $website->data());
			}
			
			return $this->redirect($redirect);
		}
		
		return $current;
	}
	
	
	public function delete()
	{
		$items = array();
		
		if (isset($this->request->data['websites'])) {
			$items = (array) $this->request->data['websites'];
		}
		
		if ($items) {
			Websites::softDelete(array('id' => $items));
			Session::write('websiteDeleted', TRUE);
		}
		
		return $this->redirect('Websites::index');
	}
	
	
	protected function _step_1($website)
	{
		$step   = 1;
		$fields = array('host' => NULL, 'timezone' => NULL);
		$url    = parse_url($website->host) + array('host' => NULL, 'path' => NULL);
		
		$website->host = $url['host'] ?: $url['path'];
		
		$errors = array_intersect_key($website->errors(), $fields);
		
		return compact('errors', 'step');
	}
	
	
	protected function _step_2($website)
	{
		$step   = 2;
		$fields = array(
			'api_username'          => NULL,
			'api_password'          => NULL,
			'api_signature'         => NULL,
			'currency_abbreviation' => NULL,
			'currency_symbol'       => NULL,
		);
		$website->currency_abbreviation = strtoupper($website->currency_abbreviation);
		$errors = array_intersect_key($website->errors(), $fields);
		
		if (!$this->request->is('post')) {
			return compact('errors', 'step');
		}
		
		if (!$errors) {
			$payment = new ExpressCheckout();
			$payment->credentials(array(
				'username'  => $website->api_username,
				'password'  => $website->api_password,
				'signature' => $website->api_signature,
			));
			
			$payment->urls(array(
				'return' => 'http://' . $this->request->url,
				'cancel' => 'http://' . $this->request->url,
			));
			
			$payment->currency($website->currency_abbreviation);
			
			$payment->addItem(array(
				'id'    => 1,
				'name'  => 'test',
				'price' => '1.00',
				'qty'   => 1
			));
			
			$success = $payment->create();
			
			if (!$success) {
				$error = $payment->errors();
				$error = array_pop($error);
				
				$errors['paypal'] = array('<strong>Paypal error: </strong>' . $error);
			}
		}
		
		return compact('errors', 'step');
	}
	
	
	protected function _step_3($website)
	{
		$step   = 3;
		$fields = array('code_prefix' => NULL);
		$errors = array_intersect_key($website->errors(), $fields);
		
		return compact('errors', 'step');
	}
	
	
	protected function _step_4($website)
	{
		$step   = 4;
		$fields = array(
			'website_template' => NULL,
			'website_content'  => NULL,
		);
		
		if (!$this->request->is('post')) {
			$website->website_template = Websites::fetchTemplate('http://' . $website->host);
		}
		
		$website->website_template = Websites::parseTemplate($website->website_template, $website->website_content);
		
		$errors = array_intersect_key($website->errors(), $fields);
		
		return compact('errors', 'step');
	}
	
	
	protected function _step_5($website)
	{
		$step   = 5;
		$fields = array('email_template' => NULL);
		$errors = array_intersect_key($website->errors(), $fields);
		
		if (strpos($website->email_template, '{--voucherCodes--}') === FALSE) {
			$errors['invalidTemplate'] = array('Your email template must contain the placeholder <b>{--voucherCodes--}</b>');
		}
		
		return compact('errors', 'step');
	}
}

?>