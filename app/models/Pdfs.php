<?php

namespace app\models;

class Pdfs extends Model {

    protected $_meta = array(
        'key'     => 'id'
      , 'source'  => 'pdfs'
      , 'locked'  => TRUE
    );

    protected $_schema = array(
        'id'        => array('type' => 'integer'  , 'null' => FALSE, 'key' => 'primary')
      , 'owner_id'  => array('type' => 'integer'  , 'null' => FALSE)
      , 'product_id'=> array('type' => 'integer'  , 'null' => TRUE)
      , 'path'      => array('type' => 'string'   , 'null' => FALSE)
      , 'file_name' => array('type' => 'string'   , 'null' => FALSE)
      , 'file_size' => array('type' => 'integer'  , 'null' => FALSE)
      , 'created'   => array('type' => 'datetime' , 'null' => FALSE)
    );

    public $belongsTo = array(
        'Product' => array(
            'to' => 'app\models\Products'
          , 'key'=> 'product_id'
        )
    );

    public function validates($entity, array $options = array())
    {
        $errors = array();

        if (!empty($entity->error)) {
          switch ($entity->error) {
            case UPLOAD_ERR_INI_SIZE :
            case UPLOAD_ERR_FORM_SIZE :
              $errors[] = 'The file is to large. Maximum upload size is <b>' . static::maxUploadSize() . '</b>';
            break;

            case UPLOAD_ERR_PARTIAL :
              $errors[] = 'Transfer failed. Please try again.';
            break;

            case UPLOAD_ERR_NO_FILE :
              $errors[] = 'Missing file.';
            break;

            default :
              $errors[] = 'Internal server error. Please try again.';
            break;
          }
        }

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        if (!empty($entity->tmp_name) && file_exists($entity->tmp_name) && is_readable($entity->tmp_name))
        {
            if (finfo_file($finfo, $entity->tmp_name) != 'application/pdf')
            {
                $errors[] = 'Uploaded file is not a pdf file.';
            }
        }

        $entity->errors($errors);

        return empty($errors);
    }

    public function save($entity, $data = null, array $options = array())
    {
        if (!$entity->validates())
        {
            return FALSE;
        }

        if (!empty($entity->tmp_name) && is_uploaded_file($entity->tmp_name) && !$entity->_store())
        {
            return FALSE;
        }

        return parent::save($entity, $data, $options);
    }

    public function url($entity)
    {
        return 'pdfs/' . md5($entity->owner_id) . '/' . $entity->path;
    }

    public function _store($entity)
    {
        $pdf_name   = md5(mt_rand() . microtime(TRUE)) . '.pdf';
        $file_path  = LITHIUM_APP_PATH . '/webroot/pdfs/' . md5($entity->owner_id) . '/';

        if (!static::mkdirRecursive($file_path))
        {
            $entity->errors(array(
              'Internal server error. Please try again later.'
            ));

            return FALSE;
        }

        if(move_uploaded_file($entity->tmp_name, $file_path . $pdf_name))
        {
            $entity->set(array(
                'path'      => $file_path
              , 'file_name' => $pdf_name
              , 'file_size' => filesize($file_path . $pdf_name)
            ));

            chmod($file_path . $pdf_name, 0777);

            return TRUE;
        }

        return FALSE;
    }

    public function mkdirRecursive($path, $mode = 0777)
    {
        $path = rtrim($path, '/');
        $dirs = explode('/', $path);
        $path = $dirs[0];

        for ($d = 1; $d < count($dirs); $d++)
        {
            $path .= '/' . $dirs[$d];

            if (!is_dir($path) && !mkdir($path))
            {
                return FALSE;
            }

            chmod($path, $mode);
        }

        return TRUE;
    }

}