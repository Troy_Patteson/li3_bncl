<?php use app\models\Campaigns;

$options = array('inline' => FALSE);
$this->html->script('/libs/file-upload/vendor/jquery.ui.widget', $options);
$this->html->script('/libs/file-upload/jquery.iframe-transport.js', $options);
$this->html->script('/libs/file-upload/jquery.fileupload.js', $options);

$landingImageName = isset($landingImage) ? $landingImage->file_name : NULL;
$detailsImageName = isset($detailsImage) ? $detailsImage->file_name : NULL;

?>

  <div class="control-group">
    <label class="control-label" for="inLayout">Layout</label>
    <div class="controls">
      <select class="" name="layout_id" id="inLayout">
        <?php foreach ($layouts as $layout) : ?>
        <option <?php echo $layout->id == $campaign->layout_id ? 'selected="selected"' : ''?> value="<?=$layout->id?>"><?=$layout->name?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" for="inClock">Clock</label>
    <div class="controls">
      <select name="clock" id="inClock">
        <option <?php echo $campaign->clock == Campaigns::CLOCK_CLASIC ? 'selected="selected"' : ''?> value="<?=Campaigns::CLOCK_CLASIC?>">Classic clock</option>
        <option <?php echo $campaign->clock == Campaigns::CLOCK_48_TO_24 ? 'selected="selected"' : ''?> value="<?=Campaigns::CLOCK_48_TO_24?>">48 to 24 clock</option>
      </select>
    </div>
  </div>

	<div class="control-group">
		<label class="control-label" for="inLandingImage">Landing page image</label>
		<div class="controls">
			<div class="image-upload <?=isset($landingImage) ? 'has-image' : ''?>">
				<div class="fileupload fileupload-new">
					<div class="input-append">
						<div class="uneditable-input"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div>
						<span class="btn btn-file">
							<span class="fileupload-new"><i class="icon-upload"></i></span>
              <span class="fileupload-exists"><i class="icon-upload"></i></span>
							<input data-url="<?=$this->url('Campaigns::upload_image')?>" type="file" id="inLandingImage" name="image" />
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
					</div>
				</div>

				<small class="fileupload-errors text-error"></small>

				<div class="fileupload-progress">
          <div class="progress progress-striped active">
            <div class="bar"></div>
          </div>
        </div>

				<div class="fileupload-complete">
					<div class="fileupload-label input-large text-success pull-left"><?=$landingImageName?></div>
					<button type="button" class="btn btn-delete"><i class="icon-minus-sign"></i></button>
					<input type="hidden" name="landing_image" value="<?=$campaign->landing_image?>" />
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inDetailsImage">Customer details page image</label>
		<div class="controls">
			<div class="image-upload <?=isset($detailsImage) ? 'has-image' : ''?>">
				<div class="fileupload fileupload-new">
					<div class="input-append">
						<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div>
						<span class="btn btn-file">
							<span class="fileupload-new"><i class="icon-upload"></i></span>
							<span class="fileupload-exists"><i class="icon-upload"></i></span>
							<input data-url="<?=$this->url('Campaigns::upload_image')?>" type="file" id="inDetailsImage" name="image" />
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
					</div>
				</div>

				<small class="fileupload-errors text-error"></small>

				<div class="fileupload-progress">
          <div class="progress progress-striped active">
            <div class="bar"></div>
          </div>
        </div>

				<div class="fileupload-complete">
					<div class="fileupload-label input-large text-success pull-left"><?=$detailsImageName?></div>
					<button type="button" class="btn btn-delete"><i class="icon-minus-sign"></i></button>
					<input type="hidden" name="details_image" value="<?=$campaign->details_image?>" />
				</div>
			</div>
		</div>

    <hr />

    <div class="control-group">
      <label class="control-label" for="inHeader">Customer details header</label>
      <div class="controls">
        <textarea class="input-block-level" rows="10" id="inHeader" name="header"><?=$campaign->header?></textarea>
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="inFooter">Customer details footer</label>
      <div class="controls">
        <textarea class="input-block-level" rows="10" id="inFooter" name="footer"><?=$campaign->footer?></textarea>
      </div>
    </div>
	</div>
