<div>
  <div class="campaign-wrapper campaign-splash clearfix">
    <a href="<?=$this->url(array('args' => 'buy') + $this->_request->params)?>">
      <img src="<?=$this->url($campaign->landing_image->url())?>" />
    </a>
    <?=$this->_render('template', 'clocks/index', compact('campaign'))?>
  </div>

  <?=$this->_render('template', $layout->name . '/_style')?>
  <?=$this->_render('template', '_links')?>

  <?php if(!empty($rakuten_mediaforge_mid)): ?>
    <script type="text/javascript" src="//jp-tags.mediaforge.com/js/<?=$rakuten_mediaforge_mid?>"></script>
  <?php endif; ?>
</div>