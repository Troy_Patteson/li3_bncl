<fieldset class="campaign-products-list">

<script type="text/javascript">
  jQuery( document ).ready(function($)
  {
    $('.btn').click(function(e)
    {
        e.stopPropagation();
        e.preventDefault();

        var $this = $(this);

        var value = parseFloat($this.parent().find('input[name*="qty"]').val());
        var limit = parseFloat($this.parent().find('input[name*="limit"]').val());

        if($this.hasClass('btn-increment'))
        {
            value += 1;
            if(limit && value > limit)
            {
                value = limit;
            }
        }
        else if($this.hasClass('btn-decrement'))
        {
            value -= 1;
            if(value < 0)
            {
                value = 0;
            }
        }

        var $input = $this.parent().find('input[name*="qty"]');
        $input.val(value);
        $input.each(pushValue);
    });

    $('input[name*="qty"]').each(pushValue);
    $('input[name*="qty"]').change(pushValue);
    $('.extend-voucher input').click(function(e) {
      $(this).closest('.campaign-product-row').find('input[name*="qty"]').each(pushValue);
    });

    function pushValue()
    {
      $this = $(this);
      $row  = $this.parents('.campaign-product-row');
      var limit = parseFloat($this.parent().find('input[name*="limit"]').val());

      price     = parseFloat( $('.campaign-product-price span', $row).html() );
      quantity  = $this.val();

      if(quantity < 0)
      {
          quantity = 0;
      }

      if(limit && quantity > limit)
      {
          quantity = limit;
      }

      $this.val(quantity);

      if ($row.find('.extended').is(':checked')) {
        price += 5;
      }

      $('.campaign-product-total span', $row).html(parseFloat((price * quantity).toFixed(10)).toFixed(2));
    }
  });
</script>

<table>
  <tr class="campaign-product-header">
    <th class="campaign-product-name">Product</th>
    <th class="campaign-product-qty">Quantity</th>
    <th class="campaign-product-price">Price</th>
    <th class="campaign-product-total">Total</th>
  </tr>
  <?php foreach($campaign->products as $product) : ?>
  <tr class="campaign-product-row">
    <td class="campaign-product-name">
      <?=$product->name?>
      <?php if (($product->availability->remaining || $product->availability->remainingExtended) && !$soldOut) : ?>
        <?php if(!empty($rakuten_mediaforge_mid)): ?>
        <script type="text/javascript" src="//jp-tags.mediaforge.com/js/<?=$rakuten_mediaforge_mid?>/?prodID=<?=$product->id?>"></script>
        <?php endif; ?>

        <?php if (isset($product->max_allowed)) : ?>
        <span class="campaign-remaining-qty">(Purchase limit: <?=$product->max_allowed?>)</span>
        <?php endif; ?>

        <?php if ($product->availability->remainingExtended) : ?>
        <div class="extend-voucher">
          <label class="checkbox">
            <input type="radio" name="extend[<?=$product->id?>]" value="0" <?=@$formData['extend'][$product->id] != 1 ? 'checked' : ''?> />
            1 month to create <span class="note">(included)</span>
          </label>
          <br>
          <label class="checkbox">
            <input type="radio" class="extended" name="extend[<?=$product->id?>]" value="1" <?=@$formData['extend'][$product->id] == 1 ? 'checked' : ''?> />
            3 months to create <span class="important">($5.00 per voucher)</span>
          </label>
          <?php endif; ?>
        </div>
      <?php else : ?>
      <span class="campaign-product-sold-out">- SOLD OUT -</span>
      <?php endif; ?>
    </td>
    <td class="campaign-product-qty">
      <?php if (($product->availability->remaining || $product->availability->remainingExtended) && !$soldOut) : ?>
      <input type="hidden" name="limit[<?=$product->id?>]" value="<?=@$product->buy_limit?>">
      <a href="#" class="btn btn-decrement">-</a>
      <input type="text" name="qty[<?=$product->id?>]" value="<?=@$formData['qty'][$product->id]?>">
      <a href="#" class="btn btn-increment">+</a>
      <?php endif; ?>
    </td>
    <td class="campaign-product-price">
      <?=$campaign->website->currency_symbol?><span><?=$product->price?></span>
    </td>
    <td class="campaign-product-total"><?=$campaign->website->currency_symbol?><span>0.00</span></td>
  </tr>
  <?php endforeach; ?>
</table>

</fieldset>
