<?php

namespace app\models;

use \lithium\util\String;
use \lithium\net\http\Media;
use \lithium\analysis\Logger;

use \li3_swiftmailer\mailer\Transports;
use \li3_swiftmailer\mailer\Message;

use \Swift_Attachment;

class Orders extends Model
{
	const STATUS_PENDING   = 0;
	const STATUS_COMPLETED = 1;
	const STATUS_CANCELED  = 2;
	const STATUS_FAILED    = 3;

	const EMAIL_SUBJECT      = 'Thank you for your order';

	protected $_meta = array(
		'key'    => 'id',
		'source' => 'orders',
		'locked' => TRUE,
	);

	protected $_schema = array(
		'id'             => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'campaign_id'    => array('type' => 'integer',  'null' => FALSE),
		'token'          => array('type' => 'string',   'null' => TRUE,  'default' => NULL),
		'payer_id'       => array('type' => 'string',   'null' => TRUE,  'default' => NULL),
		'transaction_id' => array('type' => 'string',   'null' => TRUE,  'default' => NULL),
		'first_name'     => array('type' => 'string',   'null' => FALSE),
		'last_name'      => array('type' => 'string',   'null' => FALSE),
		'email'          => array('type' => 'string',   'null' => FALSE),
		'phone'          => array('type' => 'string',   'null' => TRUE,  'default' => NULL),
		'value'          => array('type' => 'float',    'null' => FALSE),
		'created'        => array('type' => 'datetime', 'null' => FALSE),
		'status'         => array('type' => 'integer',  'null' => FALSE, 'default' => 0),
	);

	public $validates = array(
		'first_name'         => array(array('notEmpty',     'message' => 'Missing first name', 'required' => TRUE)),
		'last_name'          => array(array('notEmpty',     'message' => 'Missing last name', 'required' => TRUE)),
		'email'              => array(array('email',        'message' => 'Invalid email', 'required' => TRUE)),
		'email_confirmation' => array(array('confirmEmail', 'message' => 'The email and email confirmation do not match', 'required' => FALSE)),
		'phone'              => array(array('phone',        'message' => 'Invalid phone number', 'required' => TRUE)),
		//'products'   => array(array('notEmpty', 'message' => 'You haven\'t added any products', 'required' => TRUE)),
	);


	public static $emailVars = array('{--name--}', '{--voucherCodes--}');
	public static $messagesVars = array('{--customerName--}', '{--customerEmail--}');


	public function save($entity, $data = null, array $options = array())
	{
		$exists = $entity->exists();
		$result = parent::save($entity, $data, $options);

		if ($result && !$exists && $entity->products) {
			foreach ($entity->products as $product) {
				$item = OrdersItems::create(array(
					'order_id'   => $entity->id,
					'product_id' => $product->id,
					'quantity'   => $product->qty,
					'price'      => $product->price,
					'extended'   => $product->extended
				));
				$item->save();
			}
		}

		return $result;
	}


	public function reachedBuyLimit($entity, $products = NULL)
	{
		$limitReached = FALSE;
		$products = $products ?: $entity->products;

		if (!$entity->email || !$products) {
			return $limitReached;
		}

		foreach ($products as $product) {
			if ($product->buy_limit <= 0) {
				continue;
			}

			$previous = Campaigns::report(array(
				'limit'      => $product->buy_limit,
				'conditions' => array(
					'Orders.email' => $entity->email,
					'Products.id'  => $product->id
				)
			));

			$product->max_allowed = $product->buy_limit;

			if (!empty($previous['orderItems'])) {
				$product->max_allowed = $product->buy_limit - $previous['orderItems']->count();

				if ($product->max_allowed < $product->qty) {
					$limitReached = TRUE;
				}
			}
		}

		return $limitReached;
	}


	public function transactionId($entity)
	{
		return $entity->transaction_id ?: $entity->id;
	}


	public function fullName($entity)
	{
		return trim($entity->first_name . ' ' . $entity->last_name);
	}


	public function sendEmail($entity)
	{
		$campaign = Campaigns::first($entity->campaign_id);
		$website  = Websites::first($campaign->website_id);
		$vouchers = Vouchers::byOrder($entity->id);
		$items    = OrdersItems::forOrder($entity->id);

		$pdfs = array();

		foreach ($items as $key => $value)
		{
        $pdf = null;
        if(!empty($value->product_id))
        {
            $pdf = Pdfs::first(array(
                'conditions' => array(
                    'product_id' => $value->product_id
                )
            ));
        }
        if(!empty($pdf->id))
        {
            array_push($pdfs, $pdf);
        }
		}

		return static::sendOrderConfirmation(array(
			'campaign' => $campaign,
			'website'  => $website,
			'vouchers' => $vouchers,
			'toName'   => $entity->fullName(),
			'toEmail'  => $entity->email,
			'pdfs'     => $pdfs
		));
	}


	public static function sendOrderConfirmation($params)
	{
		$params += array(
			'campaign' => NULL,
			'website'  => NULL,
			'vouchers' => NULL,
			'toName'   => NULL,
			'toEmail'  => NULL,
			'pdfs'     => NULL
		);

		extract($params);

		$template = $campaign->email_template ?: $website->email_template;
		$renderer = Media::view('default', $params);
		$codesTpl = $renderer->render('all', $params, array(
			'controller' => 'campaigns',
			'template'   => '_email_vouchers',
			'handler'    => 'html'
		));

		$msgBody = str_replace(static::$emailVars, array($toName, $codesTpl), $template);

		$mailer  = Transports::adapter('default');
		$message = Message::newInstance()
			->setSubject(static::EMAIL_SUBJECT)
			->setFrom(array('no-reply@' . $website->host => $website->host))
			->setTo(array($toEmail => $toName))
			->setBody(wordwrap(strip_tags(preg_replace('/<br(.*?)>/', "\n", $msgBody))))
			->addPart($msgBody, 'text/html');

		foreach ($pdfs as $pdf)
		{
				$message->attach(Swift_Attachment::fromPath($pdf->path . $pdf->file_name));
		}

		return (boolean) $mailer->send($message);
	}
}

?>