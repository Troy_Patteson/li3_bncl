
	<div class="content-top">
		<div class="pull-right">
			<ul>
				<li class="content-top-search"><a href="#" class="btn btn-search"><i class="icon-search"></i></a></li>
				<li><a href="<?=$this->url(array('Campaigns::report', 'id' => $campaign->id, 'type' => 'csv'))?>" class="btn btn-download"><i class="icon-arrow-down"></i></a></li>
				<li><a href="<?=$this->url(array('Campaigns::index'))?>" class="btn btn-close"><i class="icon-remove"></i></a></li>
			</ul>
		</div>
		
		<h1><?= $campaign->title; ?> report</h1>
	</div>
	
	<div class="content-middle">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Code</th>
				<th>Transaction</th>
				<th>Product</th>
				<th>Price</th>
				<th>Extended</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Phone</th>
				<th class="col-updated">Order date</th>
			</tr>
		</thead>
		
		<tbody>
			<?php foreach ($orderItems as $item) : ?>
			<tr>
				<td><code><?=$item->formatCode()?></code></td>
				<td><?=$item->transaction_id?></td>
				<td><?=$item->product_name?></td>
				<td><?=$item->friendly_price?></td>
				<td><?=$item->extended ? 'Yes' : 'No'?></td>
				<td><?=$item->first_name?></td>
				<td><?=$item->last_name?></td>
				<td><?=$item->email?></td>
				<td><?=$item->phone?></td>
				<td class="col-updated">
          <?php
          $date = new DateTime($item->order_date, new DateTimeZone('GMT'));
          $date->setTimezone(new DateTimeZone('Australia/Sydney'));
          echo $date->format('M dS Y h:i A');
          ?>
        </td>
			</tr>
			<?php endforeach; ?>
			
		</tbody>
	</table>
	</div>
	
	<div class="content-bottom">     

	<?php
	echo $this->_render('element', 'pagination', array(
		'totalPages'  => $totalPages,
		'currentPage' => isset($this->_request->params['page']) ? $this->_request->params['page'] : 1,
		'baseUrl'     => array('Campaigns::report', 'id' => $campaign->id),
	));
	?>
	</div>

	<?php echo $this->_render('template', 'reports-search-form'); ?>
