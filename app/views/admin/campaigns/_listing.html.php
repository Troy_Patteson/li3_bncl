<?php use app\models\Campaigns; ?>

<table class="table">
	<thead>
		<tr>
			<th class="col-check"><input type="checkbox" name="check-all" data-target="campaigns[]" value="0" /></th>
			<th>Campaign</th>
			<th></th>
			<th class="col-updated">Created</th>
		</tr>
	</thead>

	<tbody>
		<?php
		$action = $this->_request->params['action'] == 'unpublished' ? 'add' : 'edit';
		foreach ($campaigns as $item) :
			$created = date('M jS Y', strtotime($item->created));
			$status  = '';
			switch ($item->status)
			{
        case Campaigns::STATUS_DELETED:
          $status = 'campaigns-listing-deleted';
          break;

				case Campaigns::STATUS_ACTIVE:
					$status = 'campaigns-listing-active';
					break;

        case Campaigns::STATUS_INCOMPLETE:
          $status = 'campaigns-listing-incomplete';
          break;

				case Campaigns::STATUS_ENDED:
					$status = 'campaigns-listing-ended';
					break;
			}
			$title = $item->title;
			if(!empty($item->system_title))
			{
				$title = $item->system_title;
			}
		?>
		<tr class="campaigns-listing <?=$status?>">
			<td class="col-check"><input type="checkbox" name="campaigns[]" value="<?=$item->id?>" /></td>
			<td>
					<a href="<?=$this->url(array('Campaigns::' . $action, 'id' => $item->id))?>"><?=mb_strimwidth($title, 0, 50, '...')?></a>
					<br />
					taking place on <strong>"<?=$item->website->host?>"</strong>
			</td>
			<td>
				<?php if (empty($state) || (!empty($state) && $state != 'unpublished')) : ?>
				<div class="campaign-listing-info">
					<a href="<?=$this->url(array('Campaigns::summary', 'id' => $item->id))?>">Quick summary</a> |
					<a href="<?=$this->url(array('Campaigns::report', 'id' => $item->id))?>">View report</a> |
					<a href="<?=$this->url(array('Campaigns::duplicate', 'id' => $item->id))?>">Duplicate</a> |
					<a href="<?=$item->publicUrl()?>" target="_blank"><?=mb_strimwidth($item->publicUrl(), 0, 50, '...')?></a>
					<br />
					<a href="<?=$this->url(array('Campaigns::download_vouchers', 'id' => $item->id, 'type' => 'csv'))?>">Download vouchers</a>
					<?php if ($item->hasExtended) : ?>
					| <a href="<?=$this->url(array('Campaigns::download_vouchers', 'id' => $item->id, 'type' => 'csv', 'args' => array('extended')))?>">Download extended vouchers</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</td>
			<td class="col-updated"><?=$created?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>