<?php

$startDate = $campaign->localDate('start_date');
$endDate   = $campaign->localDate('end_date');

?>

	<div class="control-group">
		<label class="control-label" for="inStartDate">Start Date</label>
		<div class="controls">
			<input class="input-block-level datepicker" type="text" id="inStartDate" name="local_start_date" value="<?=$startDate?>" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="inEndDate">End Date</label>
		<div class="controls">
			<input class="input-block-level datepicker" type="text" id="inEndDate" name="local_end_date" value="<?=$endDate?>" />
		</div>
	</div>
