<?php

namespace app\controllers;

use lithium\action\DispatchException;

class IndexController extends \lithium\action\Controller
{
	public function index()
	{
		//return $this->redirect('Auth::login');
		throw new DispatchException('Could not route request.');
	}
}

?>