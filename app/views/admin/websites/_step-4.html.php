
	<div class="control-group">
		<label class="control-label" for="wsWebTemplate">Fetched markup</label>
		<div class="controls">
			<textarea class="input-block-level" rows="10" id="wsWebTemplate" name="website_template"><?=$website->website_template?></textarea>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="wsWebContentID">Content ID</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsWebContentID" name="website_content" value="<?=$website->website_content?>" autocomplete="off" />
			<small class="muted">(where should I place my content)</small>
		</div>
	</div>
