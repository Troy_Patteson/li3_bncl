<?php

namespace app\models;

class Vouchers extends Model
{
	const STATUS_DELETED    = 0;
	const STATUS_ACTIVE     = 1;

	protected $_meta = array(
		'key'    => 'id',
		'source' => 'vouchers',
		'locked' => TRUE,
	);
	
	protected $_schema = array(
		'id'             => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'product_id'     => array('type' => 'integer',  'null' => FALSE),
		'order_item_id'  => array('type' => 'integer',  'null' => TRUE,  'default' => NULL),
		'voucher'        => array('type' => 'string',   'null' => FALSE),
		'extended'       => array('type' => 'integer',  'null' => FALSE, 'default' => 0),
		'created'        => array('type' => 'datetime', 'null' => FALSE),
		'status'         => array('type' => 'integer',  'null' => FALSE, 'default' => 1),
	);
	
	public $validates = array(
		
	);
	
	
	public function formatCode($entity)
	{
		return strtoupper(rtrim(chunk_split($entity->voucher, 4, ''), ''));
	}
	
	
	public static function byOrder($orderId)
	{
		return static::all(array(
			'conditions' => array(
				'Orders.id' => $orderId
			),
			
			'fields' => array(
				'Vouchers' => array('voucher'),
				array('Products.name AS product_name'),
				array('OrdersItems.extended AS extended'),
			),
			
			'joins' => array(
				array(
					'type'        => 'INNER',
					'source'      => 'orders_items',
					'alias'       => 'OrdersItems',
					'constraints' => 'OrdersItems.id = Vouchers.order_item_id'
				),
				
				array(
					'type'        => 'INNER',
					'source'      => 'orders',
					'alias'       => 'Orders',
					'constraints' => 'OrdersItems.order_id = Orders.id'
				),
				
				array(
					'type'        => 'INNER',
					'source'      => 'products',
					'alias'       => 'Products',
					'constraints' => 'OrdersItems.product_id = Products.id'
				),
			),
			
			'order' => array('Products.name' => 'ASC')
		));
	}
}



?>