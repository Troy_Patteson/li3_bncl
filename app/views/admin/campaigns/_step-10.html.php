<?php
$previewUrl = $campaign->previewUrl($this->_request);
$publicUrl  = $campaign->publicUrl();
?>

  <div class="well public-url">
    <a href="<?=$publicUrl?>" target="_blank" class="" type="button"><?=$publicUrl?></a>
  </div>

<div class="text-center">

	<a href="<?=$previewUrl?>" target="_blank" class="btn btn-preview" type="button"><i class="icon-eye-open"></i>Preview</a>
</div>