<?php

use \lithium\util\Validator;

use \app\models\Users;
use \app\models\Websites;
use \app\models\Layouts;
use \app\models\Campaigns;

use \Exception;
use \DateTime;
use \DateTimeZone;

Validator::add('uniqueEmail', function ($value, $format, $options) {
	$conditions = array(
		'email'  => $value,
		'status' => Users::STATUS_ACTIVE
	);

	if (isset($options['values']['id'])) {
		$conditions['id'] = array('!=' => $options['values']['id']);
	}

	return !Users::first(compact('conditions'));
});

Validator::add('uniqueHost', function ($value, $format, $options) {
	$conditions = array(
		//'owner_id' => @$options['values']['owner_id'],
		'host'     => $value,
		'status'   => Websites::STATUS_ACTIVE
	);

	if (isset($options['values']['id'])) {
		$conditions['id'] = array('!=' => $options['values']['id']);
	}

	return !Websites::first(compact('conditions'));
});

Validator::add('intRange', function ($value, $format, $options) {
	$defaults = array(
		'lower' => 0,
		'upper' => 100
	);

	$options += $defaults;
	$intVal = (int) $value;

	return (
		$intVal == $value &&
		$intVal >= $options['lower'] &&
		$intVal <= $options['upper']
	);
});

Validator::add('preg', function ($value, $format, $options) {
	return preg_match($options['match'], $value);
});

Validator::add('confirmPassword', function ($value, $format, $options) {
	$values = $options['values'];

	if (!isset($values['confirm']) || $values['confirm'] != $values['password']) {
		return FALSE;
	}

	return TRUE;
});


Validator::add('validWebsite', function ($value, $format, $options) {
	$values = $options['values'];

	//if (empty($value) || empty($values['owner_id'])) {
	if (empty($value)) {
		return FALSE;
	}

	return (boolean) Websites::first(array(
		'conditions' => array(
			'id'       => $value,
			//'owner_id' => $values['owner_id']
		)
	));
});


Validator::add('validLayout', function ($value, $format, $options)
{
    if (empty($value))
    {
        return FALSE;
    }

    return (boolean) Layouts::first(array(
        'conditions' => array(
            'id' => $value
        )
    ));
});


Validator::add('validClock', function($value, $format, $options)
{
    switch ($value)
    {
        case Campaigns::CLOCK_CLASIC:
            return TRUE;
            break;

        case Campaigns::CLOCK_48_TO_24:
            return TRUE;
            break;
    }

    return FALSE;
});


Validator::add('validEndDate', function ($value, $format, $options) {
	$values   = $options['values'];
	$timezone = new DateTimeZone('GMT');
	$current  = new DateTime('NOW', $timezone);

	try {
		$start = new DateTime($values['start_date'], $timezone);
		$end   = new DateTime($values['end_date'], $timezone);
	} catch(Exception $e) {
		return FALSE;
	}

	return $start < $end && $current < $end;
});


Validator::add('validStartDate', function ($value, $format, $options) {
	$timezone  = new DateTimeZone('GMT');
	$startDate = NULL;

	try {
		$startDate = new DateTime($value, $timezone);
	} catch(Exception $e) {}

	return !empty($startDate);
});


Validator::add('price', function ($value, $format, $options) {
	return is_numeric($value) && $value >= 0;
});


Validator::add('validTimeZone', function ($value, $format, $options) {
	return in_array($value, DateTimeZone::listIdentifiers());
});


Validator::add('confirmEmail', function ($value, $format, $options) {
	return $value == $options['values']['email'];
});

?>