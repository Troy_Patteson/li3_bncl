<?php

$titles = array(
  1  => 'Step 1: Essentials',
  2  => 'Step 2: Dates',
  3  => 'Step 3: Looks',
  4  => 'Step 4: Products',
  5  => 'Step 5: Vouchers',
  6  => 'Step 6: Notifications',
  7  => 'Step 7: Reports',
  8  => 'Step 8: Emails',
  9  => 'Step 9: Messages & Tracking',
  10 => 'Step 10: Preview & publish',
);

$currentStep = intVal($step, 10);
$prevStep    = $currentStep - 1;
$nextStep    = $currentStep + 1;

?>

<form class="" action="<?=$this->url($this->_request->params)?>" method="post">

  <div class="content-top">
      <div class="pull-right">
        <a href="<?=$this->url('Campaigns::index')?>" class="btn btn-close"><i class="icon-remove"></i></a>
      </div>

      <h1>Create campaign</h1>
  </div>

  <div class="content-middle">
      <fieldset>
        <h2><?=$titles[$currentStep]?></h2>
        <?php if ($errors) : ?>
        <div class="alert alert-error">
          <ul>
            <?php foreach ($errors as $error) : ?>
            <li><?php echo implode('<br />', $error)?></li>
            <?php endforeach; ?>
          </ul>
        </div>
        <?php endif; ?>
        <?php echo $this->_render('template', '_step-' . $step); ?>
      </fieldset>
  </div>

  <div class="content-bottom">
      <div class="form-actions">
        <div class="actions-left">
        <?php if ($currentStep != 1) : ?>
        <a href="<?=$this->url(array('args' => 'step-' . $prevStep) + $this->_request->params)?>" class="btn btn-prev">Prev<i class="icon-chevron-left"></i></a>
        <?php endif; ?>
        </div>
        <div class="actions-middle">
          <?php
            echo $this->_render('element', 'form-wizard', array(
              'stepsTitles' => $titles,
              'currentStep' => $currentStep,
              'unlocked'    => $unlocked,
              'baseHref'    => array('Campaigns::add', 'id' => $campaign->id)
            ));
          ?>
        </div>
        <div class="actions-right">
          <?php if ($currentStep != 10) : ?>
          <button type="submit" class="btn btn-next"><i class="icon-chevron-right"></i>Next</button>
          <?php else : ?>
          <button type="submit" class="btn btn-save"><i class="icon-chevron-right"></i>Publish</button>
          <?php endif; ?>
        </div>
      </div>
  </div>

</form>

<div class="tpl" id="tpl-product">
  <?php echo $this->_render('template', '_product', array('product' => NULL)); ?>
</div>

<div class="tpl" id="tpl-email">
  <?php echo $this->_render('template', '_email', array('email' => NULL)); ?>
</div>