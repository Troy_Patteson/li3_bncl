<table class="table table-striped">
	<thead>
		<tr>
			<th class="col-check"><input type="checkbox" name="check-all" data-target="websites[]" value="0" /></th>
			<th>User</th>
			<th class="col-updated">Updated</th>
		</tr>
	</thead>
	
	<tbody>
		<?php 
		foreach ($websites as $item) :
			$updated = date('M jS Y', strtotime($item->modified));
		?>
		<tr>
			<td class="col-check"><input type="checkbox" name="websites[]" value="<?=$item->id?>" /></td>
			<td><a href="<?=$this->url(array('Websites::edit', 'id' => $item->id))?>"><?=$item->host?></a></td>
			<td class="col-updated"><?=$updated?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
