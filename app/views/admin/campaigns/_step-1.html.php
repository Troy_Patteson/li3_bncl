
	<div class="control-group">
		<label class="control-label" for="inWebsite">Website</label>
		<div class="controls">
			<select class="" name="website_id" id="inWebsite">
				<option value="0"></option>

				<?php foreach ($websites as $website) : ?>
				<option <?php echo $website->id == $campaign->website_id ? 'selected="selected"' : ''?> value="<?=$website->id?>"><?=$website->host?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inTitle">Name</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="inTitle" name="title" value="<?=$campaign->title?>" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inSlug">Slug</label>
		<div class="controls">
			<input data-custom="<?=$this->_request->params['action'] == 'edit' ? 1 : 0?>" class="input-block-level" type="text" id="inSlug" name="slug" value="<?=$campaign->slug?>" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="inSystemTitle">System title</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="inSystemTitle" name="system_title" value="<?=$campaign->system_title?>" />
		</div>
	</div>
