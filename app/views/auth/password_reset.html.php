<form class="authentication span5" method="post" action="<?=$this->url($this->_request->url);?>">

  <div class="form-fields">
    <fieldset>
  		<legend>Password reset</legend>

<?php if ($invalid) : ?>
    <p>
      This URL is invalid or has expired.
    </p>
  </fieldset>
  
  <div class="form-actions">
    <a href="<?=$this->url('Auth::password_recovery')?>">Request another URL</a>
  </div>
<?php else : ?> 
  		
  	<?php if ($errors) : ?>
  		<div class="alert alert-error"><i class="icon-minus-sign"></i> <?=$errors?></div>
  	<?php endif; ?>
  		
  	<?php if ($success) : ?>
  		<div class="alert alert-success"><i class="icon-ok"></i> Your new password has been saved.</div>
  		<p>
  			You can now return to the <a href="<?=$this->url('Auth::login')?>"><i class="icon-lock"></i> log in screen</a> and authenticate using your new password.
  		</p>
  	<?php else : ?>
  		<p>
  			Please choose a new password for your account:
  		</p>
  		
  		<input type="password" name="password" id="password" placeholder="New Password" class="input-block-level" value="" />
  		<input type="password" name="confirm" id="confirm" placeholder="Confirm Password" class="input-block-level last" value="" />
  	</fieldset>
  </div>

	<div class="form-actions">
		<button type="submit" class="btn btn-authentication pull-right">Save</button>
		<a href="<?=$this->url(array('Auth::login', 'admin' => NULL))?>">Return to the login form</a>
	</div>
	<?php endif; ?>

<?php endif; ?>

</form>