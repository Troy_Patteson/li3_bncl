<div>
  <label class="control-label checkbox">
    <input type="checkbox" name="daily_report" value="1" <?php echo !empty($campaign->daily_report) ? 'checked' : '' ?> />
    Email daily
  </label>
</div>

<hr />

<div id="manageEmails">
	<?php
	foreach ($emails as $email) :
		echo $this->_render('template', '_email', compact('email'));
	endforeach;
	
	if (!$emails->count()) :
		echo $this->_render('template', '_email', array('email' => NULL));
	endif;
	?>
</div>

<div class="text-center">
	<button id="addEmail" class="btn btn-add" type="button"><i class="icon-plus-sign-alt"></i>Add email</button>
</div>
