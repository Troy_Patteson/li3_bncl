
You can reset your password for the {{INSERT APP NAME HERE}} platform by clicking <a href="<?=$url?>">here</a>. This URL will expire in 24 hours after it has been generated.<br />
<br />
If you are not able to click the above link, please copy and paste the following URL in your browser's address bar:<br />
<?=$url?><br />
<br />
<br />
Thank you,<br />
{{INSERT APP NAME HERE}} Technical Support 