
<?=$this->_render('template', '_errors')?>

<form method="post" action="<?=$this->url(array('args' => 'buy') + $this->_request->params)?>#errorsMsg">

	<?=$this->_render('template', '_products')?>

	<script type="text/javascript">
	jQuery( document ).ready(function($)
	{
		$('.campaign-form-row input[name="email_confirmation"]').change(function()
		{
			var email = $(this).val();
			var email_confirmation = $('.campaign-form-row input[name="email"]').val();

			if(email != email_confirmation)
			{
				$('.email-confirmation-error').show();
			}
			else
			{
				$('.email-confirmation-error').hide();
			}
		});
	});
	</script>

	<div class="campaign-customer-and-submit">
		<p>To purchase your voucher please enter your name, email and quantity and press Buy Now.</p>
		<fieldset class="campaign-buyer-details">
			<legend>Customer Details</legend>

			<div class="campaign-form-row clearfix">
				<label>First name</label>
				<input type="text" name="first_name" value="<?=$formData['first_name']?>" />
			</div>

			<div class="campaign-form-row clearfix">
				<label>Last name</label>
				<input type="text" name="last_name" value="<?=$formData['last_name']?>" />
			</div>

			<div class="campaign-form-row clearfix">
				<label>Email</label>
				<input type="email" name="email" value="<?=$formData['email']?>" />
			</div>

			<div class="campaign-form-row clearfix">
				<label>Confirm email</label>
				<input type="email" name="email_confirmation" value="<?=@$formData['email_confirmation']?>" />
				<div class="email-confirmation-error">The email and email confirmation do not match</div>
			</div>

			<div class="campaign-form-row clearfix">
				<label>Phone</label>
				<input type="text" name="phone" value="<?=$formData['phone']?>" />
			</div>
		</fieldset>

		<fieldset class="campaign-form-actions">
			<button type="submit">Buy now</button>
		</fieldset>
	</div>

</form>