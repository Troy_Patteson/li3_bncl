<form class="" action="<?=$this->url(array('Users::edit', 'id' => $this->_request->params['id']))?>" method="post">

  <div class="content-top">
		<div class="pull-right">
			<a href="<?=$this->url('Users::index')?>" class="btn btn-close"><i class="icon-remove"></i></a>
		</div>
		
		<h1>Edit user</h1>
	</div>
	
  <div class="content-middle">
		<fieldset>
			<h2>Editing <span class="text-success"><?=$editUser->email?></span></p>
			
			<?php if ($errors) : ?>
			<div class="alert alert-error">
				Please fix these errors and re-submit the form:
				<ul>
					<?php foreach ($errors as $error) : ?>
					<li><?php echo implode('<br />', $error)?></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>
			
			<?php if ($success) : ?>
			<div class="alert alert-success">The user's password has been updated.</div>
			<?php endif; ?>
			
			<div class="control-group">
				<label class="control-label" for="userEmail">Password</label>
				<div class="controls">
					<input class="input-block-level" type="password" name="password" value="" autocomplete="off" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="userEmail">Confirm Password</label>
				<div class="controls">
					<input class="input-block-level" type="password" name="confirm" value="" autocomplete="off" />
				</div>
			</div>
		</fieldset>
  </div>
		
  <div class="content-bottom">
		<div class="form-actions text-right">
      <button type="submit" class="btn btn-save"><i class="icon-ok"></i>Save</button>
		</div>
  </div>
	</form>
