<?php

use \lithium\net\http\Media;

?><!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->html->charset();?>
	<title><?=$campaign->title;?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="<?php echo $this->path('libs/placeholder-enhanced/js/jquery.placeholder-enhanced.min.js') ?>"></script>
</head>

<body>
	<?php
	$logo = 'logos/' . $campaign->website->host . '.png';
	
	if (file_exists(Media::webroot() . '/' . $logo)) :
	?>
		<div class="logo">
			<img src="<?=$this->url($logo)?>" />
		</div>
	<?php endif; ?>
	
	
	<?php echo $this->content(); ?>
</body>
</html>