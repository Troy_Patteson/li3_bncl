<?php

namespace app\models;

use \lithium\util\String;
use \lithium\net\http\Media;

use \li3_swiftmailer\mailer\Transports;
use \li3_swiftmailer\mailer\Message;

class EmailNotifications extends Model
{
	const EMAIL_SUBJECT      = 'Your campaign is running low on products';
	
	protected $_meta = array(
		'key'    => 'id',
		'source' => 'email_notifications',
		'locked' => TRUE,
	);
	
	protected $_schema = array(
		'id'          => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'campaign_id' => array('type' => 'integer',  'null' => FALSE),
		'address'     => array('type' => 'string',   'null' => FALSE),
	);
	
	public $validates = array(
		'address' => array(
			array('email', 'message' => 'Invalid email address', 'required' => TRUE)
		)
	);
	
	
	public static function sendFor($campaignId)
	{
		$campaign = Campaigns::completeDetails(array('id' => $campaignId));
		$products = $campaign->products->find(function($product) {
			return $product->threshold !== NULL && $product->remaining <= $product->threshold;
		});
		
		if (!$products->count()) {
			return NULL;
		}
		
		$recipients = static::findAllByCampaignId($campaign->id);
		
		$params   = compact('campaign', 'products');
		$renderer = Media::view('default', $params);
		$msgBody  = $renderer->render('all', $params, array(
			'controller' => 'admin/campaigns/email',
			'template'   => 'notifications',
			'handler'    => 'html'
		));
		
		$mailer  = Transports::adapter('default');
		$message = Message::newInstance()
			->setSubject(static::EMAIL_SUBJECT)
			->setFrom(array('no-reply@' . $campaign->website->host => $campaign->website->host))
			->setBody(wordwrap(strip_tags(preg_replace('/<br(.*?)>/', "\n", $msgBody))))
			->addPart($msgBody, 'text/html');
		
		foreach ($recipients as $to) {
			$message->setTo(array($to->address));
			$mailer->send($message);
		}
		
		foreach ($products as $product) {
			$product->threshold = $product->threshold == 0 ? NULL : floor($product->remaining / 3);
			$product->save();
		}
	}
}



?>