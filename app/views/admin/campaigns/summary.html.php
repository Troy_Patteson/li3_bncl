<?php
use \app\models\Products;
?>
	<div class="content-top">
		<div class="pull-right">
			<ul>
				<li><a href="<?=$this->url(array('Campaigns::index'))?>" class="btn btn-close"><i class="icon-remove"></i></a></li>
			</ul>
		</div>

		<h1><?= $campaign->title; ?> summary</h1>
	</div>

	<div class="content-middle">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Product Description</th>
				<th>Price</th>
				<th>Number Sold</th>
				<th>Number Extended</th>
				<th>Total Sales</th>
			</tr>
		</thead>

		<tbody>

			<?php foreach ($campaignItems as $item) : ?>
			<tr>
				<td>
					<?php if ($item->status != Products::STATUS_ACTIVE) : ?>
						<s><?=$item->product_name?></s>
					<?php else: ?>
						<?=$item->product_name?>
					<?php endif; ?>
				</td>
				<td><?=$website->currency_symbol . number_format($item->price, 2)?></td>
				<td><?=number_format($item->total_sold, 0)?></td>
				<td><?=number_format($item->total_extended, 0)?></td>
				<td><?=$website->currency_symbol . number_format($item->total_sales, 2)?></td>
			</tr>
			<?php endforeach; ?>

		</tbody>
	</table>
	</div>

	<div class="content-bottom">

	<?php
	echo $this->_render('element', 'pagination', array(
		'totalPages'  => $totalPages,
		'currentPage' => isset($this->_request->params['page']) ? $this->_request->params['page'] : 1,
		'baseUrl'     => array('Campaigns::summary', 'id' => $campaign->id),
	));
	?>
	</div>