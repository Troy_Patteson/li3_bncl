<form class="delete-form" action="<?=$this->url(array('Campaigns::delete', 'id' => @$this->_request->params['page']))?>" method="post">
    <div class="content-top">
        <div class="pull-right">
          <ul>
            <li><a href="<?=$this->url('Campaigns::unpublished')?>" class="btn btn-unpublished"><i class="icon-cloud"></i><span><?=$unpublished?></span></a></li>
            <li class="content-top-search"><a href="#" class="btn btn-search"><i class="icon-search"></i></a></li>
            <li><a href="<?=$this->url('Campaigns::add')?>" class="btn btn-create"><i class="icon-plus"></i></a></li>
            <li><button class="btn btn-delete" type="submit" disabled="disabled"><i class="icon-minus-sign"></i></button></li>
          </ul>
        </div>
        
        <h1>Campaigns list</h1>
    </div>
    
    <div class="content-middle">
        <?php if ($deleted) : ?>
        <div class="alert alert-success">The selected campaigns have been deleted</div>
        <?php endif; ?>
        
        <?php if ($success) : ?>
        <div class="alert alert-success">The campaign <strong><?=$success['title']?></strong> has been saved</div>
        <?php endif; ?>
        
        <?php
        if ($campaigns && $campaigns->pages) :
          echo $this->_render('template', '_listing');
        elseif (!$deleted && !$unpublished) :
        ?>
        <div class="alert alert-info">There are no campaigns matching the filtering criteria!</div>
        <?php endif; ?>
    </div>

    <div class="content-bottom">     
        <?php
        echo $this->_render('element', 'pagination', array(
          'totalPages'  => isset($campaigns->pages) ? $campaigns->pages : 1,
          'currentPage' => isset($this->_request->params['page']) ? $this->_request->params['page'] : 1,
          'baseUrl'     => 'Campaigns::index',
        ));
        ?>
    </div>
</form>

<?php echo $this->_render('template', 'reports-search-form'); ?>