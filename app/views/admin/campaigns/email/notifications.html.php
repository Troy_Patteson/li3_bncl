<h3><?=$campaign->title?></h3>

<p style="padding: 10px 0;">
	Your campaign is about to run out of the following products:
</p>

<table style="border: 1px solid #000; border-collapse: collapse">
	<tr>
		<th style="border: 1px solid #000; padding: 10px;">Item</th>
		<th style="border: 1px solid #000; padding: 10px;">Quantity</th>
	</tr>
	
	<?php foreach ($products as $product) : ?>
	<tr>
		<td style="border: 1px solid #000; padding: 10px;"><?=$product->name?></td>
		<td style="border: 1px solid #000; padding: 10px;"><b><?=intVal($product->remaining, 10)?></b> out of <b><?=intVal($product->vouchers, 10)?></b> remaining</td>
	</tr>
	<?php endforeach; ?>
</table>

<p style="padding: 10px 0;">
	<a href="<?=$campaign->editUrl(5)?>" target="_blank">Add more voucher codes</a>
</p>