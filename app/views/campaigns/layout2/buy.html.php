<?php

$soldOut = $campaign->soldOut() || $campaign->hasEnded();

?>
<div>
  <div class="campaign-wrapper clearfix">
    <h1><?=$campaign->title?></h1>

    <div class="campaign-form">

      <?php if(!empty($campaign->header)): ?>
      <div class="campaign-header"><?php echo $campaign->header; ?></div>
      <?php endif; ?>

      <?php
      if ($soldOut) :
        echo $this->_render('template', $layout->name . '/_sold_out', compact('soldOut'));
      else :
        echo $this->_render('template', $layout->name . '/_order_form', compact('soldOut'));
      endif;
      ?>

      <?php if(!empty($campaign->footer)): ?>
      <div class="campaign-footer"><?php echo $campaign->footer; ?></div>
      <?php endif; ?>

    </div>
  </div>

  <?=$this->_render('template', $layout->name . '/_style')?>
  <?=$this->_render('template', '_links')?>
</div>