<?php

$options = array('inline' => FALSE);
$this->html->script('/libs/file-upload/vendor/jquery.ui.widget', $options);
$this->html->script('/libs/file-upload/jquery.iframe-transport.js', $options);
$this->html->script('/libs/file-upload/jquery.fileupload.js', $options);

?>

<div id="campaignProducts">
	<?php
	foreach ($products as $product) :
		echo $this->_render('template', '_product', array('product' => $product));
	endforeach;

	if (!$products->count()) :
		echo $this->_render('template', '_product', array('product' => NULL));
	endif;
	?>
</div>

<div class="text-center">
	<button id="addProduct" class="btn btn-add" type="button"><i class="icon-plus-sign-alt"></i>Add product</button>
</div>
