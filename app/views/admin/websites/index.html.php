<form class="delete-form" action="<?=$this->url('Websites::delete')?>" method="post">

  <div class="content-top">
      <div class="pull-right">
        <ul>
          <li><a href="<?=$this->url('Websites::add')?>" class="btn btn-create"><i class="icon-plus"></i></a></li>
          <li><button class="btn btn-delete" type="submit" disabled="disabled"><i class="icon-minus-sign"></i></button></li>
        </ul>
      </div>
      
      <h1>Websites list</small></h1>
  </div>

  <div class="content-middle">
      <?php if ($deleted) : ?>
      <div class="alert alert-success">The selected websites have been deleted</div>
      <?php endif; ?>
      
      <?php if ($success) : ?>
      <div class="alert alert-success">The website <strong><?=$success['host']?></strong> has been saved</div>
      <?php endif; ?>
      
      <?php
      if ($websites && $websites->pages) :
        echo $this->_render('template', '_listing');
      elseif (!$deleted) :
      ?>
      <div class="alert">You haven't created any websites yet!</div>
      <?php endif; ?>
  </div>

  <div class="content-bottom">
      <?php
      echo $this->_render('element', 'pagination', array(
        'totalPages'  => isset($websites->pages) ? $websites->pages : 1,
        'currentPage' => isset($this->_request->params['page']) ? $this->_request->params['page'] : 1,
        'baseUrl'     => 'Websites::index',
      ));
      ?>
  </div>

</form>
