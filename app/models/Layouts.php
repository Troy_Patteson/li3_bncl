<?php namespace app\models;

class Layouts extends Model {

    protected $_meta = array(
        'key'     => 'id'
      , 'source'  => 'layouts'
      , 'locked'  => TRUE
    );

    protected $_schema = array(
        'id'  => array('type' => 'integer', 'null' => FALSE, 'key' => 'primary')
      , 'name'=> array('type' => 'string' , 'null' => FALSE, 'default' => NULL)
    );

    public $hasMany = array(
        'Campaigns' => array(
            'to'  => 'app\models\Campaigns'
          , 'key' => array('id' => 'layout_id')
        )
    );

}