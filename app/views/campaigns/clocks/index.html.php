<?php use app\models\Campaigns;

switch ($campaign->clock)
{
    case Campaigns::CLOCK_CLASIC:
        echo $this->_render('template', 'clocks/clock0', compact('campaign'));
        break;

    case Campaigns::CLOCK_48_TO_24:
        echo $this->_render('template', 'clocks/clock1', compact('campaign'));
        break;
}

?>