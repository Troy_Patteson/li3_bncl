class website {

  # Set the correct permissions on the 'app/resources/tmp' directory, if it exists.
  if $storage_directory_exists == 'true' {
    file { '/vagrant/app/resources/tmp':
      mode    => 0777,
      recurse => true,
    }
  }

}
