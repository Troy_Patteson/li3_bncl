# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # -----------------------------
  # Basics
  # -----------------------------

  config.vm.box = "precise32"
  config.vm.box_url = "http://files.vagrantup.com/precise32.box"

  # -----------------------------
  # Networking
  # -----------------------------

  config.vm.network :private_network, ip: '10.2.2.10'
  config.vm.network :forwarded_port, guest: 80, host: 8888
  config.vm.network :forwarded_port, guest: 3306, host: 8889

  # -----------------------------
  # Provisioning
  # -----------------------------

  config.vm.provision :shell, :inline => 'echo "Etc/UTC" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata'

  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = 'puppet/manifests'
    puppet.manifest_file  = 'base.pp'
    puppet.module_path    = 'puppet/modules'
  end

  # -----------------------------
  # Machine settings
  # -----------------------------

  config.vm.provider :virtualbox do |vb|
    vb.gui = false
    vb.customize ["modifyvm", :id, "--memory", "512", "--rtcuseutc", "on"]
  end

  # -----------------------------
  # Ownership
  # -----------------------------

  config.vm.synced_folder ".", "/vagrant", :nfs => true

end
