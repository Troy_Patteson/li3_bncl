<?php

namespace li3_paypal;

class ExpressCheckout extends \lithium\core\Object
{
	protected $_environment = 'production';
	
	protected $_currency = 'USD';
	
	protected $_credentials = array(
		'username'  => NULL,
		'password'  => NULL,
		'signature' => NULL
	);
	
	protected $_connections = array(
		'production' => NULL,
		'sandbox'    => NULL
	);
	
	protected $_endpoints = array(
		'production' => 'api-3t.paypal.com',
		'sandbox'    => 'api-3t.sandbox.paypal.com'
	);
	
	protected $_redirect = array(
		'production' => 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=',
		'sandbox'    => 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='
	);
	
	protected $_urls = array(
		'return' => NULL,
		'cancel' => NULL
	);
	
	protected $_products = array();
	
	protected $_total = 0;
	
	protected $_token         = NULL;
	protected $_payerId       = NULL;
	protected $_transactionId = NULL;
	protected $_response      = NULL;
	protected $_details       = NULL;
	
	protected $_autoConfig = array('credentials' => 'merge', 'urls' => 'merge', 'currency');
	
	protected $_classes = array(
		'service' => 'lithium\net\http\Service'
	);
	
	
	public function __construct($config = array())
	{
		$config += array(
			'sandboxed' => FALSE
		);
		
		$this->sandboxed($config['sandboxed']);
		
		parent::__construct($config);
	}
	
	
	public function credentials(array $params = array())
	{
		if ($params === array()) {
			return $this->_credentials;
		}
		
		$this->_credentials = $params + $this->_credentials;
	}
	
	
	public function currency($abbr = NULL)
	{
		if ($abbr === NULL) {
			return $this->_currency;
		}
		
		$this->_currency = $abbr;
	}
	
	
	public function urls(array $params = array())
	{
		if ($params === array()) {
			return $this->_urls;
		}
		
		$this->_urls = $params + $this->_urls;
	}
	
	
	public function sandboxed($flag = NULL)
	{
		if ($flag === NULL) {
			return $this->_environment == 'sandbox';
		}
		
		$this->_environment = $flag ? 'sandbox' : 'production';
	}
	
	
	public function addItem($item)
	{
		$item += array(
			'id'    => NULL,
			'name'  => NULL,
			'price' => 0,
			'qty'   => 0
		);
		
		$this->_products[] = $item;
		$this->_total += $item['price'] * $item['qty'];
	}
	
	
	public function create()
	{
		$data = $this->_defaults() + array(
			'METHOD' => 'SetExpressCheckout',
			'SOLUTIONTYPE' => 'Sole',
			'PAYMENTREQUEST_0_CURRENCYCODE'  => $this->_currency,
			'PAYMENTREQUEST_0_PAYMENTACTION' => 'SALE'
		);
		
		foreach ($this->_products as $key => $product) {
			$data['L_PAYMENTREQUEST_0_NAME'   . $key] = $product['name'];
			$data['L_PAYMENTREQUEST_0_NUMBER' . $key] = intVal($product['id'], 10);
			$data['L_PAYMENTREQUEST_0_AMT'    . $key] = number_format($product['price'], 2, '.', '');
			$data['L_PAYMENTREQUEST_0_QTY'    . $key] = intVal($product['qty'], 10);
		}
		
		$data['PAYMENTREQUEST_0_AMT'] = number_format($this->_total, 2, '.', '');
		
		if (!$this->_makeRequest($data) || strtolower($this->_response['ACK']) != 'success') {
			return FALSE;
		}
		
		$this->_token = $this->_response['TOKEN'];
		
		return TRUE;
	}
	
	
	public function checkout($redirect = TRUE)
	{
		$url = $this->_redirect[$this->_environment] . $this->token();
		
		if (!$redirect) {
			return $url;
		}
		
		header('Location: ' . $url);
		exit();
	}
	
	
	public function getDetails($token)
	{
		$data = $this->_defaults() + array(
			'METHOD' => 'GetExpressCheckoutDetails',
			'TOKEN'  => $token
		);
		
		if (!$this->_makeRequest($data) || strtolower($this->_response['ACK']) != 'success') {
			return FALSE;
		}
		
		$this->_details = $this->_response;
		$this->_token   = $this->_response['TOKEN'];
		$this->_payerId = $this->_response['PAYERID'];
		
		return TRUE;
	}
	
	
	public function capture()
	{
		if (!$this->_details) {
			return FALSE;
		}
		
		$data = $this->_defaults() + array(
			'METHOD'                         => 'DoExpressCheckoutPayment',
			'TOKEN'                          => $this->token(),
			'PAYERID'                        => $this->payerId(),
			'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
			'PAYMENTREQUEST_0_AMT'           => $this->_details['PAYMENTREQUEST_0_AMT'],
			'PAYMENTREQUEST_0_ITEMAMT'       => $this->_details['PAYMENTREQUEST_0_AMT'],
			'PAYMENTREQUEST_0_CURRENCYCODE'  => $this->_details['PAYMENTREQUEST_0_CURRENCYCODE']
		);
		
		if (!$this->_makeRequest($data)) {
			return FALSE;
		}
		
		$statuses = array(
			'Completed',
			'Pending',
			'Processed',
			'Completed-Funds-Held',
		);
		
		$response = $this->_response + array(
			'PAYMENTINFO_0_PAYMENTSTATUS' => NULL
		);
		
		if (!in_array($response['PAYMENTINFO_0_PAYMENTSTATUS'], $statuses)) {
			return FALSE;
		}
		
		$this->_transactionId = $response['PAYMENTINFO_0_TRANSACTIONID'];
		
		return TRUE;
	}
	
	
	public function total()
	{
		return $this->_total;
	}
	
	
	public function token()
	{
		return $this->_token;
	}
	
	
	public function payerId()
	{
		return $this->_payerId;
	}
	
	
	public function transactionId()
	{
		return $this->_transactionId;
	}
	
	
	public function response()
	{
		return $this->_response;
	}
	
	
	public function errors()
	{
		$errors = array();
		
		foreach ($this->_response as $key => $val) {
			if (preg_match('/^L_ERRORCODE(?<id>\d+)$/', $key, $matches)) {
				$message = $this->_response['L_LONGMESSAGE' . $matches['id']];
				
				if (!in_array($message, $errors)) {
					$errors[$val] = $message;
				}
			}
		}
		
		return $errors;
	}
	
	
	protected function _endpoint()
	{
		return $this->_endpoints[$this->_environment];
	}
	
	
	protected function _connection()
	{
		if (empty($this->_connections[$this->_environment])) {
			$this->_connections[$this->_environment] = $this->_instance('service', array(
				'scheme' => 'https',
				'host'   => $this->_endpoint(),
				'port'   => 443
			));
		}
		
		return $this->_connections[$this->_environment];
	}
	
	
	protected function _defaults()
	{
		return array(
			'USER'      => $this->_credentials['username'],
			'PWD'       => $this->_credentials['password'],
			'SIGNATURE' => $this->_credentials['signature'],
			'VERSION'   => 89,
			
			'returnUrl' => $this->_urls['return'],
			'cancelUrl' => $this->_urls['cancel'],
		);
	}
	
	
	protected function _makeRequest($data)
	{
		$response = $this->_connection()->post('/nvp', $data);
		
		parse_str($response, $this->_response);
		
		$this->_response += array(
			'ACK'   => NULL,
			'TOKEN' => NULL,
		);
		
		
		return $response ? $this->_response : FALSE;
	}
}

?>