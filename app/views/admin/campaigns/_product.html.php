<?php

use \app\models\Products;

$product = isset($product) ? $product : Products::create();

$productField = $product->exists() ? 'products[' . $product->id . ']' : 'new-products[]';
$priceField   = $product->exists() ? 'prices[' . $product->id . ']' : 'new-prices[]';
$extendable   = $product->exists() ? 'extendable[' . $product->id . ']' : 'new-extendable[]';
$pdfField     = $product->exists() ? 'pdfs[' . $product->id . ']' : 'new-pdfs[]';

$errors = $product->errors();

?>

<div>

	<div class="control-group product-listing">
		<label class="control-label">Product name &amp; price</label>
		<div class="controls">
			<input class="input-large <?=isset($errors['name']) ? 'error' : ''?>" type="text" name="<?=$productField?>" value="<?=$product->name?>" />

			<div class="input-append">
				<input class="input-mini <?=isset($errors['price']) ? 'error' : ''?>" type="text" name="<?=$priceField?>" value="<?=$product->price?>" />
				<span class="add-on"><?=$website->currency_abbreviation?></span>
			</div>

			<button class="btn btn-delete remove-item" type="button"><i class="icon-minus-sign"></i></button>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">Attachment</label>
		<div class="controls">
			<div class="image-upload <?=!empty($product->pdf->id) ? 'has-image' : ''?>">
				<div class="fileupload fileupload-new">
					<div class="input-append">
						<div class="uneditable-input"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div>
						<span class="btn btn-file">
							<span class="fileupload-new"><i class="icon-upload"></i></span>
							<span class="fileupload-exists"><i class="icon-upload"></i></span>
							<input data-url="<?=$this->url('Campaigns::upload_pdf')?>" type="file" name="pdf" />
						</span>
						<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
					</div>
				</div>

				<small class="fileupload-errors text-error"></small>

				<div class="fileupload-progress">
					<div class="progress progress-striped active">
						<div class="bar"></div>
					</div>
				</div>

				<div class="fileupload-complete">
					<div class="fileupload-label input-large text-success pull-left"><?=@$product->pdf->file_name ?></div>
					<button type="button" class="btn btn-delete"><i class="icon-minus-sign"></i></button>
					<input type="hidden" name="<?=$pdfField?>" value="<?=@$product->pdf->id ?>" />
				</div>
			</div>
		</div>
	</div>

	<hr />

</div>