	
	<div class="control-group">
		<label class="control-label" for="wsApiUsername">Paypal API username</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsApiUsername" name="api_username" value="<?=$website->api_username?>" autocomplete="off" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="wsApiPassword">Paypal API password</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsApiPassword" name="api_password" value="<?=$website->api_password?>" autocomplete="off" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="wsApiSignature">Paypal API signature</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsApiSignature" name="api_signature" value="<?=$website->api_signature?>" autocomplete="off" />
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="wsCurrency">Currency abbreviation</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsCurrency" name="currency_abbreviation" value="<?=$website->currency_abbreviation?>" autocomplete="off" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="wsSymbol">Currency symbol</label>
		<div class="controls">
			<input class="input-block-level" type="text" id="wsSymbol" name="currency_symbol" value="<?=$website->currency_symbol?>" autocomplete="off" />
		</div>
	</div>
