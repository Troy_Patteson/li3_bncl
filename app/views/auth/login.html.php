<form class="authentication span5" method="post" action="<?=$this->url($this->_request->params);?>">

  <div class="form-fields">
  	<fieldset>
  		<legend>Login</legend>
  		
  		<?php if (@$errors) : ?>
  		<div class="alert alert-error"><i class="icon-minus-sign"></i> Invalid username or password.</div>
  		<?php endif; ?>
  		
  		<input type="text" name="email" id="email" placeholder="Email" class="input-block-level" value="<?=@$this->_request->data['email']?>" />
  		<input type="password" name="password" id="password" placeholder="Password" class="input-block-level last" />
  	</fieldset>
  </div>
	
	<div class="form-actions">
		<button type="submit" class="btn btn-authentication pull-right">Login</button>
		<a href="<?=$this->url(array('Auth::password_recovery', 'admin' => NULL))?>">Forgot your password?</a>
	</div>

</form>