<?php

namespace app\models;

class Products extends Model
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE  = 1;

	const EXTENSION_PRICE = 5.00;

	protected $_meta = array(
		'key'    => 'id',
		'source' => 'products',
		'locked' => TRUE,
	);

	protected $_schema = array(
		'id'          => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'campaign_id' => array('type' => 'integer',  'null' => FALSE),
		'name'        => array('type' => 'string',   'null' => FALSE),
		'price'       => array('type' => 'float',    'null' => FALSE),
		'vouchers'    => array('type' => 'integer',  'null' => FALSE),
		'threshold'   => array('type' => 'integer',  'null' => TRUE,  'default' => NULL),
		'buy_limit'   => array('type' => 'integer',  'null' => FALSE, 'default' => 0),
		'created'     => array('type' => 'datetime', 'null' => FALSE),
		'status'      => array('type' => 'integer',  'null' => FALSE),
	);

	public $hasOne = array(
			'Pdf' => array(
					'to'  => 'app\models\Pdfs'
			)
	);

	public $validates = array(
		'name' => array(
			array('notEmpty', 'message' => 'You specified an invalid product name', 'required' => TRUE),
		),

		'price' => array(
			array('price', 'message' => 'Invalid product price', 'required' => TRUE),
		),

		'vouchers' => array(
			array('intRange', 'lower' => 1, 'upper' => 999999, 'message' => 'Invalid vouchers number', 'required' => TRUE),
		),
	);


	public static function forCampaign($campaignId)
	{
		$products = static::all(array(
			'conditions' => array(
				'Products.campaign_id' => $campaignId,
				'Products.status'      => static::STATUS_ACTIVE
			),

			'fields' => array(
				'Products' => array_keys(Products::schema()->fields()),
				'Pdf'      => array_keys(Pdfs::schema()->fields()),
				array(
					'COUNT(Vouchers.id) AS available_vouchers',
				)
			),

			'joins' => array(
				array(
					'type'        => 'LEFT',
					'source'      => 'vouchers',
					'alias'       => 'Vouchers',
					'constraints' => 'Vouchers.product_id = Products.id AND Vouchers.order_item_id IS NULL AND Vouchers.extended = 0'
				)
			),

			'group' => array('Products.id'),
			'order' => array('Products.created' => 'ASC'),
			'with'  => 'Pdf'
		));

		$availability = static::availability($products->values('id'));

		foreach ($products as $prod) {
			$count = $availability->offsetGet($prod->id);
			$prod->remainingExtended = $count->remainingExtended;
			$prod->vouchersExtended  = $count->totalExtended;
		}


		return $products;
	}


	public function attachVouchers($entity, $count, $prefix = '', $extended = 0)
	{
		$count = intVal($count, 10);

		if (!$count) {
			return;
		}

		$entity->vouchers += $count;

		if (!$extended) {
			$entity->save(array(), array(
				'validate' => FALSE
			));
		}

		$vouchers = array();
		for ($i = 0; $i < $count; $i++) {
			$vouchers[] = static::generateVoucher($prefix);
		}

		$created = gmdate('Y-m-d H:i:s');
		$query = 'INSERT INTO `vouchers` (`product_id`, `voucher`, `extended`, `created`, `status`) VALUES {:values}';

		do {
			$batch = array_splice($vouchers, 0, 2000);
			foreach ($batch as $key => $voucher) {
				$batch[$key] = array(
					'product_id' => $entity->id,
					'voucher'    => $voucher,
					'extended'   => $extended,
					'created'    => $created,
					'status'     => Vouchers::STATUS_ACTIVE
				);
			}
			static::executeQuery($query, $batch);
		} while (!empty($vouchers));
	}

	public static function generateVoucher($prefix = '')
	{
      static $count = 0;
      $count++;

      $seed  = str_pad($count, 7, '0', STR_PAD_LEFT);
      $time  = microtime(TRUE) * 10000 + $count;
      $rand  = mt_rand(1000000, 9999999);
      $value = base_convert($rand . $seed . $time, 10, 26);
      $value = substr($value, -15);
      $check = preg_replace('/[^0-9]/', '', $value);

      do {
          $check = array_sum(str_split($check));
      } while (strlen($check) > 1);

      $value = $prefix . $value . $check;

      return $value;
	}

	public static function availability($productsIds)
	{
		$productsIds = (array) $productsIds;

		if (count($productsIds)) {
			$query = array(
				'conditions' => array(
					'product_id'    => $productsIds,
					'order_item_id' => NULL,
					'extended'      => 0
				),

				'fields' => array(
					'product_id AS id',
					'COUNT(id) AS remaining'
				),

				'group' => array('product_id')
			);

			$remainingRegular = Vouchers::all($query);

			$query['conditions']['extended'] = 1;
			$remainingExtended = Vouchers::all($query);

			unset($query['conditions']['order_item_id']);
			$totalExtended = Vouchers::all($query);
		}

		$products = static::buildSet();

		foreach ($productsIds as $id) {
			$remaining    = $remainingRegular ? $remainingRegular->offsetGet($id) : NULL;
			$remainingExt = $remainingExtended ? $remainingExtended->offsetGet($id) : NULL;
			$totalExt     = $totalExtended ? $totalExtended->offsetGet($id) : NULL;

			$products->offsetSet($id, static::create(array(
				'id'                => $id,
				'remaining'         => $remaining ? $remaining->remaining : 0,
				'remainingExtended' => $remainingExt ? $remainingExt->remaining : 0,
				'totalExtended'     => $totalExt ? $totalExt->remaining : 0,
			)));
		}

		return $products;
	}


	public function save($entity, $data = NULL, array $options = array())
	{
		if (!empty($data)) {
			$entity->set($data);
			$data = NULL;
		}

		if ($entity->modified('vouchers')) {
			$vouchers = static::availability($entity->id)->first();
			$vouchers = $vouchers ? $vouchers->remaining : 0;

			$export = $entity->export();

			$entity->threshold = floor(($export['update']['vouchers'] - $export['data']['vouchers'] + $vouchers) / 3);
		}

		parent::save($entity, $data, $options);
	}


	public static function summary($campaignId, $params)
	{
		$params += array(
			'page'  => 1,
			'limit' => Campaigns::DEFAULT_COUNT
		);

		$conditions = array('conditions' => array(
			'Products.campaign_id' => $campaignId
		));

		$query = array(
			'fields' => array(
				array('Products.name AS product_name'),
				array('Products.price AS price'),
				array('Products.status AS status'),
				array('SUM(OrdersItems.quantity) AS total_sold'),
				array('SUM(OrdersItems.quantity * OrdersItems.extended) AS total_extended'),
				array('SUM(OrdersItems.quantity * OrdersItems.price + OrdersItems.quantity * OrdersItems.extended * ' . static::EXTENSION_PRICE . ') AS total_sales'),
			),

			'joins' => array(
				array(
					'type'        => 'LEFT',
					'source'      => 'orders_items',
					'alias'       => 'OrdersItems',
					'constraints' => 'OrdersItems.product_id = Products.id'
				),
				array(
					'type'        => 'LEFT',
					'source'      => 'orders',
					'alias'       => 'Orders',
					'constraints' => 'Orders.id = OrdersItems.order_id'
				)
			),

			'group' => array('Products.id'),

			'order' => array('Products.price' => 'ASC', 'Products.name' => 'ASC')
		) + $conditions + $params;

		$totalPages = ceil(Products::count($conditions) / $params['limit']);

		array_push($query['conditions'], array(
			'Orders.status' => Orders::STATUS_COMPLETED
		));

		$products   = Products::all($query);

		return array(
			'campaignItems' => $products,
			'totalPages'    => $totalPages
		);
	}
}

?>
