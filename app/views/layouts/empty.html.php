<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->html->charset();?>
	<title>MiniBox Admin &gt; <?php echo $this->title(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<link href="<?php echo $this->path('libs/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet" />
	<link href="<?php echo $this->path('libs/jasny-bootstrap/css/jasny-bootstrap.css'); ?>" rel="stylesheet" />
	<link href="<?php echo $this->path('libs/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
  <link href="<?php echo $this->path('libs/placeholder-enhanced/css/placeholder-enhanced.css'); ?>" rel="stylesheet" />

	
	<link href="<?php echo $this->path('css/main.css'); ?>" rel="stylesheet" />
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->path('ico/apple-touch-icon-144-precomposed.png'); ?>" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->path('ico/apple-touch-icon-114-precomposed.png'); ?>" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->path('ico/apple-touch-icon-72-precomposed.png'); ?>" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo $this->path('ico/apple-touch-icon-57-precomposed.png'); ?>" />
	<link rel="shortcut icon" href="<?php echo $this->path('favicon.ico'); ?>" />
</head>

<body>
	<?php echo $this->content(); ?>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="<?php echo $this->path('libs/placeholder-enhanced/js/jquery.placeholder-enhanced.min.js') ?>"></script>
</body>
</html>