
  <div class="content-top">
    <div class="pull-right">
      <ul>
        <?php if (!empty($campaign)) : ?>
        <li><a href="<?=$this->url(array('Campaigns::report', 'id' => $campaign->id))?>" class="btn btn-close"><i class="icon-remove"></i></a></li>
        <?php else : ?>
        <li><a href="<?=$this->url(array('Campaigns::index'))?>" class="btn btn-close"><i class="icon-remove"></i></a></li>
        <?php endif; ?>
      </ul>
    </div>

    <?php if (!empty($campaign)) : ?>
    <h1>Search <?=$campaign->title?> report</h1>
    <?php else : ?>
    <h1>Search reports</h1>
    <?php endif; ?>
  </div>

  <div class="content-middle">
  <?php if(!$orderItems->count()): ?>
    <h2>No search results</h2>
  <?php else: ?>
  <table class="table table-striped">
    <thead>
      <tr>
        <?php if (empty($campaign)) : ?>
        <th>Campaign</th>
        <?php endif;?>

        <th>Code</th>
        <th>Transaction</th>
        <th>Product</th>
        <th>Price</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Phone</th>
        <th class="col-updated">Order date</th>
      </tr>
    </thead>

    <tbody>

      <?php foreach ($orderItems as $item) : ?>
      <tr>
        <?php if (empty($campaign)) : ?>
        <td>
          <strong><?=$item->website_host?></strong><br />
          <?=$item->campaign_title?><br />
        </td>
        <?php endif;?>

        <td><code><?=$item->formatCode()?></code></td>
        <td><?=$item->transaction_id?></td>
        <td><?=$item->product_name?></td>
        <td><?=$item->friendly_price?></td>
        <td><?=$item->first_name?></td>
        <td><?=$item->last_name?></td>
        <td><?=$item->email?></td>
        <td><?=$item->phone?></td>
        <td class="col-updated"><?=$item->order_date?></td>
      </tr>
      <?php endforeach; ?>

    </tbody>
  </table>
  <?php endif; ?>
  </div>

  <div class="content-bottom">
  <?php

  $baseUrl = array('Campaigns::search');

  if (!empty($campaign)) {
    $baseUrl['id'] = $campaign->id;
  }

  echo $this->_render('element', 'pagination', array(
    'totalPages'  => $totalPages,
    'currentPage' => isset($this->_request->params['page']) ? $this->_request->params['page'] : 1,
    'baseUrl'     => $baseUrl,
    'query'       => $this->_request->query
  ));
  ?>
  </div>

<div class="tpl" id="tpl-search">
  <div class="section-search">
    <?php if (!empty($campaign)) : ?>
    <form class="form-inline" action="<?=$this->url(array('Campaigns::search', 'id' => $campaign->id))?>" method="post">
    <?php else : ?>
    <form class="form-inline" action="<?=$this->url(array('Campaigns::search'))?>" method="post">
    <?php endif; ?>
      <input class="input-small" type="text" name="email" placeholder="Email" />
      <input class="input-small" type="text" name="first_name" placeholder="First name" />
      <input class="input-small" type="text" name="last_name" placeholder="Last name" />
      <button type="submit" class="btn">Search</button>
    </form>
  </div>
</div>