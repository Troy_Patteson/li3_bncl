
<form class="" action="<?=$this->url($this->_request->params)?>" method="post">
	<div class="content-top">
		<div class="pull-right">
			<a href="<?=$this->url('Campaigns::index')?>" class="btn btn-close"><i class="icon-remove"></i></a>
		</div>
		
		<h1>Duplicate campaign <em><?=$campaign->title?></em></h1>
	</div>
	
	<div class="content-middle">
		<fieldset>
			<h2>Are you sure you want to duplicate this campaign?</h2>
		</fieldset>
	</div>
	
	<div class="content-bottom">
		<div class="form-actions">
			<div class="actions-left">
				
			</div>
			
			<div class="actions-middle">
				
			</div>
			
			<div class="actions-right">
				<button type="submit" class="btn btn-next"><i class="icon-chevron-right"></i>Next</button>
			</div>
		</div>
	</div>
</form>