<?php

namespace app\extensions\command;

use lithium\analysis\Logger;

use \app\models\Campaigns;

class EmailCampaignsReports extends \lithium\console\Command
{
	public function run() {
		Campaigns::processEnding();
	}
}

?>
