<?php

namespace app\controllers\root;

use app\models\Users;
use li3_extras\storage\Session;
use lithium\util\Inflector;

class IndexController extends \app\controllers\AuthController
{
	public function _init()
	{
		parent::_init();
		
		$this->_render['layout'] = 'admin';
	}
	
	
	public function render(array $options = array())
	{
		$controller = Inflector::underscore($this->request->params['controller']);
		
		$options += array(
			'controller' => 'root/' . $controller
		);
		
		return parent::render($options);
	}
	
	
	public function index()
	{
		return $this->redirect('Users::index');
	}
	
	
	public function account()
	{
		$this->set(array(
			'errors'  => array(),
			'success' => Session::flash('flash.passwordChanged')
		));
		
		$user = Users::first($this->user['id']);
		
		if (!$this->request->is('post')) {
			return;
		}
		
		$data = array_intersect_key($this->request->data, array('password' => '', 'confirm' => ''));
		$user->set($data);
		
		if ($user->save()) {
			Session::write('flash.passwordChanged', TRUE);
			return $this->redirect('Index::account');
		}
		
		$this->set(array(
			'errors' => $user->errors()
		));
	}
}

?>