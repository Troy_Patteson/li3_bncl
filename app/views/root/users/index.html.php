<form class="delete-form" action="<?=$this->url('Users::delete')?>" method="post">

    <div class="content-top">
			<div class="pull-right">
        <ul>
  				<li><a href="<?=$this->url('Users::add')?>" class="btn btn-create"><i class="icon-plus"></i></a></li>
  				<li><button class="btn btn-delete" type="submit" disabled="disabled"><i class="icon-minus-sign"></i></button></li>
        </ul>
      </div>
			
			<h1>Users list</h1>
		</div>
		
    <div class="content-middle">
		<?php if ($deletedUsers) : ?>
		<div class="alert alert-success">The selected users have been deleted</div>
		<?php endif; ?>
		
		<?php
		if ($admins->pages) :
			echo $this->_render('template', '_listing');
		elseif (!$deletedUsers) :
		?>
		<div class="alert">You haven't created any users yet!</div>
		<?php endif; ?>
    </div>

    <div class="content-bottom">     
      <?php
      echo $this->_render('element', 'pagination', array(
        'totalPages'  => isset($admins->pages) ? $admins->pages : 1,
        'currentPage' => isset($this->_request->params['page']) ? $this->_request->params['page'] : 1,
        'baseUrl'     => 'Users::index',
      ));
      ?>
    </div>
</div>

</form>