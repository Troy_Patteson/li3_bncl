<?php if (!empty($isCheckout)) : ?>
<script type="text/javascript">
    var _gaq = _gaq || [];

    _gaq.push(['_set', 'currencyCode', '<?=$campaign->website->currency_abbreviation?>']);

    _gaq.push(['_addTrans'
      , '<?=$order->transactionId()?>'
      , '<?=$campaign->website->host?>'
      , '<?=$order->value?>'
      , '0.00'
      , '0.00'
      , ''
      , ''
      , ''
    ]);

  <?php foreach ($order->products as $item) : ?>
    _gaq.push(['_addItem'
      , '<?=$order->transactionId()?>'
      , '<?=$item->product_id?>'
      , '<?=$item->product->name?>'
      , ''
      , '<?=$item->price?>'
      , '<?=$item->quantity?>'
    ]);
  <?php endforeach; ?>

    _gaq.push(['_trackTrans']);
</script>
<?php endif; ?>