<?php

$timezone    = new DateTimeZone('GMT');
$currentDate = new DateTime('NOW', $timezone);
$endDate     = new DateTime($campaign->end_date, $timezone);

$countdown   = $endDate->diff($currentDate);
$countdown   = $countdown->invert ? $countdown : $currentDate->diff($currentDate);

$difference  = max(0, $endDate->getTimestamp() - $currentDate->getTimestamp());

?>

<div class="campaign-sidebar">
  <div class="campaign-countdown">

    <h2>Time remaining</h2>

    <div class="campaign-countdown-label">
      <div class="campaign-countdown-row clearfix">
        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits days">-</div>
          <div class="campaign-countdown-block">Days</div>
        </div>

        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits">&nbsp;</div>
        </div>

        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits hours">--</div>
          <div class="campaign-countdown-block">Hrs</div>
        </div>

        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits">:</div>
        </div>

        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits minutes">--</div>
          <div class="campaign-countdown-block">Mins</div>
        </div>

        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits">:</div>
        </div>

        <div class="campaign-countdown-column">
          <div class="campaign-countdown-block campaign-countdown-digits seconds">--</div>
          <div class="campaign-countdown-block">Secs</div>
        </div>
      </div>

    </div>

    <?php /*
    <?=$countdown->format('%a days and %h hours left');?>
    */ ?>
  </div>

  <img class="campaign-details-image" src="<?=$this->url($campaign->details_image->url())?>" />
</div>

<?=$this->_render('template', '_timer', compact('difference'))?>