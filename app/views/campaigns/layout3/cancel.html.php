<div>
  <div class="campaign-wrapper clearfix">
    <h1><?=$campaign->title?></h1>

    <div class="campaign-form">
      <div class="campaign-cancel-msg">
        <?php echo $message;?>
      </div>
      <a href="<?=$this->url(array('args' => 'buy') + $this->_request->params)?>">Return to campaign</a>
    </div>
  </div>

  <?=$this->_render('template', $layout->name . '/_style')?>
</div>