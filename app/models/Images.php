<?php

namespace app\models;

class Images extends Model
{
	const MAX_WIDTH = 960;
  
  protected $_meta = array(
		'key'    => 'id',
		'source' => 'images',
		'locked' => TRUE,
	);
	
	protected $_schema = array(
		'id'        => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'owner_id'  => array('type' => 'integer',  'null' => FALSE),
		'path'      => array('type' => 'string',   'null' => FALSE),
		'file_name' => array('type' => 'string',   'null' => FALSE),
		'file_size' => array('type' => 'integer',  'null' => FALSE),
		'width'     => array('type' => 'integer',  'null' => FALSE),
		'height'    => array('type' => 'integer',  'null' => FALSE),
		'created'   => array('type' => 'datetime', 'null' => FALSE),
	);
	
	public static $allowedTypes = array(
		IMAGETYPE_JPEG => 'JPEG',
		IMAGETYPE_PNG  => 'PNG',
	);
	
	
	public function validates($entity, array $options = array())
	{
		$errors = array();
		
		if (!empty($entity->error)) {
			switch ($entity->error) {
				case UPLOAD_ERR_INI_SIZE :
				case UPLOAD_ERR_FORM_SIZE :
					$errors[] = 'The file is to large. Maximum upload size is <b>' . static::maxUploadSize() . '</b>';
				break;
				
				case UPLOAD_ERR_PARTIAL :
					$errors[] = 'Transfer failed. Please try again.';
				break;
				
				case UPLOAD_ERR_NO_FILE :
					$errors[] = 'Missing file.';
				break;
				
				default :
					$errors[] = 'Internal server error. Please try again.';
				break;
			}
		}
		
		if (!empty($entity->tmp_name) && file_exists($entity->tmp_name) && is_readable($entity->tmp_name)) {
			$imageData = getimagesize($entity->tmp_name);
			
			if (!$imageData || !array_key_exists($imageData[2], static::$allowedTypes)) {
				$errors[] = 'Uploaded file is not an image. Allowed formats are ' . implode(', ', static::$allowedTypes);
			}
			
			if ($imageData) {
				$entity->width  = $imageData[0];
				$entity->height = $imageData[1];
				$entity->type   = $imageData[2];
			}
		}
		
		$entity->errors($errors);
		
		return empty($errors);
	}
	
	
	public function save($entity, $data = null, array $options = array())
	{
		if (!$entity->validates()) {
			return FALSE;
		}
		
		if (!empty($entity->tmp_name) && is_uploaded_file($entity->tmp_name) && !$entity->_store()) {
			return FALSE;
		}
		
		return parent::save($entity, $data, $options);
	}
	
	
	public function url($entity)
	{
		return 'images/' . md5($entity->owner_id) . '/' . $entity->path;
	}
	
	
	public function _store($entity)
	{
		$newWidth  = min($entity->width, static::MAX_WIDTH);
		$newHeight = round($entity->height * $newWidth / $entity->width);
		$imgType   = strtolower(static::$allowedTypes[$entity->type]);
		$create    = 'imagecreatefrom' . $imgType;
		$imgName   = md5(mt_rand() . microtime(TRUE)) . '.jpg';
		$filePath  = LITHIUM_APP_PATH . '/webroot/images/' . md5($entity->owner_id) . '/';
		
		if (!static::mkdirRecursive($filePath)) {
			$entity->errors(array(
				'Internal server error. Please try again later.'
			));
			
			return FALSE;
		}
		
		$oldImage = $create($entity->tmp_name);
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		
		imagecopyresampled($newImage, $oldImage, 0, 0, 0, 0, $newWidth, $newHeight, $entity->width, $entity->height);
		
		$result = imagejpeg($newImage, $filePath . $imgName, 100);
		
		imagedestroy($oldImage);
		imagedestroy($newImage);
		
		if ($result) {
			$entity->set(array(
				'path'      => $imgName,
				'file_name' => $entity->name,
				'file_size' => filesize($filePath . $imgName),
				'width'     => $newWidth,
				'height'    => $newHeight
			));
			
			chmod($filePath . $imgName, 0777);
		}
		
		return $result;
	}
	
	
	public static function maxUploadSize()
	{
		$maxSize = array(
			ini_get('post_max_size'),
			ini_get('upload_max_filesize')
		);
		
		array_walk($maxSize, function(&$val, $key) {
			$val  = trim($val);
			$unit = strtoupper(substr($val, -1));
			
			switch ($unit) {
				case 'G' :
					$val *= 1024;
				case 'M' :
					$val *= 1024;
				case 'K' :
					$val *= 1024;
			}
			
			$val = (float) $val;
		});
		
		$size = min($maxSize);
		
		foreach (array(' KB', ' MB', ' GB') as $unit) {
			if ($size / 1024 >= 1) {
				$size = ($size / 1024) . $unit;
				continue;
			}
			
			break;
		}
		
		return $size;
	}
	
	
	public function mkdirRecursive($path, $mode = 0777)
	{
		$path = rtrim($path, '/');
		$dirs = explode('/' , $path);
		$path = $dirs[0];
		
		for ($d = 1; $d < count($dirs); $d++) {
			$path .= '/' . $dirs[$d];
			
			if (!is_dir($path) && !mkdir($path)) {
				return FALSE;
			}
			
			@chmod($path, $mode);
		}
		
		return TRUE;
	}
}



?>
