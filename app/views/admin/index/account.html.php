<form class="" action="<?=$this->url('Index::account')?>" method="post">

  <div class="content-top">
  		<h1>Account</h1>
  </div>
	
  <div class="content-middle">
  		<fieldset>
  			<h2>Change your password</h2>
  			
  			<?php if ($errors) : ?>
  			<div class="alert alert-error">
  				Please fix these errors and re-submit the form:
  				<ul>
  					<?php foreach ($errors as $error) : ?>
  					<li><?php echo implode('<br />', $error)?></li>
  					<?php endforeach; ?>
  				</ul>
  			</div>
  			<?php endif; ?>
  			
  			<?php if ($success) : ?>
  			<div class="alert alert-success">Your password has been updated.</div>
  			<?php endif; ?>
  			
  			<div class="control-group">
  				<label class="control-label" for="userEmail">Password</label>
  				<div class="controls">
  					<input class="input-block-level" type="password" name="password" value="" autocomplete="off" />
  				</div>
  			</div>
  			
  			<div class="control-group">
  				<label class="control-label" for="userEmail">Confirm Password</label>
  				<div class="controls">
  					<input class="input-block-level" type="password" name="confirm" value="" autocomplete="off" />
  				</div>
  			</div>
  		</fieldset>
  		
  </div>

  <div class="content-bottom">
    <div class="form-actions text-right">
      <button type="submit" class="btn btn-save"><i class="icon-ok"></i>Save</button>
    </div>
  </div>

</form>
