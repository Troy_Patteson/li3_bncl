<?php

namespace app\models;

class EmailReports extends Model
{
	protected $_meta = array(
		'key'    => 'id',
		'source' => 'email_reports',
		'locked' => TRUE,
	);
	
	protected $_schema = array(
		'id'          => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'campaign_id' => array('type' => 'integer',  'null' => FALSE),
		'address'     => array('type' => 'string',   'null' => FALSE),
	);
	
	public $validates = array(
		'address' => array(
			array('email', 'message' => 'Invalid email address', 'required' => TRUE)
		)
	);
	
}



?>