<?php

namespace app\controllers\admin;

use app\models\Websites;
use app\models\Campaigns;
use app\models\Products;
use app\models\Vouchers;

class ProductsController extends IndexController
{
	public function attach_vouchers($flag = NULL)
	{
		$data = $this->request->data + array(
			'count' => 0
		);
		
		if (empty($data['count']) || !$this->request->is('post')) {
			return;
		}
		
		$extended = $flag == 'extended' ? 1 : 0;
		
		$product = Products::first(array(
			'conditions' => array(
				'Products.id'       => $this->request->params['id'],
				'Products.status'   => array('!=' => Products::STATUS_DELETED),
				'Campaigns.status'  => array('!=' => Campaigns::STATUS_DELETED),
				//'Websites.owner_id' => $this->user['id'],
				'Websites.status'   => array('!=' => Websites::STATUS_DELETED),
			),
			
			'fields' => array(
				'Products' => array(
					'id',
					'campaign_id',
				),
				
				array('Websites.code_prefix AS code_prefix'),
				array('COALESCE(COUNT(Vouchers.id), 0) AS vouchers')
			),
			
			'joins' => array(
				array(
					'type'        => 'INNER',
					'source'      => 'campaigns',
					'alias'       => 'Campaigns',
					'constraints' => 'Campaigns.id = Products.campaign_id'
				),
				
				array(
					'type'        => 'INNER',
					'source'      => 'websites',
					'alias'       => 'Websites',
					'constraints' => 'Websites.id = Campaigns.website_id'
				),
				
				array(
					'type'        => 'LEFT',
					'source'      => 'vouchers',
					'alias'       => 'Vouchers',
					'constraints' => 'Vouchers.product_id = Products.id AND Vouchers.extended = ' . $extended
				),
			),
			
			'group' => array('Products.id')
		));
		
		$this->set(array(
			'product' => $product
		));
		
		if ($product) {
			$product->attachVouchers($data['count'], $product->code_prefix, $extended);
		}
	}

	public function download_vouchers($flag = NULL)
	{
		if (!isset($this->request->params['id'])) {
			return $this->_notFound();
		}
		
		$extended = $flag == 'extended' ? 1 : 0;

		$vouchers = Vouchers::all(array(
			'fields' => array(
				'Vouchers' => array('voucher')
			),
			'conditions' => array(
				'Products.id'       => $this->request->params['id'],
				'Products.status'   => Products::STATUS_ACTIVE,
				'Vouchers.extended' => $extended
			),
			
			'joins' => array(
				array(
					'type'        => 'INNER',
					'source'      => 'products',
					'alias'       => 'Products',
					'constraints' => 'Products.id = Vouchers.product_id'
				),
			)
		));
		
		$this->_render['data'] = $vouchers->data();
		array_unshift($this->_render['data'], array('Code'));
	}

}

?>