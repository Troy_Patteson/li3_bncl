<?php

namespace li3_extras\storage;

class Session extends \lithium\Storage\Session {

	public static function flash($key = null, array $options = array()) {
		$value = static::read($key, $options);
		static::delete($key, $options);
		
		return $value;
	}
}

?>