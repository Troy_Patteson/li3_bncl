<?php

use \app\models\Orders;

?>
	<div class="control-group">
		<label class="control-label" for="inTyMsg">Thank you message</label>
		<div class="controls">
			<textarea class="input-block-level" rows="10" id="inTyMsg" name="thank_you_msg"><?=$campaign->thank_you_msg?></textarea>
			<small class="muted">Available variables: <code><?php echo implode('</code>, <code>', Orders::$messagesVars)?></code></small>
		</div>
	</div>

  <hr />

	<div class="control-group">
		<label class="control-label" for="inErrMsg">Error message</label>
		<div class="controls">
			<textarea class="input-block-level" rows="10" id="inErrMsg" name="error_msg"><?=$campaign->error_msg?></textarea>
			<small class="muted">Available variables: <code><?php echo implode('</code>, <code>', Orders::$messagesVars)?></code></small>
		</div>
	</div>

  <hr />

  <h4>Tracking</h4>

  <?php $website = $websites[$campaign->website_id]; ?>
  <?php if(array_key_exists($website->host, $rakuten)): ?>
  <div class="control-group">
    <label class="control-label checkbox">
      <input type="checkbox" name="use_rakuten" id="inRakuten" value="1" <?php echo $campaign->use_rakuten ? 'checked' : '' ?> /> Use Rakuten
    </label>
  </div>
  <?php endif; ?>

  <div class="control-group other-tracking-control-group">
    <label class="control-label" for="inTracking">Other tracking tags</label>
    <div class="controls">
      <textarea class="input-block-level" rows="10" id="inTracking" name="tracking"><?=$campaign->tracking?></textarea>
    </div>
  </div>
