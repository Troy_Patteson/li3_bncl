<?php

namespace app\models;

use \lithium\security\Password;

class Users extends Model
{
	const ROLE_ROOT  = 1;
	const ROLE_ADMIN = 2;
	
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE  = 1;
		
	protected $_meta = array(
		'key'    => 'id',
		'source' => 'users',
		'locked' => TRUE,
	);
	
	protected $_schema = array(
		'id'         => array('type' => 'integer',  'null' => FALSE, 'key' => 'primary'),
		'email'      => array('type' => 'string',   'null' => FALSE),
		'password'   => array('type' => 'string',   'null' => FALSE),
		'role'       => array('type' => 'integer',  'null' => FALSE, 'default' => 2),
		'created'    => array('type' => 'datetime', 'null' => FALSE),
		'modified'   => array('type' => 'datetime', 'null' => FALSE),
		'last_login' => array('type' => 'datetime', 'null' => TRUE,  'default' => NULL),
		'status'     => array('type' => 'integer',  'null' => FALSE, 'default' => 1),
	);
	
	protected $_query = array(
		'conditions' => array(
			'status' => Users::STATUS_ACTIVE
		)
	);
	
	public $validates = array(
		'email' => array(
			array('email', 'message' => 'You specified an invalid email address', 'required' => TRUE, 'last' => TRUE),
			array('uniqueEmail', 'message' => 'An user with the specified email address already exists', 'required' => TRUE),
		),
		
		'password' => array(
			array('preg', 'match' => '/^.{5,}$/', 'message' => 'The password should be at least 5 characters', 'required' => TRUE),
		),
		
		'confirm' => array(
			array('confirmPassword', 'message' => 'You need to confirm the password', 'required' => TRUE),
		),
		
		'role' => array(
			array('intRange', 'lower' => 1, 'upper' => 2, 'message' => 'Invalid user role', 'required' => TRUE, 'on' => 'create')
		),
	);
	
	
	public function save($entity, $data = NULL, array $options = array())
	{
		if ($data) {
			$entity->set($data);
			$data = NULL;
		}
		
		if (!$entity->validates()) {
			return FALSE;
		}
		
		if (!$entity->exists() || $entity->modified('password')) {
			$entity->password = @Password::hash($entity->password);
		}
		
		$options['validate'] = FALSE;
		
		return parent::save($entity, $data, $options);
	}
	
	
	public static function admin($userId)
	{
		return static::first(array(
			'conditions' => array(
				'id'     => $userId,
				'role'   => static::ROLE_ADMIN,
				'status' => static::STATUS_ACTIVE
			)
		));
	}
	
	
	public static function admins($params = array()) {
		$params += array(
			'page'  => 1,
			'limit' => static::DEFAULT_COUNT,
		);
		extract($params);
		
		$conditions = array(
			'role'   => static::ROLE_ADMIN,
			'status' => static::STATUS_ACTIVE
		);
		
		$page  = max(1, (int) $page);
		$limit = max(1, (int) $limit);
		$total = static::count(compact('conditions'));
		$pages = ceil($total / $limit);
		
		if (!$total) {
			$admins = static::buildSet();
			$admins->pages = 0;
			
			return $admins;
		}
		
		$admins = static::all(array(
			'conditions' => $conditions,
			'order'      => array('email' => 'ASC'),
			'limit'      => $limit,
			'page'       => $page
		));
		$admins->pages = $pages;
		
		return $admins;
	}
}


// skip password validation if the entity exists and the password field was not modified
Users::applyFilter('validates', function($self, $params, $chain) {
	$entity = &$params['entity'];
	
	if ($entity->exists() && !$entity->modified('password')) {
		$rules  = @$params['options']['rules'] ?: $entity->validates;
		unset($rules['password']);
		$params['options']['rules'] = $rules;
	}
	
	return $chain->next($self, $params, $chain);
});

?>