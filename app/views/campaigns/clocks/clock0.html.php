<?php

$timezone    = new DateTimeZone('GMT');
$currentDate = new DateTime('NOW', $timezone);
$endDate     = new DateTime($campaign->end_date, $timezone);

$countdown   = $endDate->diff($currentDate);
$countdown   = $countdown->invert ? $countdown : $currentDate->diff($currentDate);

$difference  = max(0, $endDate->getTimestamp() - $currentDate->getTimestamp());

?>

<div class="campaign-countdown">
  <h2>Time remaining</h2>
  <div class="campaign-countdown-label">
    <div class="campaign-countdown-row clearfix">
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits days">-</div>
        <div class="campaign-countdown-block">Days</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits">:</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits hours">--</div>
        <div class="campaign-countdown-block">Hrs</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits">:</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits minutes">--</div>
        <div class="campaign-countdown-block">Mins</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits">:</div>
      </div>
      <div class="campaign-countdown-column">
        <div class="campaign-countdown-block campaign-countdown-digits seconds">--</div>
        <div class="campaign-countdown-block">Secs</div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  jQuery( document ).ready(function($)
  {
      var remaining   = <?=$difference?>;

      var tick = function()
      {
          remaining -= 1;

          if(!remaining){
            clearInterval(tick_interval);
          }

          var seconds   = Math.max(remaining, 0);
          var minutes   = Math.max(Math.floor(seconds/60), 0);
          var hours     = Math.max(Math.floor(minutes/60), 0);
          var days      = Math.max(Math.floor(hours/24), 0);

          seconds %= 60;
          minutes %= 60;
          hours   %= 24;

          $('.campaign-countdown-label .days').html(days);
          $('.campaign-countdown-label .hours').html(addZero(hours));
          $('.campaign-countdown-label .minutes').html(addZero(minutes));
          $('.campaign-countdown-label .seconds').html(addZero(seconds));
      }

      var addZero = function(number)
      {
          if(number < 10)
          {
              return '0' + number;
          }

          return number;
      }

      tick();
      var tick_interval = setInterval(tick, 1000);

  });

</script>