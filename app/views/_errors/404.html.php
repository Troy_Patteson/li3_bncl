<div class="content-box">
	<div class="page-header">
		<h1 class="text-center"><span class="icon-warning-sign"></span> Page not found</h1>
	</div>
	
	<p class="text-center">
		The page you are looking for does not exist. Use your browser's back button to return to the previous page or go to the <a href="<?=$this->url('Index::index')?>">main page</a>.
	</p>
</div>