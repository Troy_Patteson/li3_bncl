<?php

use app\models\Users;

?><!DOCTYPE html>
<html lang="en">
<head>
  <?php echo $this->html->charset();?>
  <title>MiniBox Admin &gt; <?php echo $this->title(); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  
  <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic&amp;subset=latin,latin-ext" rel="stylesheet" />
  <link href="<?php echo $this->path('libs/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet" />
  <link href="<?php echo $this->path('libs/jasny-bootstrap/css/jasny-bootstrap.css'); ?>" rel="stylesheet" />
  <link href="<?php echo $this->path('libs/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
  <link href="<?php echo $this->path('libs/jquery-ui/css/smoothness/jquery-ui-1.10.2.custom.min.css'); ?>" rel="stylesheet" />
  <link href="<?php echo $this->path('libs/placeholder-enhanced/css/placeholder-enhanced.css'); ?>" rel="stylesheet" />

  <link href="<?php echo $this->path('css/main.css'); ?>" rel="stylesheet" />
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->path('ico/apple-touch-icon-144-precomposed.png'); ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->path('ico/apple-touch-icon-114-precomposed.png'); ?>" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->path('ico/apple-touch-icon-72-precomposed.png'); ?>" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo $this->path('ico/apple-touch-icon-57-precomposed.png'); ?>" />
  <link rel="shortcut icon" href="<?php echo $this->path('favicon.ico'); ?>" />
</head>

<body>
  <div id="wrapper" class="clearfix">   
    <div class="sidebar">
      <div class="side-top">

        <div class="user nav">
            <i class="icon-user"></i><?= mb_strimwidth($user['email'], 0, 17, '...'); ?>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i></a>
            <ul class="dropdown-menu dropdown-menu-arrow pull-right" role="menu" aria-labelledby="dropdownMenu">
              <li><a tabindex="-1" href="<?=$this->url('Index::account')?>"><i class="icon-user"></i> Account</a></li>
              <li><a tabindex="-1" href="<?=$this->url(array('Auth::logout', 'admin' => NULL))?>"><i class="icon-off"></i> Logout</a></li>
            </ul>
        </div>
      </div>

      <?php 
      switch ($user['role']) :
        case Users::ROLE_ROOT :
          echo $this->_render('element', 'side-menu-root');
        break;
        
        case Users::ROLE_ADMIN :
          echo $this->_render('element', 'side-menu-admin');
        break;
        
        default :
        break;
      endswitch;
      ?>
    </div>
    
    <div id="content">
    <?php echo $this->content(); ?>
    </div>
  </div>
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="<?php echo $this->path('libs/bootstrap/js/bootstrap.js') ?>"></script>
  <script src="<?php echo $this->path('libs/jasny-bootstrap/js/jasny-bootstrap.js') ?>"></script>
  <script src="<?php echo $this->path('libs/bootbox/bootbox.min.js') ?>"></script>
  <script src="<?php echo $this->path('libs/jquery-ui/js/jquery-ui-1.10.2.custom.min.js') ?>"></script>
  <script src="<?php echo $this->path('libs/placeholder-enhanced/js/jquery.placeholder-enhanced.min.js') ?>"></script>
  <script src="<?php echo $this->path('js/main.js') ?>"></script>
  <?php echo $this->scripts(); ?>
</body>
</html>